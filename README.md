[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/materials-modeling%2Fhiphive-examples/master)

# Hiphive-examples

In this repository you will find various examples, tutorials and advanced usage of force-constants and hiphive.

In `examples` we have the fully fleshed out analysis of Si and the clathrate BaGaGe used in our [paper](https://arxiv.org/abs/1902.01271).

In `tutorials` you'll find tutorials jupyter-notebooks for using hiphive.


In `advanced` you'll find some smaller examples illustrating some more advanced usages of hiphive and force-constants in general.


## Access to repository
Clone the repo via

    git clone https://gitlab.com/materials-modeling/hiphive-examples.git

In order to fetch the large file which are tracked with LFS, use

    git lfs pull




You can launch an interactive version of this repository on
[Binder](https://mybinder.org/v2/gl/materials-modeling%2Fhiphive-examples/master).
