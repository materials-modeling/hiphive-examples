# Bayesian phonons

This example demonstrates how to carry out a sensitivity analysis of the force constants with respect to the phonon dispersion.
This is done in two ways

* Draw force constant models from the posterior probability (Bayesian parameter estimation)
* Bootstrapping (Bagging) to generate an multiple models

**Bayesian parameter optimization** allows one to find the joint probability distributions $`P(\boldsymbol{x})`$ of the parameters $`\boldsymbol{x}`$ given some training data and priors.
Here, we consider a simple harmonic force constant model for FCC Ni.
Uniform priors are used and 200 parameter vectors are drawn from $`P(\boldsymbol{x})`$ using [Markov chain Monte Carlo](https://en.wikipedia.org/wiki/Markov_chain_Monte_Carlo) sampling.
These 200 models are used to predict the harmonic phonon dispersion shown in the figure below.

<img src="https://hiphive.materialsmodeling.org/dev/_images/Ni_bayesian_dispersion.svg" width=400>

**Bagging** can be carried out via the [`EnsembleOptimizer`](https://hiphive.materialsmodeling.org/dev/moduleref/optimizers.html#ensembleoptimizer) as shown in the second part of the example script.

This type of analysis can also be employed for other properties such ase, e.g., the free energy or the thermal conducitvity.
It also allows one to identify regions of the configurational space for which the model sensitivity is poor and thus guide the selection of additional training structures.
