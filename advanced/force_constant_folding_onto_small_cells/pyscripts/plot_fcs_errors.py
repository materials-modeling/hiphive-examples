import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_context('talk')

# parameters
dims = [3, 4, 5, 6]

# compute fcs errors
records = []
for dim in dims:

    fc2_phonopy = np.load('fcs/fc2_phonopy_dim{}.npy'.format(dim))
    fc2_hiphive = np.load('fcs/fc2_hiphive_dim{}.npy'.format(dim))
    fc2_hiphive_fold = np.load('fcs/fc2_hiphive-folded_dim{}.npy'.format(dim))

    fc_error = np.linalg.norm(fc2_hiphive - fc2_phonopy) / np.linalg.norm(fc2_phonopy)
    fc_error_fold = np.linalg.norm(fc2_hiphive_fold - fc2_phonopy) / np.linalg.norm(fc2_phonopy)

    row = dict(dim=dim, fc_error=fc_error, fc_error_fold=fc_error_fold)
    records.append(row)

df = pd.DataFrame(records)


# plotting
fig = plt.figure()
ax1 = fig.add_subplot(111)
ax1.plot(df.dim, 100 * df.fc_error, '-o', label='hiphive-cutoffs')
ax1.plot(df.dim, 100 * df.fc_error_fold, '-o', label='hiphive-folded')
ax1.set_xlabel('DIM')
ax1.set_ylabel('Relative FC error (%)')
ax1.set_ylim(bottom=0.0)
ax1.legend()

fig.tight_layout()
fig.savefig('pdf/fcs_errors_folding.pdf')
plt.show()
