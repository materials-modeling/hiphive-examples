import numpy as np
from tools import train_fcp, get_phonopy_supercell


# parameters
dim = 6
fname = 'fcs/fc2_hiphive_dim{}.npy'.format(dim)


# get hiphive fcs
supercell = get_phonopy_supercell(dim)
fcp = train_fcp(dim)
fcs = fcp.get_force_constants(supercell)
fc2 = fcs.get_fc_array(order=2)
np.save(fname, fc2)
