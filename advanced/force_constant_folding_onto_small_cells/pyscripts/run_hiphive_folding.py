import numpy as np
from tools import get_prim, get_calc, get_phonopy_supercell
from hiphive import ClusterSpace, StructureContainer, ForceConstantPotential
from hiphive.structure_generation import generate_rattled_structures
from hiphive.utilities import prepare_structures
from hiphive.cutoffs import estimate_maximum_cutoff
from hiphive.core.atoms import spos_to_atom
from trainstation import Optimizer


def get_index_offset(atoms, atoms_ref):
    """ Get index offsets for atoms relative to atoms_ref.cell and positons """
    basis = atoms_ref.get_scaled_positions()
    spos = atoms_ref.cell.scaled_positions(atoms.positions)
    indices = []
    offsets = []
    for s in spos:
        a = spos_to_atom(s, basis)
        indices.append(a.site)
        offsets.append(a.offset)
    return indices, offsets


# parameters
dim = 6
n_structures = 10
rattle = 0.01

# setup
calc = get_calc()
prim = get_prim()
supercell = get_phonopy_supercell(dim)
supercell2 = supercell.repeat(2)
max_cutoff = estimate_maximum_cutoff(supercell2) - 1e-5
cutoffs = [max_cutoff]

# generate training structures
training_structures = generate_rattled_structures(supercell, n_structures, rattle)
training_structures = prepare_structures(training_structures, supercell, calc)

# setup CS SC
cs = ClusterSpace(prim, cutoffs)
sc = StructureContainer(cs)
for x in training_structures:
    atoms = x.repeat(2)
    sc.add_structure(atoms)

# training
opt = Optimizer(sc.get_fit_data(), train_size=0.9, fit_method='ridge')
opt.train()
print(opt)
fcp = ForceConstantPotential(cs, opt.parameters)
fcs = fcp.get_force_constants(supercell2)


# folding of fcs
n_atoms = len(supercell)
indices, offsets = get_index_offset(supercell2, supercell)
fc2 = np.zeros((n_atoms, n_atoms, 3, 3))

for i in range(n_atoms):
    for i2 in range(len(supercell2)):
        j = indices[i2]
        fc2[i, j] += fcs[i, i2]

# save fcs
fname = 'fcs/fc2_hiphive-folded_dim{}.npy'.format(dim)
np.save(fname, fc2)

