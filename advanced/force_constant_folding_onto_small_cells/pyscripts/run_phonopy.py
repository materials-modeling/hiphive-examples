import numpy as np
from tools import get_phonons_phonopy


# parameters
dim = 6

fname = 'fcs/fc2_phonopy_dim{}.npy'.format(dim)


# phononpy analysis
phonopy = get_phonons_phonopy(dim)
fc2 = phonopy.get_force_constants()
np.save(fname, fc2)
