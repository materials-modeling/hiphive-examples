import numpy as np

from ase import Atoms
from ase.build import bulk
from ase.calculators.emt import EMT

from hiphive import ClusterSpace, StructureContainer, ForceConstantPotential
from hiphive.structure_generation import generate_rattled_structures
from hiphive.cutoffs import estimate_maximum_cutoff
from hiphive.utilities import prepare_structures
from trainstation import Optimizer

from phonopy import Phonopy
from phonopy.structure.atoms import PhonopyAtoms


def get_prim():
    return bulk('Al', 'fcc', a=4.0)


def get_supercell(size):
    prim = get_prim()
    return prim.repeat(size)


def get_calc():
    return EMT()


def ase_atoms_to_phonopy(atoms_ase):
    atoms_phonopy = PhonopyAtoms(symbols=atoms_ase.get_chemical_symbols(),
                                 scaled_positions=atoms_ase.get_scaled_positions(),
                                 cell=atoms_ase.cell)
    return atoms_phonopy


def phonopy_atoms_to_ase(atoms_phonopy):
    atoms_ase = Atoms(symbols=atoms_phonopy.get_chemical_symbols(),
                      scaled_positions=atoms_phonopy.get_scaled_positions(),
                      cell=atoms_phonopy.get_cell(), pbc=True)
    return atoms_ase


def get_phonopy_supercell(dim):
    """ Supercell from phonopy for given DIM, corresponds to SPOSCAR """
    prim = get_prim()
    prim_phonopy = ase_atoms_to_phonopy(prim)
    phonopy = Phonopy(prim_phonopy, supercell_matrix=dim*np.eye(3), primitive_matrix=None)
    supercell = phonopy_atoms_to_ase(phonopy.get_supercell())
    return supercell


def get_phonons_phonopy(dim, displacement=0.01):
    """ Get phonopy API object using phonopy to calculate force constants
    for a supercell of size dim """

    # setup
    calc = get_calc()
    prim = get_prim()
    prim_phonopy = ase_atoms_to_phonopy(prim)
    phonopy = Phonopy(prim_phonopy, supercell_matrix=dim*np.eye(3), primitive_matrix=None)

    # compute fc2
    phonopy.generate_displacements(distance=displacement)
    supercells = phonopy.get_supercells_with_displacements()
    set_of_forces = []
    for i, supercell in enumerate(supercells):
        supercell_ase = phonopy_atoms_to_ase(supercell)
        supercell_ase.set_calculator(calc)
        set_of_forces.append(supercell_ase.get_forces())
    phonopy.set_forces(set_of_forces)
    phonopy.produce_force_constants()
    return phonopy


def get_phonons_hiphive(fcp, dim):
    """ Get phonopy API object using FCP to get force constants for a supercell
    with size dim """

    # setup
    prim = get_prim()
    prim_phonopy = ase_atoms_to_phonopy(prim)

    phonopy = Phonopy(prim_phonopy, supercell_matrix=dim*np.eye(3), primitive_matrix=None)
    supercell = phonopy_atoms_to_ase(phonopy.get_supercell())
    fcs = fcp.get_force_constants(supercell)
    fc2 = fcs.get_fc_array(order=2)
    phonopy.set_force_constants(fc2)
    return phonopy


def get_maximum_cutoff(dim):
    supercell = get_supercell(dim)
    max_cutoff = estimate_maximum_cutoff(supercell)
    return max_cutoff - 1e-5


def train_fcp(dim, cutoffs=None, n_structures=10, rattle=0.01):

    # setup
    calc = get_calc()
    prim = get_prim()
    supercell = get_supercell(dim)

    if cutoffs is None:
        cutoffs = [get_maximum_cutoff(dim)]

    # generate training structures
    training_structures = generate_rattled_structures(supercell, n_structures, rattle)
    training_structures = prepare_structures(training_structures, supercell, calc)

    # setup CS SC
    cs = ClusterSpace(prim, cutoffs)
    sc = StructureContainer(cs)
    for x in training_structures:
        sc.add_structure(x)

    # training
    opt = Optimizer(sc.get_fit_data(), train_size=0.9)
    opt.train()
    print(opt)

    fcp = ForceConstantPotential(cs, opt.parameters)
    return fcp
