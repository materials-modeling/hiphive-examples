import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from tools import get_prim
from hiphive import ClusterSpace, ForceConstantPotential
sns.set_context('talk')


# parameters
cutoff = 9.0
fcp_fname = 'fcps/fcp_long_range_cutoff-{}.fcp'.format(cutoff)


# build model
prim = get_prim()
cs = ClusterSpace(prim, [cutoff])

np.random.seed(420)
p_offset = 0 * (2 * np.round(np.random.random(cs.n_dofs)) - 1)
p_noise = np.random.normal(0, 1, cs.n_dofs)
parameters = p_offset + p_noise
fcp = ForceConstantPotential(cs, parameters, metadata=dict(parameters=parameters))
fcp.write(fcp_fname)

# plotting
df = pd.DataFrame(fcp.orbit_data)
fig = plt.figure()
ax1 = fig.add_subplot(111)
ax1.semilogy(2 * df.radius, df.force_constant_norm, 'o')
ax1.set_xlabel('Pair distance (Å)')
ax1.set_ylabel('FC norm (eV/Å$^2$)')
fig.tight_layout()
fig.savefig('pdf/reference_model.pdf')
plt.show()
