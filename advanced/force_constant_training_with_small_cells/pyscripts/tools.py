import numpy as np
from ase.build import bulk


def get_prim():
    return bulk('Al', 'fcc', a=4.0)


def get_supercell(size):
    prim = get_prim()
    return prim.repeat(size)
