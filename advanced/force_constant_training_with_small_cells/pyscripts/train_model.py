import numpy as np
import random
from hiphive import ClusterSpace, StructureContainer, ForceConstantPotential
from hiphive.calculators import ForceConstantCalculator
from hiphive.cutoffs import estimate_maximum_cutoff
from hiphive.utilities import prepare_structure
from trainstation import Optimizer

# parameters
cutoff_ref = 9.0
std = 0.05
train_sizes = [3, 4, 5]
repeats = [3, 2, 2, 2]

# setup
fcp_ref = ForceConstantPotential.read(f'fcps/fcp_long_range_cutoff-{cutoff_ref}.fcp')
prim = fcp_ref.primitive_structure
params_ref = fcp_ref.metadata['parameters']


# cutoff check
for size in [2, 3, 4, 5, 6, 7]:
    atoms = prim.repeat(size)
    max_cutoff = estimate_maximum_cutoff(atoms)
    print(f'Size {size} maximum cutoff  {max_cutoff:.4f}')


# gen training structures
training_structures = []
for rep, size in zip(repeats, train_sizes):
    atoms_ideal = prim.repeat(size).repeat(rep)
    atoms = prim.repeat(size)
    atoms.rattle(std, seed=random.randint(1, 1000000))
    atoms = atoms.repeat(rep)
    calc = ForceConstantCalculator(fcp_ref.get_force_constants(atoms_ideal))
    atoms = prepare_structure(atoms, atoms_ideal, calc)
    training_structures.append(atoms)

# setup train
cs = ClusterSpace(prim, [cutoff_ref])
sc = StructureContainer(cs)
for x in training_structures:
    sc.add_structure(x)

for i in range(len(sc)):
    A, _ = sc.get_fit_data(structures=[i])
    cond = np.linalg.cond(A)
    print(f'Condition number with structure {i}: {cond:.2e}')

A, y = sc.get_fit_data()
cond = np.linalg.cond(A)
print(f'Condition number with all structures: {cond:.2e}')

# train
opt = Optimizer((A, y), train_size=1.0)
opt.train()
print(opt)

params = opt.parameters
print('parameter diff\n', params - params_ref)
print('parameter diff: ', np.linalg.norm(params - params_ref))

