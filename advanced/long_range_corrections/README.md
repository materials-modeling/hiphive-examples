# Handling systems with long-range interactions

Note that a more extensive example of dealing with long-range interactions is available at the [hiphive-website](https://hiphive.materialsmodeling.org/advanced_topics/long_range_forces.html)


Systems with some degree of ionicity demand special attention when computing the phonon dispersion.
Specifically, they exhibit a splitting of the longitudinal optical (LO) and transverse optical (TO) branches in the long-wave length limit, i.e. small momentum vectors ($\vec{q}\rightarrow 0$).
When computing the phonon dispersion directly from the force constant matrix without further treatment, this splitting is missing.
A common approach for recovering the correct behavior is to apply a non-analytic correction (NAC) as discussed for example by Gonze and Lee (Phys. Rev. B **55**, 10355 (1997); a more comprehensive list of references can be found in the [phonopy documentation](https://phonopy.github.io/phonopy/formulation.html#non-analytical-term-correction)).
To apply this correction one requires knowledge of the Born effective charges $\mathcal{Z}_{i,\alpha\beta}^*$ as well as the electronic part of the static dielectric tensor $\epsilon_{\alpha\beta}^\infty$.

For the same fundamental reasons such systems often feature long-ranged ionic interactions, which can be difficult to capture using a short-ranged force constant potential (FCP), leading to erroneous behavior especially near the $\Gamma$-point.
To overcome this problem one can first substract the long-ranged contribution, then express the remaining ("chemical") part via a regular short-ranged FCP, and finally combine the two whenever computing forces for an arbitrary displacement pattern, be it for generating phonon dispersions, lifetimes or forces for molecular dynamics simulations.

This notebook illustrates the physical behavior outlined above and demonstrates the procedure by which the correct behavior in the long-wavelength limit can be restored.

To this end, we consider two prototypical ionic materials, NaCl and MgO, both of which adopt the rocksalt structure.
The code below can be run for either case by changing the value of the `material` variable to either `'NaCl'` or `'MgO'`.
All reference data needed for this example, including forces, Born effective charges and dielectric tensors, is contained in the [ASE database](https://wiki.fysik.dtu.dk/ase/ase/db/db.html) `reference-calculations.db`.

The following code snippet can be used to generate configurations for use with [phonopy](https://phonopy.github.io/phonopy/) as well as [rattled structures for use with hiphive](https://hiphive.materialsmodeling.org/moduleref/structures.html#preparing-structures-for-training).
The forces obtained via density functional theory calculations using the PBE functional as implemented in [VASP](https://www.vasp.at/) are already included in the database `reference-calculations.db`.