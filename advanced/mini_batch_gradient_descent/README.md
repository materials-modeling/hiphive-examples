# Mini batch gradient descent
In this example an harmonic model for a vacancy in FCC Al will be trained.
First run `setup.py` to generate the training data in easily readable numpy format.
Next `train_mini_batch.py` can be run to peform mini batch gradient descent to train the FCP.

This uses sklearns implementation of the partial stochastic graident descent, with hyper-parameters described here https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.SGDRegressor.html#sklearn.linear_model.SGDRegressor

The learning rate can be tuned to obtain a quick convergence with respsect to epochs, however too large value will make the parameters blow up.


## Regular OLS
Running the hiphive Optimizer with OLS yields the following

```
===================== Optimizer ======================
seed                           : 42
fit_method                     : least-squares
standardize                    : True
n_target_values                : 25800
n_parameters                   : 2796
n_nonzero_parameters           : 2796
parameters_norm                : 131.7174
target_values_std              : 0.1115157
rmse_train                     : 0.00785667
rmse_test                      : 0.009272309
R2_train                       : 0.9950333
R2_test                        : 0.9931232
AIC                            : -219474.5
BIC                            : -196958.9
train_size                     : 23220
test_size                      : 2580
======================================================
```