"""
Plot convergence for mini batch fits for multiple learning rates
"""

import pandas as pd
import matplotlib.pyplot as plt


# sgd params
batch_size = 1
eta0_vals = [0.0001, 0.0002, 0.0003, 0.0004]
alpha = 0

# ols values
ols_pnorm = 131.7174
ols_rmse_train = 0.00785667
ols_rmse_test = 0.009272309

# read data
dfs = dict()
for eta0 in eta0_vals:
    tag = 'batchsize-{}_eta0-{}_alpha-{}'.format(batch_size, eta0, alpha)
    df = pd.read_csv('data/df_{}.csv'.format(tag))
    dfs[eta0] = df


# plot
fig = plt.figure(figsize=(5, 8))
ax1 = fig.add_subplot(3, 1, 1)
ax2 = fig.add_subplot(3, 1, 2)
ax3 = fig.add_subplot(3, 1, 3)

xlim = [0, 4000]
colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

for (eta0, df), color in zip(dfs.items(), colors):
    label = 'eta0={}'.format(eta0)
    ax1.plot(df.n_iter, df.rmse_train, color=color, label=label)
    ax2.plot(df.n_iter, df.rmse_test, color=color, label=label)
    ax3.plot(df.n_iter, df.p_norm, color=color, label=label)

ax1.legend()
ax2.legend()
ax3.legend()

ax1.axhline(y=ols_rmse_train, ls='--', color='k', label='OLS')
ax2.axhline(y=ols_rmse_test, ls='--', color='k', label='OLS')
ax3.axhline(y=ols_pnorm, ls='--', color='k', label='OLS')

ax1.set_ylabel('RMSE train')
ax2.set_ylabel('RMSE test')
ax3.set_ylabel('||parameters||')
ax3.set_xlabel('epochs')

ax1.set_xlim(xlim)
ax2.set_xlim(xlim)
ax3.set_xlim(xlim)
ax1.set_ylim([0, 0.04])
ax2.set_ylim([0, 0.04])
ax3.set_ylim([0, 150])

fig.tight_layout()
fig.savefig('pdf/learning_rate_convergence.pdf')
plt.show()
