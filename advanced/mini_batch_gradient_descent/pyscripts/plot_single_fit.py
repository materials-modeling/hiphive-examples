"""
Plot convergence for a single fit with mini batch
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


# sgd params
batch_size = 1
eta0 = 0.0002
alpha = 0.001

# ols values
ols_pnorm = 131.7174
ols_rmse_train = 0.00785667
ols_rmse_test = 0.009272309

# read data
tag = 'batchsize-{}_eta0-{}_alpha-{}'.format(batch_size, eta0, alpha)
df = pd.read_csv('data/df_{}.csv'.format(tag))
parameters = np.load('data/parameters_{}.npy'.format(tag))


# plot params
xlim = [0, 4000]
title = tag.replace('_', '  ')

# figure 1: parameter convergence
parameters_to_plot = [0, 1, 2, 3, 4, 5, 10, 50, 100, 2000]
fig1, ax1 = plt.subplots(1)
for i in parameters_to_plot:
    ax1.plot(parameters[:, i] - parameters[-1, i])

ax1.set_ylim([-0.4, 0.4])
ax1.set_xlim(xlim)
ax1.set_xlabel('epochs')
ax1.set_ylabel('parameters')
ax1.set_title(title)

fig1.tight_layout()
fig1.savefig('pdf/mini_batch_{}.pdf'.format(tag))


# figure 2: rmse convergence
fig2 = plt.figure()
ax1 = fig2.add_subplot(2, 1, 1)
ax2 = fig2.add_subplot(2, 1, 2)
col1 = 'tab:blue'
col2 = 'tab:orange'

ax1.semilogy(df.n_iter, df.rmse_train, color=col1, label='train')
ax1.semilogy(df.n_iter, df.rmse_test, color=col2, label='test')
ax1.axhline(y=ols_rmse_train, ls='--', color=col1)
ax1.axhline(y=ols_rmse_test, ls='--', color=col2)

ax1.legend()
ax1.set_ylabel('RMSE')
ax1.set_ylim([5e-3, 1e-1])
ax1.set_xlim(xlim)
ax1.set_title(title)

ax2.plot(df.n_iter, df.p_norm)
ax2.axhline(y=ols_pnorm, ls='--')
ax2.set_xlabel('epochs')
ax2.set_ylabel('||parameters||')
ax2.set_xlim(xlim)
ax2.set_ylim([0, 150])

fig2.tight_layout()
plt.show()
