import os
import numpy as np
from ase.build import bulk
from ase.optimize import BFGS
from ase.calculators.emt import EMT

from hiphive import ClusterSpace
from hiphive.utilities import prepare_structures
from hiphive.structure_generation import generate_rattled_structures
from hiphive.force_constant_model import ForceConstantModel

# parameters
a0 = 4.05
size = 6
cutoffs = [8.0]
n_structures = 40

# setup atoms
atoms = bulk('Al', 'fcc', cubic=False, a=a0).repeat(size)
del atoms[0]

# relax positions
calc = EMT()
atoms.set_calculator(calc)
dyn = BFGS(atoms)
dyn.run(fmax=0.0001, steps=1000)


# generate training structures
rattled_structures = generate_rattled_structures(atoms, n_structures=n_structures, rattle_std=0.03)
rattled_structures = prepare_structures(rattled_structures, atoms, calc=calc)

cs = ClusterSpace(atoms, cutoffs)
fcm = ForceConstantModel(atoms, cs)

# write training data in format which can easily be read for individual structures
os.makedirs('training_data', exist_ok=True)

for i, a in enumerate(rattled_structures):
    M = fcm.get_fit_matrix(a.get_array('displacements'))
    f = a.get_array('forces').flatten()

    fname = 'training_data/fit_data_{:04d}.npy'.format(i)
    data = dict(M=M, f=f)
    np.save(fname, data, allow_pickle=True)
