"""
Train model with mini batch gradient descent
"""

import numpy as np
import pandas as pd
from sklearn.linear_model import SGDRegressor
from sklearn.model_selection import train_test_split


def compute_rmse(indices, parameters):
    """ compute rmse for a list of structures """
    dy = []
    for i in indices:
        M, f_target = get_fit_data_single(i)
        f_predicted = M.dot(parameters)
        dy.extend(f_target - f_predicted)
    rmse = np.sqrt(np.mean(np.power(dy, 2)))
    return rmse


def get_fit_data(indices):
    """ get fit data for list of structures """
    M_list, f_list = [], []
    for i in indices:
        M, f = get_fit_data_single(i)
        M_list.append(M)
        f_list.append(f)
    return np.vstack(M_list), np.hstack(f_list)


def get_fit_data_single(index):
    """ get fit data for single structure """
    fname = 'training_data/fit_data_{:04d}.npy'.format(index)
    d = np.load(fname, allow_pickle=True).item()
    return d['M'], d['f']


# parameters
n_structures = 40


# sgd params
n_epochs = 10000  # number of iterations
batch_size = 1    # in regards to structures
eta0 = 0.0002     # initial learing rate (can be tunded for faster convergence)
alpha = 0         # regularization term, 0 yields least-squares problem else elsaticnet


tag = 'batchsize-{}_eta0-{}_alpha-{}'.format(batch_size, eta0, alpha)

# test train split (random structures chosen to be in train and test set)
train_inds, test_inds = train_test_split(range(n_structures), train_size=0.9, random_state=42)

# each column when trainig should have roughly std = 1
# here a single structure is used to find rought estimate of scaling
A, _ = get_fit_data(indices=[0])
scale = A.std(axis=0)


# setup mini batch training
n_batches = len(train_inds) // batch_size  # number of mini-batches
sgd = SGDRegressor(alpha=alpha, eta0=eta0, fit_intercept=False)

# mini-batch training
records = []
parameter_path = []
for n in range(n_epochs):

    inds = train_inds.copy()
    np.random.shuffle(inds)
    batches = np.array_split(inds, n_batches)

    for batch_inds in batches:
        A_batch, y_batch = get_fit_data(batch_inds)
        sgd.partial_fit(A_batch / scale, y_batch)

    p = sgd.coef_ / scale
    parameter_path.append(p.copy())

    # compute rmse and print
    if n % 5 == 0:
        rmse_train = compute_rmse(train_inds, p)
        rmse_test = compute_rmse(test_inds, p)

        fmt = 'iter {:4d} , ||x|| = {:10.6f} , rmse_train = {:10.6f} , rmse_test = {:10.6f}'
        print(fmt.format(n, np.linalg.norm(p), rmse_train, rmse_test))

        row = dict(n_iter=n, p_norm=np.linalg.norm(p), rmse_test=rmse_test, rmse_train=rmse_train)
        records.append(row)

    # write to file
    if n % 100 == 0 and n != 0:
        df = pd.DataFrame(records)
        df.to_csv('data/df_{}.csv'.format(tag), index=False)
        data = np.array(parameter_path)
        np.save('data/parameters_{}.npy'.format(tag), data)
