"""
Train a model using ordinary least-square (OLS)
"""

import numpy as np
from trainstation import Optimizer


def get_fit_data(indices):
    """ get fit data for list of structures """
    M_list, f_list = [], []
    for i in indices:
        M, f = get_fit_data_single(i)
        M_list.append(M)
        f_list.append(f)
    return np.vstack(M_list), np.hstack(f_list)


def get_fit_data_single(index):
    """ get fit data for single structure """
    fname = 'training_data/fit_data_{:04d}.npy'.format(index)
    d = np.load(fname, allow_pickle=True).item()
    return d['M'], d['f']


# train ols
n_structures = 40
M, f = get_fit_data(range(n_structures))
opt = Optimizer((M, f))
opt.train()
print(opt)
