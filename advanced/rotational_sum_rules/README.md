# Rotational sum rules
In this example we demonstrate how one can enforce the rotational sum rules
of a 2D material by including the rotational constraints when fitting.
This is compared to enforcing the rotational sum rules via post-processing,
and fitting with the constraints seem to be superior.