import numpy as np
import matplotlib.pyplot as plt
from hiphive import ForceConstantPotential, StructureContainer
from hiphive import enforce_rotational_sum_rules
from trainstation import Optimizer
from tools import compute_dispersion
import seaborn as sns
sns.set_context('talk')

# parameters
lw = 2
alpha_transparent = 0.8
alpha_vals = np.logspace(1, 5, 25)


# fit model
sc = StructureContainer.read('data/structure_container.sc')
cs = sc.cluster_space
opt = Optimizer(sc.get_fit_data(), train_size=1.0)
opt.train()
print(opt)

# get normal FCP
parameters = opt.parameters
fcp_normal = ForceConstantPotential(cs, parameters)
prim = fcp_normal.primitive_structure
q_normal, f_normal = compute_dispersion(prim, fcp_normal)

# apply sum rules
data = dict()
for alpha in alpha_vals:
    parameters_rot = enforce_rotational_sum_rules(cs, parameters, ['Huang', 'Born-Huang'], alpha=alpha)
    fcp = ForceConstantPotential(cs, parameters_rot)
    q, f = compute_dispersion(prim, fcp)
    data[alpha] = q, f


# plotting
fig = plt.figure(figsize=(12, 5))
ax1 = fig.add_subplot(121)
ax2 = fig.add_subplot(122)


cmap = plt.get_cmap('plasma')
colors = [cmap(i) for i in np.linspace(0, 1, len(data))]

for ax in [ax1, ax2]:
    for c, (alpha, (q, f)) in zip(colors, data.items()):
        ax.plot(q, f, c=c, lw=lw, alpha=alpha_transparent)
    ax.plot(q_normal, f_normal, '--k')
    ax.plot(np.nan, np.nan, '--k', label='No rotational')

ax1.legend()
ax1.set_xlim([0.0, q_normal[-1]])
ax2.set_xlim([0.0, 0.07])
ax2.set_ylim([-0.6, 3])
ax1.set_ylabel('Frequency (THz)')

# colorbar
norm = plt.Normalize(vmin=np.log10(alpha_vals.min()), vmax=np.log10(alpha_vals.max()))
sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
sm._A = []
cbar = plt.colorbar(sm)
cbar.set_label('log10 (alpha)')

fig.tight_layout()
fig.savefig('pdf/rotational_sum_rules.pdf')
plt.show()
