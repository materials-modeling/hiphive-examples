import numpy as np
import matplotlib.pyplot as plt
from hiphive import ForceConstantPotential, StructureContainer
from hiphive.core.rotational_constraints import get_rotational_constraint_matrix
from trainstation import Optimizer
from tools import compute_dispersion

# parameters
lw = 2
alpha = 0.95
lambda_vals = np.logspace(-4, -1, 25)


# fit model
sc = StructureContainer.read('data/structure_container.sc')
cs = sc.cluster_space
opt = Optimizer(sc.get_fit_data(), train_size=1.0)
opt.train()
print(opt)

# get normal FCP
parameters = opt.parameters
fcp_normal = ForceConstantPotential(cs, parameters)
prim = fcp_normal.primitive_structure
q_normal, f_normal = compute_dispersion(prim, fcp_normal)

# get constraint matrix
Ac = get_rotational_constraint_matrix(cs)
yc = np.zeros((Ac.shape[0]))
A, y = sc.get_fit_data()


# apply sum rules
data = dict()
for lam in lambda_vals:

    A_full = np.vstack((A, lam * Ac))
    y_full = np.hstack((y, yc))
    opt = Optimizer((A_full, y_full), train_size=1.0, standardize=False)
    opt.train()
    parameters_rot = opt.parameters
    fcp = ForceConstantPotential(cs, parameters_rot)
    q, f = compute_dispersion(prim, fcp)
    data[lam] = q, f


# plotting
fig = plt.figure(figsize=(12, 5))
ax1 = fig.add_subplot(121)
ax2 = fig.add_subplot(122)

cmap = plt.get_cmap('plasma')
colors = [cmap(i) for i in np.linspace(0, 1, len(data))]

for ax in [ax1, ax2]:
    for c, (lam, (q, f)) in zip(colors, data.items()):
        ax.plot(q, f, c=c, lw=lw, alpha=alpha)
    ax.plot(q_normal, f_normal, '--k')
    ax.plot(np.nan, np.nan, '--k', label='No rotational')

ax1.legend()
ax1.set_xlim([0.0, q_normal[-1]])
ax2.set_xlim([0.0, 0.07])
ax2.set_ylim([-0.6, 3])
ax1.set_ylabel('Frequency (THz)')

# colorbar
norm = plt.Normalize(vmin=np.log10(lambda_vals.min()), vmax=np.log10(lambda_vals.max()))
sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
sm._A = []
cbar = plt.colorbar(sm)
cbar.set_label('log10 (lambda)')

fig.tight_layout()
fig.savefig('pdf/rotational_sum_rules_when_fitting.pdf')
plt.show()
