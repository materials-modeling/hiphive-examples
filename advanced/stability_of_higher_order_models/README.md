# Stability of higher order models
Here we'll consider Ta bulk EAM with a reference EAM potential.
Higher order models will be trained and the stability (time before MD simulation blows up) will
be analyzed with respect to multiple parameters, such as

* Training structure generation method
* Number of training structures
* Cutoffs
* Fit method

