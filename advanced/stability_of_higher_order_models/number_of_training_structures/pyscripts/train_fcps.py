import random
import numpy as np
from ase.io import read
from hiphive import ClusterSpace, StructureContainer, ForceConstantPotential
from hiphive.cutoffs import Cutoffs
from trainstation import Optimizer


# params
n_structures = 10
cutoffs = Cutoffs([[6.0, 6.0, 6.0, 6.0, 6.0]])


# fcp key
fcp_ind = 0
key = 'nstructs-{}_ind-{}'.format(n_structures, fcp_ind)
fcp_fname = 'fcps/fcp_{}.fcp'.format(key)


# read structures
fname = '../training_structures/training_structures/md_T3000.extxyz'
structures = read(fname, index=':')
n_available = len(structures)
assert n_structures <= n_available

# training structures indices
seed = random.randint(0, 50000000)
np.random.seed(seed)
inds = np.random.choice(n_available, n_structures, replace=False)

# setup CS and SC
cs = ClusterSpace(structures[0], cutoffs)
sc = StructureContainer(cs)
for x in structures:
    sc.add_structure(x)
    if len(sc) >= n_structures:
        break
print(sc)

# train
opt = Optimizer(sc.get_fit_data(), fit_method='least-squares', train_size=1.0)
opt.train()
print(opt)

# store fcp
fcp = ForceConstantPotential(cs, opt.parameters)
fcp.write(fcp_fname)
