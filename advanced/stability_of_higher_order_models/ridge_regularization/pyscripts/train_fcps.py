from ase.io import read
from hiphive import ClusterSpace, StructureContainer, ForceConstantPotential
from hiphive.cutoffs import Cutoffs
from trainstation import Optimizer


# params
cutoffs = Cutoffs([[6.0, 6.0, 6.0, 6.0, 6.0]])

# training structure choice
fname = '../training_structures/training_structures/md_T3000.extxyz'
structures = read(fname, index=':')

# CS
cs = ClusterSpace(structures[0], cutoffs)

# fit method
alpha = 0.1
key = 'ridge_alpha-{}'.format(alpha)

# SC
sc = StructureContainer(cs)
for x in structures:
    sc.add_structure(x)

# train
opt = Optimizer(sc.get_fit_data(), fit_method='ridge', alpha=alpha, train_size=1.0)
opt.train()
print(opt)

# store fcp
fcp = ForceConstantPotential(cs, opt.parameters)
fcp_fname = 'fcps/fcp_{}.fcp'.format(key)
fcp.write(fcp_fname)
