# Training structures
Here we'll consider the stability of a higher order model as a function of how the training structures were generated. Methods to test are MD, phonon-rattle, MD+rattle.
These methods are tried for various temperatures.
