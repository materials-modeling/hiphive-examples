import random
import numpy as np

from hiphive.utilities import prepare_structures
from tools import get_single_calculator

from ase.io import read, write
from ase import units
from ase.md.velocitydistribution import MaxwellBoltzmannDistribution
from ase.md.langevin import Langevin


# parameters
size = 6
temperatures = [1000, 1500, 2000, 2500, 3000, 3500]
n_eq = 10000
n_sample = 20000
dt = 1.0
dump_interval = 200

# setup atoms
prim = read('../prim.xyz')
atoms_ideal = prim.repeat(size)


# calc
potential_file = '../Ta1_Ravelo_2013.eam.alloy'
calc = get_single_calculator(potential_file, 'Ta', 'eam/alloy')


for T in temperatures:

    run = True
    run_count = 0
    while run:

        run_count += 1

        try:
            seed = random.randint(0, 5000000)
            np.random.seed(seed)

            print('Temperature: {}'.format(T))

            # set up molecular dynamics simulation
            atoms = atoms_ideal.copy()
            atoms.set_calculator(calc)
            dyn = Langevin(atoms, dt * units.fs, T * units.kB, 0.02)

            # equilibration run
            MaxwellBoltzmannDistribution(atoms, T * units.kB)
            dyn.run(n_eq)

            # sample structures run
            step = 0
            structures = []
            while step < n_sample:
                print(step)
                dyn.run(dump_interval)
                structures.append(atoms.copy())
                step += dump_interval

            structures = prepare_structures(structures, atoms_ideal, calc)

            print(' Number of snapshots: {}'.format(len(structures)))
            write('training_structures/md_T{}.extxyz'.format(T), structures)
            run = False

        except Exception as e:
            print('   ', run_count)
            print('   ', str(e))
            print()
