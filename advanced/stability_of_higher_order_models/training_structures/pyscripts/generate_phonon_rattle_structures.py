import numpy as np

from ase import Atoms
from ase.io import read, write

from phonopy import Phonopy
from phonopy.structure.atoms import PhonopyAtoms

from hiphive.structure_generation import generate_phonon_rattled_structures
from hiphive.utilities import prepare_structures
from tools import get_single_calculator


def ase_atoms_to_phonopy(atoms_ase):
    atoms_phonopy = PhonopyAtoms(symbols=atoms_ase.get_chemical_symbols(),
                                 scaled_positions=atoms_ase.get_scaled_positions(),
                                 cell=atoms_ase.cell)
    return atoms_phonopy


def phonopy_atoms_to_ase(atoms_phonopy):
    atoms_ase = Atoms(symbols=atoms_phonopy.get_chemical_symbols(),
                      scaled_positions=atoms_phonopy.get_scaled_positions(),
                      cell=atoms_phonopy.get_cell(), pbc=True)
    return atoms_ase


#  parameters
dim = 6
temperatures = [4000, 3500, 3000, 2500, 2000, 1500, 1000]
n_structures = 100


# setup atoms and calc
potential_file = '../Ta1_Ravelo_2013.eam.alloy'
calc = get_single_calculator(potential_file, 'Ta', 'eam/alloy')
prim = read('../prim.xyz')
prim.set_calculator(calc)

# setup phonopy
prim_phonopy = ase_atoms_to_phonopy(prim)
phonopy = Phonopy(prim_phonopy, supercell_matrix=dim*np.eye(3), primitive_matrix=None)

# calculate supercells
phonopy.generate_displacements(distance=0.01)
supercells = phonopy.get_supercells_with_displacements()
set_of_forces = []
for i, supercell in enumerate(supercells):
    supercell_ase = phonopy_atoms_to_ase(supercell)
    supercell_ase.set_calculator(calc)
    set_of_forces.append(supercell_ase.get_forces())
phonopy.set_forces(set_of_forces)

# get fc2
phonopy.produce_force_constants()
fc2 = phonopy.get_force_constants()
print(fc2.shape)


# phonon rattle
supercell = phonopy_atoms_to_ase(phonopy.get_supercell())

for T in temperatures:
    print(T, flush=True)
    structures = generate_phonon_rattled_structures(supercell, fc2, n_structures, T)
    structures = prepare_structures(structures, supercell, calc)
    write('training_structures/phonon_rattle_T{}.extxyz'.format(T), structures)
