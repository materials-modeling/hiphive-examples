"""
Take MD structures and add some random displacements
"""
import numpy as np
from tools import get_single_calculator
from ase.io import read, write
from hiphive.utilities import prepare_structures

# params
T_vals = [2000, 2500, 3000]
rattle_std_vals = [0.05, 0.1]


# calc
potential_file = '../Ta1_Ravelo_2013.eam.alloy'
calc = get_single_calculator(potential_file, 'Ta', 'eam/alloy')


# run
for T in T_vals:
    fname = 'training_structures/md_T{}.extxyz'.format(T)
    structures = read(fname, index=':')

    f_std = np.std([atoms.get_forces() for atoms in structures])
    f_max = np.max([atoms.get_forces() for atoms in structures])
    f = np.array([atoms.get_forces() for atoms in structures]).flatten()
    for rattle_std in rattle_std_vals:
        print(T, rattle_std)
        new_structures = []
        for atoms in structures:

            # generate displacements
            disp_md = atoms.get_array('displacements')
            disp_rnd = np.random.normal(0, rattle_std, (len(atoms), 3))
            disp = disp_md + disp_rnd

            # setup atoms
            atoms_tmp = atoms.copy()
            atoms_tmp.positions += disp
            atoms_tmp.set_array('displacements', None)
            atoms_tmp.set_array('forces', None)
            new_structures.append(atoms_tmp)

        # write structures
        new_structures = prepare_structures(new_structures, atoms, calc)
        fname_out = 'training_structures/md_T{}_with_rattle-{}.extxyz'.format(T, rattle_std)
        write(fname_out, new_structures)
