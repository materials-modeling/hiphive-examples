import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_context('notebook')


def analyze_times(times):
    err = 2 * times.std() / np.sqrt(len(times))
    row = dict(time_ave=times.mean(), time_std=times.std(), error=err)
    return row

T_vals = [1000, 1500, 2000, 2500, 3000, 3500, 4000]

# read MD data
recs = []
for T in T_vals:
    fname = 'stability_data/times_md_T{}.txt'.format(T)
    if os.path.isfile(fname):
        times = np.loadtxt(fname)
        row = analyze_times(times)
        row['temp'] = T
        recs.append(row)
df_md = pd.DataFrame(recs)


# read ph-rattle data
recs = []
for T in T_vals:
    fname = 'stability_data/times_phonon_rattle_T{}.txt'.format(T)
    if os.path.isfile(fname):
        times = np.loadtxt(fname)
        row = analyze_times(times)
        row['temp'] = T
        recs.append(row)
df_ph = pd.DataFrame(recs)

# read md + rattle
recs = []
for T in T_vals:
    for rattle in [0.05, 0.1]:
        fname = 'stability_data/times_md_T{}_with_rattle-{}.txt'.format(T, rattle)
        if os.path.isfile(fname):
            times = np.loadtxt(fname)
            row = analyze_times(times)
            row['temp'] = T
            row['rattle'] = rattle
            recs.append(row)
df_md_rattle = pd.DataFrame(recs)

# plotting
fig = plt.figure(figsize=(5, 3.2))
ax1 = fig.add_subplot(1, 1, 1)

ax1.errorbar(df_md.temp, df_md.time_ave, fmt='o', yerr=df_md.error, label='MD')
ax1.errorbar(df_ph.temp, df_ph.time_ave, fmt='o', yerr=df_ph.error, label='phonon-rattle')

for rattle in df_md_rattle.rattle.unique():
    df = df_md_rattle[df_md_rattle.rattle == rattle]
    ax1.errorbar(df.temp, df.time_ave, fmt='o', yerr=df.error, label='MD+rattle {}'.format(rattle))

# labels etc
ax1.legend()
ax1.set_xlabel('Temperature')
ax1.set_ylabel('Time to crash (fs)')
ax1.set_ylim(bottom=0.0)

fig.tight_layout()
fig.savefig('pdf/training_structure_stability.pdf')
plt.show()
