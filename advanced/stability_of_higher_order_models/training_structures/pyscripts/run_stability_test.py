import os
import random
import numpy as np

from hiphive import ForceConstantPotential
from hiphive.calculators import ForceConstantCalculator

from ase.io import read
from ase import units
from ase.md.velocitydistribution import MaxwellBoltzmannDistribution
from ase.md.langevin import Langevin


# FCP parameters
key = 'md_T3000_with_rattle-0.05'
key = 'phonon_rattle_T4000'

fcp_fname = 'fcps/fcp_{}.fcp'.format(key)


# MD parameters
size = 6
T = 3000
n_steps_max = 200000
dt = 1.0
n_runs = 100

# setup atoms and calc
prim = read('../prim.xyz')
atoms_ideal = prim.repeat(size)
fcp = ForceConstantPotential.read(fcp_fname)
calc = ForceConstantCalculator(fcp.get_force_constants(atoms_ideal))


# output
data_fname = 'stability_data/times_{}.txt'.format(key)
print(data_fname)

if os.path.isfile(data_fname):
    data = np.loadtxt(data_fname)
else:
    data = []


for i in range(n_runs):

    # setup
    atoms = atoms_ideal.copy()
    atoms.set_calculator(calc)
    seed = random.randint(0, 5000000)
    np.random.seed(seed)
    dyn = Langevin(atoms, dt * units.fs, T * units.kB, 0.02)
    MaxwellBoltzmannDistribution(atoms, T * units.kB)

    # run
    try:
        dyn.run(n_steps_max)
    except Exception:
        time = dt * dyn.get_number_of_steps()
        print(i, time, flush=True)
        data.append(time)
        np.savetxt(data_fname, data)

np.savetxt(data_fname, data)
