import os
from ase.calculators.lammpsrun import LAMMPS
from ase.data import atomic_masses, atomic_numbers


def get_single_calculator(potential_file, atom_type, pair_style,
                          pair_coeff_tag=None, mass=None, keep=False):
    """Returns an ase lammps calculator. Only works for monoatomic systems.

    Parameters
    ----------
    potential_file : str
        path to lammps potential file
    atom_type : str
        Atom type (pair coef tag)
    pair_style : str
        pair_style for lammps (must match potential file)
    pair_coeff_tag : str
        If a specific tag is needed rather than just the atom type
    mass : float
        atomic mass
    keep : bool
        keep tmp files or not (False is recommended unless debugging)

    Returns
    --------
    calc (ase-calculator): ASE lammps calculator

    """
    if not os.path.isfile(potential_file):
        raise ValueError('Potential File not found1')

    if mass is None:
        Z = atomic_numbers[atom_type]
        mass = atomic_masses[Z]
    if pair_coeff_tag is None:
        pair_coeff_tag = atom_type

    potential_file = os.path.abspath(potential_file)
    pair_coeff = ['* * ' + potential_file + ' ' + pair_coeff_tag]
    mass = ['1 %.6f' % mass]
    parameters = {'pair_style': pair_style,
                  'pair_coeff': pair_coeff,
                  'mass': mass}

    calc = LAMMPS(parameters=parameters, keep_tmp_files=keep, keep_alive=False, no_data_file=True)
    return calc
