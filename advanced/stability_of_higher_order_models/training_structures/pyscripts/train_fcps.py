from ase.io import read
from hiphive import ClusterSpace, StructureContainer, ForceConstantPotential
from hiphive.cutoffs import Cutoffs
from trainstation import Optimizer


# params
cutoffs = Cutoffs([[6.0, 6.0, 6.0, 6.0, 6.0]])

# training structure choice
method = 'phonon_rattle'
T = 4000
key = '{}_T{}'.format(method, T)

# read structures
fname = 'training_structures/{}.extxyz'.format(key)
structures = read(fname, index=':')

# setup CS and SC
cs = ClusterSpace(structures[0], cutoffs)
sc = StructureContainer(cs)
for x in structures:
    sc.add_structure(x)
print(sc)

# train
opt = Optimizer(sc.get_fit_data(), fit_method='least-squares', train_size=1.0)
opt.train()
print(opt)

# store fcp
fcp = ForceConstantPotential(cs, opt.parameters)
fcp_fname = 'fcps/fcp_{}.fcp'.format(key)
fcp.write(fcp_fname)
