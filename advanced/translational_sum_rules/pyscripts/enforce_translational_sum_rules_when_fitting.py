import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from hiphive import ForceConstantPotential, StructureContainer
from hiphive.core.translational_constraints import get_translational_constraint_matrix
from trainstation import Optimizer
from tools import compute_dispersion
import seaborn as sns
sns.set_context('paper')


def get_constraint_matrix(cs):
    Ac = get_translational_constraint_matrix(cs)
    return Ac.to_array()


# parameters
lambda_vals = np.logspace(-3, 3, 35)


# train model with analytical sum rules sum rules
sc = StructureContainer.read('structure_containers/sc_sumrules-True.sc')
cs = sc.cluster_space
opt = Optimizer(sc.get_fit_data(), train_size=1.0)
opt.train()
rmse_sumrules = opt.rmse_train

parameters = opt.parameters
fcp_normal = ForceConstantPotential(cs, parameters)
prim = fcp_normal.primitive_structure
q_sumrules, f_sumrules = compute_dispersion(prim, fcp_normal)


# train model without sum rules
sc = StructureContainer.read('structure_containers/sc_sumrules-False.sc')
cs = sc.cluster_space
opt = Optimizer(sc.get_fit_data(), train_size=1.0)
opt.train()
rmse_no_sumrules = opt.rmse_train

parameters = opt.parameters
fcp_normal = ForceConstantPotential(cs, parameters)
prim = fcp_normal.primitive_structure
q_no_sumrules, f_no_sumrules = compute_dispersion(prim, fcp_normal)


# train models with constraints added during fitting
Ac = get_constraint_matrix(cs)
yc = np.zeros((Ac.shape[0]))
A, y = sc.get_fit_data()

rmse_data = []
freqs_data = dict()
for lam in lambda_vals:

    # fit model
    A_full = np.vstack((A, lam * Ac))
    y_full = np.hstack((y, yc))
    opt = Optimizer((A_full, y_full), train_size=1.0, standardize=False)
    opt.train()

    # compute freqs
    parameters_rot = opt.parameters
    fcp = ForceConstantPotential(cs, parameters_rot)
    q, f = compute_dispersion(prim, fcp)
    freqs_data[lam] = q, f

    # compute rmse
    dy = np.dot(A, parameters_rot) - y
    rmse = np.sqrt(np.mean(dy**2))
    rmse_data.append(dict(lam=lam, rmse=rmse))

df_rmse = pd.DataFrame(rmse_data)


# plotting
lw = 2.0
alpha = 0.9
col1 = 'r'
col2 = 'k'

fig = plt.figure(figsize=(8, 3))
ax1 = fig.add_subplot(121)
ax2 = fig.add_subplot(122)

cmap = plt.get_cmap('plasma')
colors = [cmap(i) for i in np.linspace(0, 1, len(freqs_data))]

for ax in [ax1, ax2]:
    for c, (lam, (q, f)) in zip(colors, freqs_data.items()):
        ax.plot(q, f, c=c, lw=lw, alpha=alpha)

    ax.plot(q_no_sumrules, f_no_sumrules,  '--', c=col2, lw=lw)
    ax.plot(np.nan, np.nan, '--', c=col2, lw=lw, label='No   sum-rules')

    ax.plot(q_sumrules, f_sumrules, '--', c=col1, lw=lw)
    ax.plot(np.nan, np.nan, '--', c=col1, lw=lw, label='With sum-rules')

ax1.legend()
ax2.legend()
ax1.set_xlim([0.0, q_sumrules[-1]])
ax2.set_xlim([0.0, 0.06])
ax2.set_ylim([-0.2, 2.5])
ax1.set_ylabel('Frequency (THz)')

# colorbar
norm = plt.Normalize(vmin=np.log10(lambda_vals.min()), vmax=np.log10(lambda_vals.max()))
sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
sm._A = []
cbar = plt.colorbar(sm)
cbar.set_label('log10 (lambda)')

fig.tight_layout()
fig.savefig('pdf/phonon_dipsersion.pdf')

# plot rmse and gamma frequencies vs lambda
col1 = 'k'
col2 = 'tab:blue'
col3 = 'tab:orange'


gamma_freqs = []
for lam, (q, f) in freqs_data.items():
    row = [lam] + f[0].tolist()
    gamma_freqs.append(row)
gamma_freqs = np.array(gamma_freqs)

fig = plt.figure(figsize=(8, 3))
ax1 = fig.add_subplot(121)
ax2 = fig.add_subplot(122)

ax1.semilogx(df_rmse.lam, df_rmse.rmse, '-o', c=col1)
ax1.axhline(y=rmse_sumrules, c=col2, alpha=alpha, ls='--', lw=lw, label='With sum-rules')
ax1.axhline(y=rmse_no_sumrules, c=col3, alpha=alpha, ls='--', lw=lw, label='No   sum-rules')

ax2.semilogx(gamma_freqs[:, 0], gamma_freqs[:, 1:], '-o', c=col1)

for i, fi in enumerate(f_sumrules[0]):
    label = '' if i > 0 else 'With sum-rules'
    ax2.axhline(y=fi, c=col2, alpha=alpha, ls='--', lw=lw, label=label)

for i, fi in enumerate(f_no_sumrules[0]):
    label = '' if i > 0 else 'No   sum-rules'
    ax2.axhline(y=fi, c=col3, alpha=alpha, ls='--', lw=lw, label=label)


ax1.set_xlabel(r'$\lambda$')
ax1.set_ylabel('RMSE')
ax2.set_xlabel(r'$\lambda$')
ax2.set_ylabel('Gamma frequency')

ax1.legend()
ax2.legend()

fig.tight_layout()
fig.savefig('pdf/gamma_frequencies_and_rmse.pdf')
plt.show()


