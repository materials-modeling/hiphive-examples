from ase.io import read
from hiphive import ClusterSpace, StructureContainer
from hiphive.structure_generation import generate_rattled_structures
from hiphive.utilities import prepare_structures
from ase.build import bulk
from ase.calculators.emt import EMT


# parameters
cutoffs = [7.5]
prim = bulk('Al', a=4.0)
supercell = prim.repeat(6)

# training structures
structures = generate_rattled_structures(supercell, 5, 0.15)
structures = prepare_structures(structures, supercell, calc=EMT())


# build clusterspace and structurecontainer
cs = ClusterSpace(prim, cutoffs, acoustic_sum_rules=False)
sc = StructureContainer(cs)
for structure in structures:
    sc.add_structure(structure)
sc.write('structure_containers/sc_sumrules-False.sc')


# build clusterspace and structurecontainer
cs = ClusterSpace(prim, cutoffs, acoustic_sum_rules=True)
sc = StructureContainer(cs)
for structure in structures:
    sc.add_structure(structure)
sc.write('structure_containers/sc_sumrules-True.sc')
