## Anharmonic phonons in BaGaGe

We employ a few different mehtods to study the renormalization of phonons in BaGaGe.

The effective harmonic model, SED and VACF analysis relies on MD trajectories produced using the
fourth order model in the NVE ensemble.

