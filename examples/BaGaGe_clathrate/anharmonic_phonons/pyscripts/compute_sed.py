import numpy as np
from ase.io import read
from hiphive.md_tools import compute_sed


invfs_to_meV = 1000 * 4.13567


# parameters
T = 300
size_x = 40
size = (size_x, 1, 1)
size_str = 'x'.join(str(s) for s in size)


# atoms
prim = read('../dft_calculations/structures/POSCAR_groundstate_vdW-DF-cx')
ideal = prim.repeat(size)

# traj
print('Reading traj')
traj_fname = 'trajs/traj_nve_size{}_T{}.traj@:'.format(size_str, T)
traj = read(traj_fname)


# k-points
a = 10.84440619
k1 = 2.0 * np.pi / a  # inverse angstrom
n_points = size_x + 1
k_end = np.array([k1, 0.0, 0.0])
k_points = np.array([i * k_end for i in np.linspace(0, 1, n_points)])
k_norms = np.linalg.norm(k_points, axis=1)

# frequencies, dt must match timestep between snapshots
dt = 50
max_w = 1/dt * invfs_to_meV
w = np.linspace(0, max_w, len(traj))

# compute sed
print('Computing SED')
sed = compute_sed(traj, ideal, prim, k_points)


# save to file
data_fname = 'data/sed_size{}_T{}.npy'.format(size_str, T)
data_dict = {'k-points': k_points, 'k-norms': k_norms, 'w': w, 'sed': sed}
np.save(data_fname, data_dict)

# plot
import matplotlib.pyplot as plt
clim = [np.log10(np.min(sed)), np.log10(np.max(sed))]

plt.figure(figsize=(5.0, 7.0))
plt.imshow(np.log10(sed[:, :].T), aspect='auto', origin=(0, 0), cmap='plasma')
plt.clim(clim)
plt.colorbar()
plt.xlim([0, size_x/2])
plt.ylim([0, len(w)/2])
plt.show()
