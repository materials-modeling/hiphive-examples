"""
Naive implementation of window averaging for velocity autocorrelation function
"""

import numpy as np
from ase.io.trajectory import TrajectoryReader


# parameters
T = 100
size = (4, 4, 4)


# vacf parameters
dt = 50
window_size = 2000

# read traj
traj_fname = 'trajs/traj_nve_size{}_T{}.traj'.format(size_str, T)
traj = TrajectoryReader(traj_fname)

time = np.arange(0, window_size*dt, dt)

# setup
frames = [traj[i] for i in range(window_size)]
vacf = np.zeros(window_size)

# run window average velocity autocorrelation
frame_ind = window_size
while frame_ind < len(traj):
    print('Frame ind {} of {}'.format(frame_ind, len(traj)))

    v0 = frames[0].get_velocities()

    for t, frame in enumerate(frames):
        v_t = frame.get_velocities()
        vacf[t] += np.mean(np.sum(np.multiply(v0, v_t), axis=1))

    frames.pop(0)
    frames.append(traj[frame_ind])
    frame_ind += 1


# normalize
vacf = vacf / vacf[0]

# save vacf to file
header = 'Col 1: time (fs)\nCol 2: vacf(t)'
fname = 'data/vacf_size{}_T{}_window{}'.format(size_str, T, window_size)
np.savetxt(fname, np.vstack((time, vacf)).T, header=header)
