import numpy as np
from ase.io import read
from ase.io.trajectory import TrajectoryReader
from hiphive import ClusterSpace, StructureContainer, ForceConstantPotential
from hiphive.utilities import get_displacements
from hiphive.fitting import Optimizer


# parameters
T = 300
size = (4, 4, 4)
size_str = 'x'.join(str(s) for s in size)


# read atoms
prim = read('../dft_calculations/structures/POSCAR_groundstate_vdW-DF-cx')
atoms_ideal = prim.repeat(size)
traj_fname = 'trajs/traj_nve_size{}_T{}.traj'.format(size_str, T)
traj = TrajectoryReader(traj_fname)


# setup CS SC
cutoffs = [5.4]
cs = ClusterSpace(prim, cutoffs, symprec=1e-4)
sc = StructureContainer(cs)


# add training structures
n_structures = 25
interval = 20
inds = np.arange(0, n_structures * interval, interval)


for i in inds:
    atoms = traj[i]
    disps = get_displacements(atoms, atoms_ideal)
    max_disp = np.max(np.linalg.norm(disps, axis=1))
    print('max disp', max_disp)
    atoms.new_array('displacements', disps)
    atoms.new_array('forces', atoms.get_forces())
    atoms.positions = atoms_ideal.get_positions()
    
    sc.add_structure(atoms)


# fit
opt = Optimizer(sc.get_fit_data(), train_size=1.0)
opt.train()
print(opt)

# save fcp
fcp = ForceConstantPotential(sc.cluster_space, opt.parameters)
fcp.write('fcps/fcp_ehm_T{}.fcp'.format(T))

