import pickle
from ase.io import read
from hiphive import ClusterSpace, ForceConstantPotential
from hiphive.calculators import ForceConstantCalculator
from hiphive.self_consistent_phonons import self_consistent_harmonic_model
from hiphive.cutoffs import Cutoffs


def write_raw_fit(fname, raw_fit):
    with open(fname, 'wb') as handle:
        pickle.dump(raw_fit, handle, protocol=pickle.HIGHEST_PROTOCOL)


def read_raw_fit(fname):
    with open(fname, 'rb') as handle:
        return pickle.load(handle)


THz2meV = 4.13567

# parameters
harmonic_cutoffs = [5.4]
temperatures = [100, 200, 300]
size = 2

# scp parameters
n_structures = 60
n_iterations = 200
alpha = 0.04

# load fcp
fit_method = 'least-squares'
cutoff_matrix = [[5.4, 4.35, 4.35]]
cutoffs = Cutoffs(cutoff_matrix)
cutoffs_str = cutoffs.to_filename_tag()

fcp_fname = '../construct_models/fcps/fcp_{}_{}.fcp'.format(cutoffs_str, fit_method)
fcp = ForceConstantPotential.read(fcp_fname)


# read atoms and calculator
prim = read('../dft_calculations/structures/POSCAR_groundstate_vdW-DF-cx')
atoms = prim.repeat(size)
calc = ForceConstantCalculator(fcp.get_force_constants(atoms))

# run scph
cs = ClusterSpace(atoms, harmonic_cutoffs, symprec=1e-4)
for T in temperatures:
    print('Running PMSCP {}K'.format(T))
    parameters_traj = self_consistent_harmonic_model(atoms, calc, cs, T, alpha, n_iterations, n_structures)
    fcp = ForceConstantPotential(cs, parameters_traj[-1])
    fcp.write('fcps/fcp_scph_T{}.fcp'.format(T))
    write_raw_fit('data/scph_parameters_T{}.pickle'.format(T), parameters_traj)
