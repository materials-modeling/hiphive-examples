import numpy as np
from ase.io import read
from ase.db import connect
from hiphive.utilities import get_displacements


def collect_pes(index, supercells):
    data_pes = []
    for atoms in supercells:
        wrapped_disps = get_displacements(atoms, atoms_ideal)
        disp = wrapped_disps[index]
        d = np.sign(disp[0]) * np.linalg.norm(disp)
        if np.linalg.norm(d) < 1e-10:
            E0 = atoms.get_potential_energy()
        E = atoms.get_potential_energy()
        data_pes.append((d, E))
    data_pes.sort(key=lambda x: x[0])
    data_pes = np.array(data_pes)
    data_pes[:, 1] -= E0
    return np.array(data_pes)



# parameters
paths = ['100', '110', '111']
data_header = 'Col 1: Displacement (Å)\nCol 2: Potential energy (eV)'

# read atoms
atoms_ideal = read('../dft_calculations/structures/POSCAR_groundstate_vdW-DF-cx')
db = connect('../dft_calculations/structures/barium_pes_vdW-DF-cx.db')


# Ba index 0 
data_file = 'data/pes_Ba_index{}_path{}_dft'
atom_index = 0
for path in paths:
    key = 'Ba_index{}_path{}'.format(atom_index, path)
    supercells = []
    for row in db.select():
        if key in row.filename:
            supercells.append(row.toatoms())
    data_pes = collect_pes(atom_index, supercells)
    np.savetxt(data_file.format(atom_index, path), data_pes, header=data_header)

# Ba index 2
data_file = 'data/pes_Ba_index{}_path{}_dft'
atom_index = 2
for path in paths:
    key = 'Ba_index{}_path{}'.format(atom_index, path)
    supercells = []
    for row in db.select():
        if key in row.filename:
            supercells.append(row.toatoms())
    data_pes = collect_pes(atom_index, supercells)
    np.savetxt(data_file.format(atom_index, path), data_pes, header=data_header)
