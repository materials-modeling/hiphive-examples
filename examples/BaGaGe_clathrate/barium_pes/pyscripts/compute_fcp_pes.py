import glob
import numpy as np
from ase.io import read
from hiphive import ForceConstantPotential, ForceConstants
from hiphive.calculators import ForceConstantCalculator


def compute_displacement_pes(index, supercells, calc):
    data_pes = []
    for atoms in supercells:
        disp = atoms.positions[index] - atoms_ideal.positions[index]
        d = np.sign(disp[0]) * np.linalg.norm(disp)
        atoms.set_calculator(calc)
        E = atoms.get_potential_energy()
        data_pes.append((d, E))
    data_pes.sort(key=lambda x: x[0])
    return np.array(data_pes)


# parameters
paths = ['100', '110', '111']
data_header = 'Col 1: Displacement (Å)\nCol 2: Potential energy (eV)'

# read atoms
atoms_ideal = read('../dft_calculations/structures/POSCAR_groundstate_vdW-DF-cx')

# setup calc for harmonic and full FCP
fcp = ForceConstantPotential.read('../construct_models/fcps/fcp_2body-5.4_4.35_4.35_least-squares.fcp')
fcs_full = fcp.get_force_constants(atoms_ideal)
fcs_second = ForceConstants.from_sparse_dict(fcs_full.get_fc_dict(order=2), fcs_full.supercell)


calc_full = ForceConstantCalculator(fcs_full)
calc_harmonic = ForceConstantCalculator(fcs_second)


# compute pes for Ba index 0
atom_index = 0
data_file = 'data/pes_Ba_index{}_path{}_fcp_{}'
for path in paths:
    for calc_tag, calc in zip(['harmonic', 'full'], [calc_harmonic, calc_full]):
        print('Running', calc_tag, path)
        fnames = glob.glob('structures/Ba_index{}_path{}/*'.format(atom_index, path))
        supercells = [read(fname) for fname in sorted(fnames)]
        data_pes = compute_displacement_pes(atom_index, supercells, calc)
        np.savetxt(data_file.format(atom_index, path, calc_tag), data_pes, header=data_header)


# compute pes for Ba index 2
atom_index = 2
data_file = 'data/pes_Ba_index{}_path{}_fcp_{}'
for path in paths:
    for calc_tag, calc in zip(['harmonic', 'full'], [calc_harmonic, calc_full]):
        print('Running', calc_tag, path)
        fnames = glob.glob('structures/Ba_index{}_path{}/*'.format(atom_index, path))
        supercells = [read(fname) for fname in sorted(fnames)]
        data_pes = compute_displacement_pes(atom_index, supercells, calc)
        np.savetxt(data_file.format(atom_index, path, calc_tag), data_pes, header=data_header)
