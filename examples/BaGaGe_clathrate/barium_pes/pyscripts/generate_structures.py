import os
import numpy as np
from ase.io import read, write


def generate_displaced_structures(index, disp_path):
    supercells = []
    for disp in disp_path:
        atoms = supercell.copy()
        atoms.positions[index] += disp
        supercells.append(atoms)
    return supercells


# parameters
N = 31
d_amp = 0.8
d_values = np.linspace(-d_amp, d_amp, N)

# read atoms
supercell = read('../dft_calculations/structures/POSCAR_groundstate_vdW-DF-cx')
Ba_small_index = 0  # small cage
Ba_large_index = 2  # large cage


# displacement paths
disp_path = dict()
disp_path['100'] = [np.array([d, 0, 0]) for d in d_values]
disp_path['110'] = [np.array([d, d, 0]) for d in d_values/np.sqrt(2.0)]
disp_path['111'] = [np.array([d, d, d]) for d in d_values/np.sqrt(3.0)]


# generate structures
supercells_Ba_small = dict()
supercells_Ba_large = dict()
for key, path in disp_path.items():
    supercells_Ba_small[key] = generate_displaced_structures(Ba_small_index, path)
    supercells_Ba_large[key] = generate_displaced_structures(Ba_large_index, path)


# write Ba small structures
for path_tag, supercells in supercells_Ba_small.items():
    dirname = 'structures/Ba_index{}_path{}/'.format(Ba_small_index, path_tag)
    os.makedirs(dirname, exist_ok=True)
    for i, atoms in enumerate(supercells):
        poscar_fname = dirname + '/POSCAR-{:05d}'.format(i)
        write(poscar_fname, atoms)

# write Ba small structures
fname = 'structures/Ba_index{}_path{}/POSCAR-{:05d}'
for path_tag, supercells in supercells_Ba_large.items():
    dirname = 'structures/Ba_index{}_path{}/'.format(Ba_large_index, path_tag)
    os.makedirs(dirname, exist_ok=True)
    for i, atoms in enumerate(supercells):
        poscar_fname = dirname + '/POSCAR-{:05d}'.format(i)
        write(poscar_fname, atoms)
