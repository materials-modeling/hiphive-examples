from ase.db import connect
from ase.io import read
from hiphive import ClusterSpace, StructureContainer
from hiphive.utilities import get_displacements
from hiphive.cutoffs import Cutoffs

"""
Model 1:
cutoff_matrix = [[5.4, 3.0, 3.0]]

Model 2:
cutoff_matrix = [[5.4, 3.5, 3.5]]

Model 3:
cutoff_matrix = [[5.4, 4.0, 4.0]]

Model 4:
cutoff_matrix = [[5.4, 4.35, 4.35]]

Model 5:
cutoff_matrix = [[5.4, 4.7, 4.7]]

Model 6:
cutoff_matrix = [[5.4, 4.35, 4.35, 3.0, 3.0]]

Model 7:
cutoff_matrix = [[5.4, 4.35, 4.35], [4.0, 4.0, 4.0]]
"""

# cutoffs
cutoff_matrix = [[5.4, 3.0, 3.0]]
cutoffs = Cutoffs(cutoff_matrix)
cutoffs_str = cutoffs.to_filename_tag()
sc_fname = 'structure_containers/sc_{}.sc'.format(cutoffs_str)

# connect to dbs filenames
path = '../dft_calculations/structures//'
mc_db1 = path + 'mc_rattle_std0.042_vdW-DF-cx.db'
md_db1 = path + 'mc_rattle_based_md_T300_vdW-DF-cx.db'
md_db2 = path + 'mc_rattle_based_md_T650_vdW-DF-cx.db'
atoms_ideal = read(path + 'POSCAR_groundstate_vdW-DF-cx')

# collect training structures
training_structures = []
for db_fname in [mc_db1, md_db1, md_db2]:
    db = connect(db_fname)
    supercells = []
    for row in db.select():
        atoms = row.toatoms()
        displacements = get_displacements(atoms, atoms_ideal)
        atoms.new_array('forces', atoms.get_forces())
        atoms.new_array('displacements', displacements)
        atoms.calc = None
        atoms.positions = atoms_ideal.get_positions()
        training_structures.append(atoms)

# build ClusterSpace and StructureContainer
cs = ClusterSpace(atoms_ideal, cutoffs, symprec=1e-4)
sc = StructureContainer(cs)

for atoms in training_structures:
    sc.add_structure(atoms)
sc.write(sc_fname)
