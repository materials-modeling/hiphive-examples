from collections import defaultdict
from hiphive.fitting import CrossValidationEstimator
from hiphive import StructureContainer, ForceConstantPotential
from hiphive.cutoffs import Cutoffs


# cutoffs
cutoff_matrix = [[5.4, 4.35, 4.35]]
cutoffs = Cutoffs(cutoff_matrix)
cutoffs_str = cutoffs.to_filename_tag()


# fit params
fit_method = 'least-squares'
fit_kwargs = defaultdict(dict)
fit_kwargs['rfe'] = dict(step=0.01, n_jobs=1)


# fnames
sc_fname = 'structure_containers/sc_{}.sc'.format(cutoffs_str)
cve_fname = 'cve_data/cve_{}_{}.pickle'.format(cutoffs_str, fit_method)
fcp_fname = 'fcps/fcp_{}_{}.fcp'.format(cutoffs_str, fit_method)

# setup
sc = StructureContainer.read(sc_fname)
cs = sc.cluster_space

# train
cve = CrossValidationEstimator(sc.get_fit_data(), fit_method=fit_method, validation_method='k-fold',
                               n_splits=10, **fit_kwargs[fit_method])
cve.validate()
cve.train()
print(cve)

# save to file
cve.write_summary(cve_fname)
fcp = ForceConstantPotential(cs, cve.parameters)
fcp.write(fcp_fname)
