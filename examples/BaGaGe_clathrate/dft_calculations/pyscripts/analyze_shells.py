from ase.io import read
import matplotlib.pyplot as plt
from hiphive.utilities import get_neighbor_shells

atoms_bgg = read('structures/POSCAR.ground-state_vdW-DF-cx_BGG')
shells_bgg = get_neighbor_shells(atoms_bgg, cutoff=6.5)
Ba_distances = [shell.distance for shell in shells_bgg if 'Ba' in shell.types]


# plot distances
bins = 50
fig = plt.figure(figsize=(5, 3.2))
ax1 = fig.add_subplot(1, 1, 1)
ax1.hist(Ba_distances, bins=bins)
ax1.set_xlabel('Ba-X distances (A)')
ax1.set_ylabel('Counts')


fig.tight_layout()
plt.show()
