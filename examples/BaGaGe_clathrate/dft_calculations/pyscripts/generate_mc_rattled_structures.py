import random
import numpy as np
import matplotlib.pyplot as plt
from ase.io import read, write
from ase.data import chemical_symbols
from hiphive.structure_generation.rattle import mc_rattle


def get_histogram_data(data, bins=100):
    counts, bins = np.histogram(data, bins=bins, density=True)
    bin_centers = [(bins[i+1]+bins[i])/2.0 for i in range(len(bins)-1)]
    return bin_centers, counts


n_structures = 100
rattle = 0.04
minimum_distance = 2.5
max_disp = 1.0
n_iter = 30

out_file = 'rattled_structures/structures_mc_rattle{:.3f}.extxyz'.format(rattle)

# setup atoms and calc
atoms = read('relaxed_structure/POSCAR.ground-state_vdW-DF-cx')

active_atoms = np.where(atoms.numbers == 56)[0]

# generate structures
Ba_indices = np.where(atoms.numbers == 56)[0]
GaGe_indices = np.setdiff1d(range(len(atoms)), Ba_indices)
rattled_structures = []
for i in range(n_structures):
    print(i)
    atoms_rattle = atoms.copy()
    for j in range(n_iter):
        seed = random.randint(0, 1000000000)
        disps = mc_rattle(atoms_rattle, 1.12*rattle, minimum_distance, N_iter=1,
                          max_disp=max_disp, active_atoms=Ba_indices, seed=seed)
        atoms_rattle.positions += disps

        seed = random.randint(0, 1000000000)
        disps = mc_rattle(atoms_rattle, rattle, minimum_distance, N_iter=1,
                          max_disp=max_disp, active_atoms=GaGe_indices, seed=seed)
        atoms_rattle.positions += disps

    rattled_structures.append(atoms_rattle)

# collect distributions
numbers = sorted(set(atoms.numbers))
disp_distributions = {num: [] for num in numbers}
dist_distribution = []
for structure in rattled_structures:
    dist_distribution.extend(structure.get_all_distances(mic=True).flatten())
    disps = np.linalg.norm(structure.positions - atoms.positions, axis=1)
    for num in numbers:
        disp_distributions[num].extend(disps[atoms.numbers == num])


# write
write(out_file, rattled_structures)


# plot distributions
disp_bins = np.linspace(0.0, 1.6, 80)
dist_bins = np.linspace(1.8, 3.0, 120)

figsize = (12, 4.5)
fs = 14
lw = 2.0

fig = plt.figure(figsize=figsize)
ax1 = fig.add_subplot(1, 2, 1)
ax2 = fig.add_subplot(1, 2, 2)

for num, data in disp_distributions.items():
    dist = get_histogram_data(data, disp_bins)
    ax1.plot(dist[0], dist[1], lw=lw, label=chemical_symbols[num])
ax1.set_xlabel('Displacements (Å)', fontsize=fs)

dist = get_histogram_data(dist_distribution, dist_bins)
ax2.plot(dist[0], dist[1], lw=lw)
ax2.set_xlabel('Distances (Å)', fontsize=fs)

for ax in [ax1, ax2]:
    ax.set_ylim(bottom=0.0)
    ax.legend(fontsize=fs)
    ax.tick_params(labelsize=fs)

plt.tight_layout()
plt.savefig('pdf/mc_rattle_displacements.pdf')
plt.show()
