import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import mplpub
import numpy as np

# parameters
paths = ['100', '110', '111']

# read FCP data
Ba_small_pes_fcp = dict()
Ba_small_pes_dft = dict()
Ba_large_pes_fcp = dict()
Ba_large_pes_dft = dict()

for path in paths:
    Ba_small_pes_fcp[path] = np.loadtxt('../barium_pes/data/pes_Ba_index0_path{}_fcp_full'.format(path))
    Ba_small_pes_dft[path] = np.loadtxt('../barium_pes/data/pes_Ba_index0_path{}_dft'.format(path))
    Ba_large_pes_fcp[path] = np.loadtxt('../barium_pes/data/pes_Ba_index2_path{}_fcp_full'.format(path))
    Ba_large_pes_dft[path] = np.loadtxt('../barium_pes/data/pes_Ba_index2_path{}_dft'.format(path))


# plot
mplpub.setup(template='natcom', height=2.4, width=3.2)

gs = gridspec.GridSpec(1, 3)
gs.update(wspace=0.0, hspace=0.0)
ax1 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[1])
ax3 = plt.subplot(gs[2])

col1 = 'goldenrod'
col2 = 'cornflowerblue'
alpha = 0.8
ms = 2.0

for ax, path in zip([ax1, ax2, ax3], paths):
    ax.plot(Ba_small_pes_fcp[path][:, 0], Ba_small_pes_fcp[path][:, 1], color=col1, label='2a')
    ax.plot(Ba_large_pes_fcp[path][:, 0], Ba_large_pes_fcp[path][:, 1], color=col2, label='6d')
    ax.plot(np.nan, np.nan, 'ok', ms=ms, alpha=alpha, label='DFT')
    ax.plot(Ba_small_pes_dft[path][:, 0], Ba_small_pes_dft[path][:, 1], 'o', color=col1, ms=ms, alpha=alpha, label='')
    ax.plot(Ba_large_pes_dft[path][:, 0], Ba_large_pes_dft[path][:, 1], 'o', color=col2, ms=ms, alpha=alpha, label='')
    ax.set_title(r'$\left<{}\right>$'.format(path))

for ax in [ax1, ax2, ax3]:
    ax.set_xlim(-0.8, 0.8)
    ax.set_ylim(0, 1.7)
    ax.set_yticks(np.arange(0, 2, 0.4))

ax1.legend(loc=9, handlelength=1)
ax1.set_ylabel('Energy (eV)')
ax2.set_xlabel(r'Displacement (\AA)')
ax2.set_yticklabels([])
ax3.set_yticklabels([])

plt.tight_layout()
plt.savefig('figs/clathrate_barium_pes_all.pdf')
