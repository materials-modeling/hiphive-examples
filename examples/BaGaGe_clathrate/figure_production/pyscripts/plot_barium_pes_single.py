import matplotlib.pyplot as plt
import mplpub
import numpy as np

# parameters
path = '111'


# Read data
Ba_small_pes_dft = np.loadtxt('../barium_pes/data/pes_Ba_index0_path{}_dft'.format(path))
Ba_small_pes_fcp = np.loadtxt('../barium_pes/data/pes_Ba_index0_path{}_fcp_full'.format(path))
Ba_small_pes_fcp_second = np.loadtxt('../barium_pes/data/pes_Ba_index0_path{}_fcp_harmonic'.format(path))

Ba_large_pes_dft = np.loadtxt('../barium_pes/data/pes_Ba_index2_path{}_dft'.format(path))
Ba_large_pes_fcp = np.loadtxt('../barium_pes/data/pes_Ba_index2_path{}_fcp_full'.format(path))
Ba_large_pes_fcp_second = np.loadtxt('../barium_pes/data/pes_Ba_index2_path{}_fcp_harmonic'.format(path))


# plot
mplpub.setup(template='natcom', height=2.3, width=3.2)  # noqa

fig, ax1 = plt.subplots(1, 1)
alpha = 0.3
col1 = 'goldenrod'
col2 = 'cornflowerblue'

ax1.plot(Ba_small_pes_fcp[:, 0], Ba_small_pes_fcp[:, 1], color=col1, label='FC model Ba (2a)')
ax1.plot(Ba_large_pes_fcp[:, 0], Ba_large_pes_fcp[:, 1], color=col2, label='FC model Ba (6d)')
ax1.plot(Ba_small_pes_dft[:, 0], Ba_small_pes_dft[:, 1], 'o', color=col1)
ax1.plot(Ba_large_pes_dft[:, 0], Ba_large_pes_dft[:, 1], 'o', color=col2)
ax1.plot(np.nan, np.nan, '--k', label='Harmonic')
ax1.plot(np.nan, np.nan, 'ok', label='DFT')

ax1.plot(Ba_small_pes_fcp_second[:, 0], Ba_small_pes_fcp_second[:, 1], '--', color=col1)
ax1.plot(Ba_large_pes_fcp_second[:, 0], Ba_large_pes_fcp_second[:, 1], '--', color=col2)
ax1.fill_between(Ba_small_pes_fcp[:, 0], Ba_small_pes_fcp[:, 1], Ba_small_pes_fcp_second[:, 1], color=col1, alpha=alpha)
ax1.fill_between(Ba_large_pes_fcp[:, 0], Ba_large_pes_fcp[:, 1], Ba_large_pes_fcp_second[:, 1], color=col2, alpha=alpha)

ax1.set_xlim(-0.8, 0.8)
ax1.legend(loc=9, handlelength=3.3)
ax1.set_ylabel('Energy (eV)')
ax1.set_ylim(0.0, 1.5)
ax1.set_xlabel(r'Displacement (\AA)')

plt.tight_layout()
plt.savefig('figs/clathrate_barium_pes_path{}.pdf'.format(path))
