import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from dynasor_pytools.fft_helpers import filon_fft
import mplpub


# constants
THz2meV = 4.13567
invfs2meV = 1000 * 4.13567/(2*np.pi)  # angular_frequency to meV

# parameters
T_vals = [100, 200, 300]

# vacf parameters
size = (4, 4, 4)
size_str = 'x'.join(str(s) for s in size)
t_0 = 10000.0   # fs
t_width = 200


# load phonopy dos
scale = 1 / (54*3 * THz2meV)

dos_phonopy = np.loadtxt('../phonopy_analysis/data/dos_phonopy')
dos_phonopy[:, 1] *= scale

# load scph dos
dos_scph = dict()
for T in T_vals:
    w, dos = np.loadtxt('../anharmonic_phonons/data/dos_scph_T{}_mesh32_dim2'.format(T), unpack=True)
    dos_scph[T] = w, dos * scale

# load ehm dos
dos_ehm = dict()
for T in T_vals:
    w, dos = np.loadtxt('../anharmonic_phonons/data/dos_ehm_T{}_mesh32_dim2'.format(T), unpack=True)
    dos_ehm[T] = w, dos * scale

# load vacf data and fft
dos_vacf = dict()
for T in T_vals:
    t, vacf = np.loadtxt('../anharmonic_phonons/data/vacf_size{}_T{}_window2000'.format(size_str, T), unpack=True)

    # raw dos withfour FFT filter
    w_raw, dos_raw = filon_fft(t, vacf)
    w_raw *= invfs2meV
    dos_raw *= 1.0/np.pi / invfs2meV

    # with fermi-dirac filter
    w, dos = filon_fft(t, vacf, fft_window='fermi-dirac', t_0=t_0, t_width=t_width)
    w *= invfs2meV
    dos *= 1.0/np.pi / invfs2meV

    dos_vacf[T] = w, dos, w_raw, dos_raw


# plot
xlim = [1, 8.0]
ylim = [0.0, 0.079]

mplpub.setup(template='natcom', height=4.2, width=3.2)
gs = gridspec.GridSpec(3, 1)
gs.update(wspace=0.0, hspace=0.0)
ax1 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[1])
ax3 = plt.subplot(gs[2])


# generate 5 colors
colors = {0: 'k', 100: 'cornflowerblue', 200: 'goldenrod', 300: mplpub.tableau['red'], 400: 'k'}


# plot phonopy
ax1.plot(dos_phonopy[:, 0], dos_phonopy[:, 1], '--', color=colors[0], label='0K')
for ax in [ax2, ax3]:
    ax.plot(dos_phonopy[:, 0], dos_phonopy[:, 1], '--', color=colors[0])

# plot scph
for T, (w, dos) in dos_scph.items():
    ax3.plot(w, dos, color=colors[T])

# plot ehm
for T, (w, dos) in dos_ehm.items():
    ax2.plot(w, dos, color=colors[T])

# plot vacf
for T, data in dos_vacf.items():
    w, dos, w_raw, dos_raw = data
    ax1.plot(w, dos, color=colors[T], label='{}K'.format(T))


ax1.legend(loc=1, bbox_to_anchor=(0.27, 0.73))

xlab, ylab = 0.04, 0.82
ax1.text(xlab, ylab, r'a) VACF', transform=ax1.transAxes)
ax2.text(xlab, ylab, r'b) EHM', transform=ax2.transAxes)
ax3.text(xlab, ylab, r'c) SCPH', transform=ax3.transAxes)

for ax in [ax1, ax2, ax3]:
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
ax2.set_ylabel('Density of states')

ax1.set_xticklabels([])
ax2.set_xticklabels([])
ax3.set_xlabel('Frequency (meV)')


plt.tight_layout()
plt.savefig('figs/clathrate_dos.pdf')
plt.show()
