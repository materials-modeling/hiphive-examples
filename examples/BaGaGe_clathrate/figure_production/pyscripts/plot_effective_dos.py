import numpy as np
import matplotlib.pyplot as plt
import mplpub


# constants
THz2meV = 4.13567

# parameters
T_vals = [100, 200, 300, 400, 500, 600]

# load phonopy dos
scale = 1 / (54*3 * THz2meV)
dos_phonopy = np.loadtxt('../phonopy_analysis/data/dos_phonopy')
dos_phonopy[:, 1] *= scale

# load ETOM dos
dos_etom = dict()
fname = '../thermal_conductivity/data/dos_size-2x2x2_T-{}_cutoffs-2body-5.4_4.35_serial-False_mesh32_dim2'
for T in T_vals:
    w, dos = np.loadtxt(fname.format(T), unpack=True)
    dos_etom[T] = w, dos * scale


# plot
mplpub.setup(template='natcom', height=2.5, width=3.2)
xlim = [1, 8.0]
ylim = [0.0, 0.079]

# generate colors
cmap = plt.get_cmap('plasma')
colors = dict()
for i, T in enumerate(T_vals):
    x = i / (len(T_vals))
    colors[T] = cmap(x)
colors[0] = 'k'

colors = {0: 'k',
          100: mplpub.tableau['blue'],
          300: mplpub.tableau['orange'],
          600: mplpub.tableau['red']}


# plotting
fig = plt.figure()
ax1 = fig.add_subplot(111)
lw = 1.2
T_vals_to_plot = [100, 300, 600]

ax1.plot(dos_phonopy[:, 0], dos_phonopy[:, 1], '--', color=colors[0], label='0K')
for T in T_vals_to_plot:
    w, dos = dos_etom[T]
    ax1.plot(w, dos, color=colors[T], label='{}K'.format(T), lw=lw)
ax1.legend(loc=1, bbox_to_anchor=(0.28, 0.95))

ax1.set_xlim(xlim)
ax1.set_ylim(ylim)
ax1.set_ylabel('Density of states')
ax1.set_xlabel('Frequency (meV)')

plt.tight_layout()
plt.savefig('figs/clathrate_effective_dos.pdf')
plt.show()
