import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import mplpub


# constants
THz2meV = 4.13567

# parameters
T_vals = [100, 300, 600]

# load phonopy dos
scale = 1 / (54*3 * THz2meV)
dos_phonopy = np.loadtxt('../phonopy_analysis/data/dos_phonopy')
dos_phonopy[:, 1] *= scale

# load EHM dos
dos_ehm = dict()
fname = '../thermal_conductivity/data/dos_size-2x2x2_T-{}_cutoffs-2body-5.4_4.35_serial-True_mesh32_dim2'
for T in T_vals:
    w, dos = np.loadtxt(fname.format(T), unpack=True)
    dos_ehm[T] = w, dos * scale

# load ETOM dos
dos_etom = dict()
fname = '../thermal_conductivity/data/dos_size-2x2x2_T-{}_cutoffs-2body-5.4_4.35_serial-False_mesh32_dim2'
for T in T_vals:
    w, dos = np.loadtxt(fname.format(T), unpack=True)
    dos_etom[T] = w, dos * scale

# plot
xlim = [1, 8.0]
ylim = [0.0, 0.089]

mplpub.setup(template='natcom', height=4.0, width=3.5)
gs = gridspec.GridSpec(2, 1)
gs.update(wspace=0.0, hspace=0.0)
ax1 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[1])


# generate 5 colors
colors = {0: 'k',
          100: 'tab:purple',
          200: 'cornflowerblue',
          300: 'goldenrod',
          400: mplpub.tableau['red'],
          500: 'y',
          600: 'k',
          }

cmap = plt.get_cmap('plasma')
colors = dict()
for i, T in enumerate(T_vals):
    x = i / (len(T_vals))
    colors[T] = cmap(x)
colors[0] = 'k'


colors = {0: 'k',
          100: mplpub.tableau['blue'],
          300: mplpub.tableau['orange'],
          600: mplpub.tableau['red']}

# plot phonopy
ax1.plot(dos_phonopy[:, 0], dos_phonopy[:, 1], '--', color=colors[0], label='0K')
ax2.plot(dos_phonopy[:, 0], dos_phonopy[:, 1], '--', color=colors[0])

# plot ehm
for T, (w, dos) in dos_ehm.items():
    ax1.plot(w, dos, color=colors[T], label='{}K'.format(T))

# plot etom
for T, (w, dos) in dos_etom.items():
    ax2.plot(w, dos, color=colors[T])

ax1.legend(loc=1, bbox_to_anchor=(0.27, 0.73))

xlab, ylab = 0.04, 0.88
ax1.text(xlab, ylab, r'a) EHM', transform=ax1.transAxes)
ax2.text(xlab, ylab, r'b) ETOM', transform=ax2.transAxes)

for ax in [ax1, ax2]:
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)

ax1.set_ylabel('Density of states')
ax2.set_ylabel('Density of states')

ax1.set_xticklabels([])
ax2.set_xlabel('Frequency (meV)')

plt.tight_layout()
plt.savefig('figs/clathrate_effective_dos_v2.pdf')
plt.show()
