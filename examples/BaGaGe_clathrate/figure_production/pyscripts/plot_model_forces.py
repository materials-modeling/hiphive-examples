import numpy as np
import matplotlib.pyplot as plt
from hiphive.cutoffs import Cutoffs
from hiphive.fitting import read_summary


try:
    import mplpub
    mplpub.setup(template='natcom', height=2.3, width=2.7)  # noqa
except ModuleNotFoundError:
    pass


# parameters
fit_method = 'least-squares'
model_tag = '4'

# models
models = {'1': [[5.4, 3.0, 3.0]],
          '2': [[5.4, 3.5, 3.5]],
          '3': [[5.4, 4.0, 4.0]],
          '4': [[5.4, 4.35, 4.35]],
          '5': [[5.4, 4.7, 4.7]],
          '6': [[5.4, 4.35, 4.35, 3.0, 3.0]],
          '7': [[5.4, 4.35, 4.35], [4.0, 4.0, 4.0]]}


# read data
cutoffs = Cutoffs(models[model_tag])
cutoffs_str = cutoffs.to_filename_tag()
cve_fname = '../construct_models/cve_data/cve_{}_{}.pickle'.format(cutoffs_str, fit_method)
cve_data = read_summary(cve_fname)
scatter_data = cve_data['validation_scatter_data']


# plot
fig, ax = plt.subplots()
col1 = 'cornflowerblue'
col2 = 'goldenrod'
col3 = 'forestgreen'
xlab = 0.05
ylab = 0.88

# scatter plot
ax.set_aspect('equal', adjustable='box')
alpha = 0.4
f_range = [min(np.min(scatter_data.target), np.min(scatter_data.predicted)),
           max(np.max(scatter_data.target), np.max(scatter_data.predicted))]
f_range = [-5, 5]
ax.plot(f_range, f_range, 'k')
ax.plot(scatter_data.target, scatter_data.predicted, 'o', alpha=alpha, ms=1.7, markeredgecolor=None)

ax.text(xlab, ylab, r'b)', transform=ax.transAxes)
ax.set_xlabel(r'Target forces (eV/\AA)')
ax.set_ylabel(r'Predicted forces (eV/\AA)')
ax.set_xlim(f_range)
ax.set_ylim(f_range)

fig.tight_layout()
fig.savefig('figs/clathrate_model_forces.pdf', bbox_inches='tight')
plt.show()
