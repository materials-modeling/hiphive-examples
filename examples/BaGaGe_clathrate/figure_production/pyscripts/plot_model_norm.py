import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from hiphive import ForceConstantPotential
from hiphive.cutoffs import Cutoffs


try:
    import mplpub
    mplpub.setup(template='natcom', height=2.3, width=2.2)  # noqa
except ModuleNotFoundError:
    pass


# parameters
fit_method = 'least-squares'
model_tag = '4'

# models
models = {'1': [[5.4, 3.0, 3.0]],
          '2': [[5.4, 3.5, 3.5]],
          '3': [[5.4, 4.0, 4.0]],
          '4': [[5.4, 4.35, 4.35]],
          '5': [[5.4, 4.7, 4.7]],
          '6': [[5.4, 4.35, 4.35, 3.0, 3.0]],
          '7': [[5.4, 4.35, 4.35], [4.0, 4.0, 4.0]]}


# best model
cutoffs = Cutoffs(models[model_tag])
cutoffs_str = cutoffs.to_filename_tag()
fcp = ForceConstantPotential.read('../construct_models/fcps/fcp_{}_least-squares.fcp'.format(cutoffs_str))
df = pd.DataFrame(fcp.orbit_data)
df2 = df[df.order == 2]
df3 = df[df.order == 3]
df4 = df[df.order == 4]


# plot
fig, ax = plt.subplots()
col1 = 'cornflowerblue'
col2 = 'goldenrod'
col3 = 'forestgreen'
xlab = 0.05
ylab = 0.88

# norm-radius plot
alpha = 0.35
ms = 1.8
ax.semilogy(df2.maximum_distance, df2.force_constant_norm, 'o', alpha=alpha, color=col1, ms=ms, label='')
ax.semilogy(df3.maximum_distance, df3.force_constant_norm, '^', alpha=alpha, color=col2, ms=ms, label='')
ax.semilogy(df4.maximum_distance, df4.force_constant_norm, 'd', alpha=alpha, color=col3, ms=ms, label='')
ax.semilogy(np.nan, np.nan, 'o', color=col1, label='2nd order', ms=1.7*ms)
ax.semilogy(np.nan, np.nan, '^', color=col2, label='3rd order', ms=1.7*ms)
ax.semilogy(np.nan, np.nan, 'd', color=col3, label='4th order', ms=1.7*ms)

ax.text(xlab, ylab, r'c)', transform=ax.transAxes)
ax.set_ylabel(r'Norm of $n$-th order FCs (eV/\AA$^n$)')
ax.set_xlabel(r'Distance (\AA)')
ax.set_xlim(-0.2, 5.6)
ax.set_ylim(0.02, 200)
ax.legend(loc=3, handlelength=2)

fig.tight_layout()
fig.savefig('figs/clathrate_model_norm.pdf', bbox_inches='tight')
plt.show()
