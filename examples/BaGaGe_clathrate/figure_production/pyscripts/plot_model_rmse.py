import numpy as np
import matplotlib.pyplot as plt
from hiphive.cutoffs import Cutoffs
from hiphive.fitting import read_summary


try:
    import mplpub
    mplpub.setup(template='natcom', height=2.3, width=2.5)  # noqa
except ModuleNotFoundError:
    pass


# parameters
fit_method = 'least-squares'


# models
models = {'1': [[5.4, 3.0, 3.0]],
          '2': [[5.4, 3.5, 3.5]],
          '3': [[5.4, 4.0, 4.0]],
          '4': [[5.4, 4.35, 4.35]],
          '5': [[5.4, 4.7, 4.7]],
          '6': [[5.4, 4.35, 4.35, 3.0, 3.0]],
          '7': [[5.4, 4.35, 4.35], [4.0, 4.0, 4.0]]}

# read data
data = dict()
for model_tag, cutoff_matrix in models.items():
    cutoffs = Cutoffs(cutoff_matrix)
    cutoffs_str = cutoffs.to_filename_tag()
    cve_fname = '../construct_models/cve_data/cve_{}_{}.pickle'.format(cutoffs_str, fit_method)
    data[model_tag] = read_summary(cve_fname)


# plot
fig, ax = plt.subplots()
col1 = 'cornflowerblue'
col2 = 'goldenrod'
col3 = 'forestgreen'
xlab = 0.05
ylab = 0.88

# rmse plot
tags = list(data.keys())
xvals = np.arange(len(tags))
rmse_test = [data[tag]['rmse_validation'] for tag in tags]
nzp = [data[tag]['n_nonzero_parameters'] for tag in tags]

ax2 = ax.twinx()
ax.bar(xvals, 1e3 * np.array(rmse_test), color=col1)
ax2.plot(xvals, nzp, '-s', color=col3)

ax.text(xlab, ylab, r'a)', transform=ax.transAxes)
ax.set_xlabel('Model')
ax.set_xticks(range(0, 7))
ax.set_xticklabels(range(1, 8))
ax.set_ylabel(r'RMSE (meV/\AA)', color=col1)
ax2.set_ylabel('Number of features', color=col3)
ax.tick_params('y', colors=col1)
ax2.tick_params('y', colors=col3)
ax.set_xlim(-0.75, 6.75)
ax.set_ylim(0, 225)
ax2.set_ylim(0, 14000)

fig.tight_layout()
fig.savefig('figs/clathrate_model_rmse.pdf', bbox_inches='tight')
plt.show()
