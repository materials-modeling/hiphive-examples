import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import mplpub


# read data
data_100 = np.load('../anharmonic_phonons/data/sed_size40x1x1_T100.npy', allow_pickle=True).item()
sed_100 = data_100['sed']
w_100 = data_100['w']

data_300 = np.load('../anharmonic_phonons/data/sed_size40x1x1_T300.npy', allow_pickle=True).item()
sed_300 = data_300['sed']
w_300 = data_300['w']

# smoothing
window_size = 20
sed_smooth_100 = []
for i in range(sed_100.shape[0]):
    series = pd.Series(sed_100[i, :])
    sed_smooth_100.append(series.rolling(window_size, win_type='blackman', center=True, min_periods=1).mean())
sed_smooth_100 = np.array(sed_smooth_100)
print('smoothing dw', window_size * (w_100[1]-w_100[0]))

window_size = 50
sed_smooth_300 = []
for i in range(sed_300.shape[0]):
    series = pd.Series(sed_300[i, :])
    sed_smooth_300.append(series.rolling(window_size, win_type='blackman', center=True, min_periods=1).mean())
sed_smooth_300 = np.array(sed_smooth_300)
print('smoothing dw', window_size * (w_300[1]-w_300[0]))


# sanity plot a slice for a given k-point
k_ind = 0

fig = plt.figure(figsize=(5.0, 6.0))
ax1 = fig.add_subplot(2, 1, 1)
ax2 = fig.add_subplot(2, 1, 2)

ax1.plot(w_100, sed_100[k_ind, :], label='100K raw')
ax2.plot(w_100, sed_smooth_100[k_ind, :], label='100K smooth')
ax1.plot(w_300, sed_300[k_ind, :], label='300K raw')
ax2.plot(w_300, sed_smooth_300[k_ind, :], label='300K smooth')

for ax in [ax1, ax2]:
    ax.set_xlim([0, 20])
    ax.legend()
ax2.set_xlabel('Frequency (meV)')
fig.tight_layout()


# Real SED Heatmap plot
cmap = 'GnBu'
xlim = [0, 1.0/2]
ylim = [0, 20]
clim = [np.log10(np.min(sed_smooth_100)), np.log10(np.max(sed_smooth_100))]

mplpub.setup(template='natcom', height=3.3, width=3.4)  # noqa
fig = plt.figure()
ax1 = fig.add_subplot(1, 2, 1)
ax2 = fig.add_subplot(1, 2, 2)

im1 = ax1.imshow(np.log10(sed_smooth_100.T), aspect='auto', origin=(0, 0), cmap=cmap, extent=[0, 1, 0, w_100.max()])
im2 = ax2.imshow(np.log10(sed_smooth_300.T), aspect='auto', origin=(0, 0), cmap=cmap, extent=[0, 1, 0, w_300.max()])
im1.set_clim(clim)
im2.set_clim(clim)

ax1.set_title('100K')
ax2.set_title('300K')
ax1.set_ylabel('Frequency (meV)')
ax1.set_yticks(np.arange(0, 40, 5))
ax2.set_yticklabels([])

for ax in [ax1, ax2]:
    ax.set_xlabel('Wave vector')
    ax.set_xlim(xlim)
    ax.set_xticks(xlim)
    ax.set_xticklabels([r'$\UG{\Gamma}$', '$\pi/a$'])
    ax.set_yticks(np.arange(0, 50, 5))
    ax.set_ylim(ylim)

cbaxes = fig.add_axes([0.88, 0.118, 0.03, 0.82])
cbar = plt.colorbar(im1, cax=cbaxes)
cbar.set_label('log$_{10}$(SED)')

fig.tight_layout(pad=0.4, h_pad=0.7, rect=[0, 0, 0.9, 1])
fig.savefig('figs/clathrate_sed_size40x1x1.pdf')
plt.show()
