import numpy as np
import matplotlib.pyplot as plt
import mplpub
from scipy.optimize import curve_fit


def f(x, a, b):
    return a / x**b


def log_f(x, a, b):
    return np.log(a) - b * np.log(x)


# read data
BGG_cx_tmp = np.loadtxt('../thermal_conductivity/extra_data/BGG.cx.tmp', unpack=True)
CX_T, CX_k = BGG_cx_tmp[0], BGG_cx_tmp[1]

hiphive = np.loadtxt('../thermal_conductivity/extra_data/BGG.HiPhive', unpack=True)
hiphive_T, hiphive_k = hiphive[0], hiphive[1]

T, k, ks = np.loadtxt('../thermal_conductivity/data/kappa_fourth_order_effective_models', unpack=True)

# colors
col1 = 'cornflowerblue'
col2 = 'goldenrod'
col3 = 'forestgreen'
col2 = 'tab:red'

# fit line to old effective model kappa

popt_old, _ = curve_fit(log_f, hiphive_T, np.log(hiphive_k))
popt_new, _ = curve_fit(log_f, T, np.log(k))
popt_new_s, _ = curve_fit(log_f, T, np.log(ks))

x = np.linspace(45, 850, num=20, endpoint=True)
print(popt_old)
print(popt_new)
print(popt_new_s)


# plotting
mplpub.setup(template='natcom', height=2.6, width=3.5)

fig = plt.figure()
ax1 = fig.add_subplot(111)

ax1.loglog(CX_T, CX_k, lw=2, c=col1, label='0K FCs (Lindroth 2019)')

ax1.loglog(x, f(x, *popt_old), '-', lw=1.5, c='k', alpha=0.75)
ax1.loglog(x, f(x, *popt_new), '-', lw=1.5, c=col2, alpha=0.75)

ax1.scatter(hiphive_T, hiphive_k, zorder=100, marker='s', s=50, edgecolors='k', facecolor='None', label='TDIFCs (Lindroth 2019)')
ax1.scatter(T, k, marker='o', zorder=100, s=60, edgecolors=col2, facecolor='None', label='TDIFCs present work')


ax1.text(70, 3.8, r"$\sim T^{-0.69}$", color='k')
ax1.text(350, 1.7, r"$\sim T^{-0.54}$", color=col2)

ax1.set_xlim([10, 1000])
ax1.set_ylim([0.9e-1, 50])
ax1.legend()
ax1.set_xlabel('Temperature (K)')
ax1.set_ylabel('Lattice thermal conductivity (W/mK)')

fig.tight_layout()
fig.savefig('figs/clathrate_thermal_conductivity.pdf')

plt.show()
