import numpy as np
import matplotlib.pyplot as plt
import mplpub
from scipy.optimize import curve_fit


def f(x, a, b):
    return a / x**b


def log_f(x, a, b):
    return np.log(a) - b * np.log(x)


# read data
BGG_cx_tmp = np.loadtxt('../thermal_conductivity/extra_data/BGG.cx.tmp', unpack=True)
CX_T, CX_k = BGG_cx_tmp[0], BGG_cx_tmp[1]

hiphive = np.loadtxt('../thermal_conductivity/extra_data/BGG.HiPhive', unpack=True)
hiphive_T, hiphive_k = hiphive[0], hiphive[1]

T, k, ks = np.loadtxt('../thermal_conductivity/data/kappa_fourth_order_effective_models', unpack=True)

# read exp data
d = '../thermal_conductivity/extra_data/'
Sal = np.loadtxt(d + "SalChaTho01_fig8_BaGaGe.dat", unpack=True)
HouA = np.loadtxt(d + "data.HouZhoWan09_figure4_kappaL_crystalA", unpack=True)


# colors
col1 = 'cornflowerblue'
col2 = 'goldenrod'
col3 = 'forestgreen'
col2 = 'tab:red'

# fit line to old effective model kappa
popt_old, _ = curve_fit(log_f, hiphive_T, np.log(hiphive_k))
popt_new, _ = curve_fit(log_f, T, np.log(k))
popt_0K, _ = curve_fit(log_f, CX_T[20:], np.log(CX_k)[20:])

x = np.linspace(45, 850, num=20, endpoint=True)
print(popt_old)
print(popt_new)
print(popt_0K)


# plotting
mplpub.setup(template='natcom', height=2.2, width=3.2)

fig = plt.figure()
ax1 = fig.add_subplot(111)

cexp04 = '#9d9754'

ax1.plot(Sal[0],   Sal[1], '--', lw=2.0, c=cexp04,   zorder=10, label='Experimental')
ax1.plot(HouA[0],   HouA[1], '--', lw=2.0, c=cexp04, zorder=10)
ax1.loglog(CX_T, CX_k, lw=2, c=col1, label='0K FCs')

ax1.loglog(x, f(x, *popt_new), '-', lw=1.5, c=col2, alpha=0.75)
ax1.scatter(T, k, marker='o', zorder=100, s=60, edgecolors=col2, facecolor='None', label='Effective FCs')
ax1.scatter(T, ks, marker='d', zorder=100, s=80, edgecolors=col2, facecolor='None', label='Effective serial FCs')
ax1.text(100, 2.5, r"$\sim T^{-0.54}$", color=col2)

ax1.text(100, 0.4, r"$\sim T^{-1}$", color=col1)

ax1.set_xlim([30, 1000])
ax1.set_ylim([0.8e-1, 6])
ax1.legend()
ax1.set_xlabel('Temperature (K)')
ax1.set_ylabel('Thermal conductivity (W/mK)')

fig.tight_layout()
fig.savefig('figs/clathrate_thermal_conductivity_v2.pdf')

plt.show()
