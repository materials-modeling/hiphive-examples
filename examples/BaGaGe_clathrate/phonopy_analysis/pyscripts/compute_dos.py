import numpy as np
from ase import Atoms
from ase.io import read
from hiphive import ForceConstants
from phonopy import Phonopy
from phonopy.structure.atoms import PhonopyAtoms

# parameters
mesh = 32
dim = 2
sigma = 0.02
THz2meV = 4.13567

f_max = 2
f_min = 0.0
delta_f = 0.002

# setup phonopy
prim = read('../dft_calculations/structures/POSCAR_groundstate_vdW-DF-cx')
atoms_phpy = PhonopyAtoms(symbols=prim.get_chemical_symbols(),
                          scaled_positions=prim.get_scaled_positions(),
                          cell=prim.cell)
phonopy = Phonopy(atoms_phpy, supercell_matrix=dim * np.eye(3),
                  primitive_matrix=None)

# read fcs
supercell = phonopy.get_supercell()
supercell = Atoms(cell=supercell.cell, numbers=supercell.numbers, pbc=True,
                  scaled_positions=supercell.get_scaled_positions())

fcs = ForceConstants.read_phonopy(supercell, 'fcs/FORCE_CONSTANTS_2ND.BGG.cx.2x2x2', format='text')
fc2 = fcs.get_fc_array(order=2)
phonopy.set_force_constants(fc2)

# get dos
phonopy.set_mesh([mesh, mesh, mesh])
phonopy.set_total_DOS(sigma=sigma, freq_pitch=delta_f, freq_min=f_min, freq_max=f_max, tetrahedron_method=False)
w, dos = phonopy.get_total_DOS()
w *= THz2meV

# save
header = 'Col 1: Frequency (meV)\nCol 2: DOS'
np.savetxt('data/dos_phonopy', np.vstack((w, dos)).T, header=header)
