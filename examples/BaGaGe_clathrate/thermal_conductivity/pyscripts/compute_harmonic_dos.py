import numpy as np
from ase.io import read
from ase import Atoms
from hiphive import ForceConstantPotential
from phonopy import Phonopy
from phonopy.structure.atoms import PhonopyAtoms
from hiphive.cutoffs import Cutoffs



# parameters
T = 300
serial = True
size = (2, 2, 2)
cutoffs = Cutoffs([[5.4, 4.35]])
cutoffs_str = cutoffs.to_filename_tag()

for T in [100, 200, 300, 400, 500, 600]:

    # fnames
    size_str = 'x'.join(str(s) for s in size)
    tag = 'size-{}_T-{}_cutoffs-{}'.format(size_str, T, cutoffs_str)

    fcp_fname = 'fcps/fcp_{}_serial-{}.fcp'.format(tag, serial)
    fcp = ForceConstantPotential.read(fcp_fname)

    # phonopy dos parameters
    mesh = 32
    dim = 2
    sigma = 0.02
    THz2meV = 4.13567

    f_max = 2
    f_min = 0.0
    delta_f = 0.002

    dos_fname = 'data/dos_{}_serial-{}_mesh{}_dim{}'.format(tag, serial, mesh, dim)

    # setup phonopy
    prim = read('../dft_calculations/structures/POSCAR_groundstate_vdW-DF-cx')
    atoms_phpy = PhonopyAtoms(symbols=prim.get_chemical_symbols(),
                              scaled_positions=prim.get_scaled_positions(),
                              cell=prim.cell)
    phonopy = Phonopy(atoms_phpy, supercell_matrix=dim * np.eye(3), primitive_matrix=None)
    supercell = phonopy.get_supercell()
    supercell = Atoms(cell=supercell.cell, numbers=supercell.numbers, pbc=True,
                      scaled_positions=supercell.get_scaled_positions())

    # get fc2 matrix from fcp
    fcs = fcp.get_force_constants(supercell)
    fc2 = fcs.get_fc_array(order=2)
    phonopy.set_force_constants(fc2)

    # get dos
    phonopy.set_mesh([mesh, mesh, mesh])
    phonopy.set_total_DOS(sigma=sigma, freq_pitch=delta_f, freq_min=f_min, freq_max=f_max, tetrahedron_method=False)
    w, dos = phonopy.get_total_DOS()
    w *= THz2meV

    # save
    header = 'Col 1: Frequency (meV)\nCol 2: DOS'
    np.savetxt(dos_fname, np.vstack((w, dos)).T, header=header)
