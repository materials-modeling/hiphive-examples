import os
import numpy as np
from ase.io import read
from hiphive import ClusterSpace, StructureContainer, ForceConstantPotential
from hiphive.utilities import get_displacements
from hiphive.fitting import Optimizer
from hiphive.cutoffs import Cutoffs


def regular_fit(sc):
    opt = Optimizer(sc.get_fit_data(), train_size=1.0, check_condition=False)
    opt.train()
    print(opt)
    parameters = opt.parameters
    return parameters, opt.summary


def serial_fit(sc):

    # setup
    cs = sc._cs
    A, y = sc.get_fit_data()

    n_cols_2 = cs.get_n_dofs_by_order(order=2)
    n_cols_3 = cs.get_n_dofs_by_order(order=3)
    n_cols_tot = cs.n_dofs
    assert n_cols_tot == n_cols_2 + n_cols_3
    assert n_cols_tot == A.shape[1]

    # fit using only second order parameters
    opt_2 = Optimizer((A[:, :n_cols_2], y), train_size=1.0)
    opt_2.train()
    print(opt_2)
    y_res = y - opt_2.predict(A[:, :n_cols_2])
    parameters_2 = opt_2.parameters


    # fit using both second and third
    opt_3 = Optimizer((A[:, n_cols_2:], y_res), train_size=1.0)
    opt_3.train()
    print(opt_3)
    parameters_3 = opt_3.parameters

    # concatenate parameters
    assert len(parameters_2) == n_cols_2
    assert len(parameters_3) == n_cols_3

    parameters = np.zeros(n_cols_tot)
    parameters[:n_cols_2] = parameters_2
    parameters[n_cols_2:] = parameters_3

    return parameters, opt_3.summary


# parameters
T = 500
serial = False
size = (2, 2, 2)
cutoffs = Cutoffs([[5.4, 4.35]])
cutoffs_str = cutoffs.to_filename_tag()


# fnames
size_str = 'x'.join(str(s) for s in size)
tag = 'size-{}_T-{}_cutoffs-{}'.format(size_str, T, cutoffs_str)

cs_fname = 'cs/cs_cutoffs-{}.cs'.format(cutoffs_str)
sc_fname = 'scs/sc_{}.sc'.format(tag)
fcp_fname = 'fcps/fcp_{}_serial-{}.fcp'.format(tag, serial)


# load atoms
atoms_ideal = read('../dft_calculations/structures/POSCAR_groundstate_vdW-DF-cx')
atoms_ideal = atoms_ideal.repeat(size)

traj_fname = 'trajs/traj_nve_size{}_T{}.traj'.format(size_str, T)
traj = read(traj_fname, index=':')

# setup CS
if os.path.isfile(cs_fname):
    cs = ClusterSpace.read(cs_fname)
else:
    cs = ClusterSpace(atoms_ideal, cutoffs, symprec=1e-4)
    cs.write(cs_fname)

# setup SC
if os.path.isfile(sc_fname):
    sc = StructureContainer.read(sc_fname)
    assert len(sc) == len(traj)
else:
    sc = StructureContainer(cs)
    for atoms in traj:
        atoms_tmp = atoms_ideal.copy()
        disps = get_displacements(atoms, atoms_ideal)
        atoms_tmp.new_array('displacements', disps)
        atoms_tmp.new_array('forces', atoms.get_forces())
        sc.add_structure(atoms_tmp)
    sc.write(sc_fname)


# fitting
if serial:
    parameters, metadata = serial_fit(sc)
else:
    parameters, metadata = regular_fit(sc)
    
# construct fcp
fcp = ForceConstantPotential(cs, parameters, metadata=metadata)
fcp.write(fcp_fname)