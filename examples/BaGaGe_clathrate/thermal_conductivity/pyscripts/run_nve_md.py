from ase.io import read
from ase import units
from ase.io.trajectory import Trajectory
from ase.md.verlet import VelocityVerlet
from ase.md import MDLogger
from hiphive import ForceConstantPotential
from hiphive.calculators import ForceConstantCalculator
from hiphive.cutoffs import Cutoffs


# MD params
T = 100
size = (2, 2, 2)
size_str = 'x'.join(str(s) for s in size)
dt = 1  # in fs
n_sample = 5000
interval = 50

# output files
traj_file = 'trajs/traj_nve_size{}_T{}.traj'.format(size_str, T)
log_file = 'logs/log_nve_size{}_T{}'.format(size_str, T)

# load atoms
atoms_ideal = read('../dft_calculations/structures/POSCAR_groundstate_vdW-DF-cx')
atoms_ideal = atoms_ideal.repeat(size)

# start from last snapshot in nvt run
atoms = read('trajs/traj_nvt_size{}_T{}.traj@-1'.format(size_str, T))

# load fcp
fit_method = 'least-squares'
cutoff_matrix = [[5.4, 4.35, 4.35]]
cutoffs = Cutoffs(cutoff_matrix)
cutoffs_str = cutoffs.to_filename_tag()

fcp_fname = '../construct_models/fcps/fcp_{}_{}.fcp'.format(cutoffs_str, fit_method)
fcp = ForceConstantPotential.read(fcp_fname)

# setup calculator
calc_fcp = ForceConstantCalculator(fcp.get_force_constants(atoms_ideal))
atoms.set_calculator(calc_fcp)

# setup md
dyn = VelocityVerlet(atoms, dt * units.fs)
logger = MDLogger(dyn, atoms, log_file, header=True, stress=False, peratom=True, mode='w')
traj_writer = Trajectory(traj_file, 'w', atoms)
dyn.attach(logger, interval=interval)
dyn.attach(traj_writer.write, interval=interval)

# run md
dyn.run(n_sample)
