Examples
========
In this repository fully fleshed out examples of hiphive applications is kept.
This includes

* 2nd and 3rd order force constants for Si diamond for thermal condutivity
* 4th order model for clathrate BaGaGe used for analysing renormalization of phonon frequencies.



Reproducability
---------------
Hiphive version 0.5 was used to carry out all the analysis.
python 3.6


Si diamond
----------
The main purpose of this example is to show how to construct second and third order force constants to be used for computing thermal condutivity using BTE.

We explore how cutoffs, number of training structures and fitting method influence model accuracy and thermal condutivity. The obtained thermal condutivity is also compared to a full phono3py calculation.


Clathrate BGG
-------------
In this example we explore how to construct higher order force constant model for the BaGaGe clathrate.
The higher order force constant models are used to sample various anharmonic properties.