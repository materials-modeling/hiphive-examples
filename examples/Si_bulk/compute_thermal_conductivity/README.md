Thermal condutivity calculations
=================================
The scripts for computing thermal condutivity is found in ``pyscripts``.
These scripts sets up a working directory to which POSCAR and force constant files are copied to. Then a regular phono3py calculation is run.