"""
Collect all kappa data from phono3py hdf5 and write it to clean data files
"""
import os
import glob
import h5py
import numpy as np


def read_hdf5_kappa(fname):
    with h5py.File(fname, 'r') as hf:
        temperatures = hf['temperature'][:]
        kappa = hf['kappa'][:][:, 0]  # xx
    return temperatures, kappa


def write_to_text(fname, t, k):
    header = 'col 1: Temperature (K)\ncol 2: Thermal conductivity (W/mK)'
    data = np.vstack((t, k)).T
    np.savetxt(fname, data, header=header)


# hiphive data
fnames = glob.glob('kappa_data/kappa_hiphive_*.hdf5')
for fname in fnames:
    tag = os.path.basename(fname).replace('.hdf5', '')
    t, k = read_hdf5_kappa(fname)
    output_fname = 'data/' + os.path.basename(fname).replace('.hdf5', '')
    write_to_text(output_fname, t, k)


# phono3py data
fname = 'kappa_data/kappa_phono3py_mesh32.hdf5'
output_fname = 'data/kappa_phono3py_mesh32'
t, k = read_hdf5_kappa(fname)
write_to_text(output_fname, t, k)

