import os
import shutil
import subprocess
from ase.io import read
from hiphive import ForceConstantPotential


# phonopy arameters
dim = 5
mesh = 32
T_min = 0
T_max = 1500
T_step = 10

# fcp parameters
fit_method = 'least-squares'
n_train_structures = 5
cutoffs = [9.65, 9.65, 2.5]
serial_fitting = False

cutoffs_str = str(cutoffs)[1:-1].replace(', ', '_')
serial_fitting_str = '_serial' if serial_fitting else ''
fcp_tag = '{}_cutoffs-{}_trainsize-{}{}'.format(fit_method, cutoffs_str, n_train_structures, serial_fitting_str)


# read fcp
fcp_fname = '../construct_hiphive_force_constants/fcps/fcp_{}_split-0.fcp'.format(fcp_tag)
fcp = ForceConstantPotential.read(fcp_fname)

# generate fcs
print('Getting force constants')
sposcar = read('../construct_phono3py_force_constants/structures/SPOSCAR_dim{}'.format(dim))
fcs = fcp.get_force_constants(sposcar)

# setup working dir
work_dir = 'kappa_calculation_hiphive_{}_mesh{}/'.format(fcp_tag, mesh)
shutil.rmtree(work_dir, ignore_errors=True)
os.makedirs(work_dir, exist_ok=True)
os.chdir(work_dir)

# write fcs files and POSCAR into work dir
fcs.write_to_phonopy('fc2.hdf5')
fcs.write_to_phono3py('fc3.hdf5')
shutil.copy('../../construct_phono3py_force_constants/structures/POSCAR_prim', 'POSCAR')

# call phono3py and compute thermal condutivity kappa
cmd = 'phono3py --dim=\"{0} {0} {0}\" --mesh=\"{1} {1} {1}\" --fc2 --fc3 --br --tmax {2} --tmin {3} --tstep {4}'.format(dim, mesh, T_max, T_min, T_step)
subprocess.call(cmd, shell=True)

kappa_fname = '../kappa_data/kappa_hiphive_{}_mesh{}.hdf5'.format(fcp_tag, mesh)
shutil.move('kappa-m{0}{0}{0}.hdf5'.format(mesh), kappa_fname)

# remove working dir
os.chdir('..')
shutil.rmtree(work_dir)
