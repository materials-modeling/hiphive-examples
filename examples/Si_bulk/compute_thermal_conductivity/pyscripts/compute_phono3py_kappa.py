import os
import shutil
import subprocess


# Parameters
dim = 5
mesh = 32
T_min = 0
T_max = 1500
T_step = 10

# setup tmp work dir
work_dir = 'kappa_calculation_phono3py_mesh{}/'.format(mesh)
shutil.rmtree(work_dir, ignore_errors=True)
os.makedirs(work_dir, exist_ok=True)
os.chdir(work_dir)

# file names
path = '../../construct_phono3py_force_constants/phono3py_calculation/'
poscar_fname = os.path.join(path, 'POSCAR')
fc2_fname = os.path.join(path, 'fc2.hdf5')
fc3_fname = os.path.join(path, 'fc3.hdf5')

# copy POSCAR and fcs into working dir
shutil.copy(poscar_fname, 'POSCAR')
shutil.copy(fc2_fname, 'fc2.hdf5')
shutil.copy(fc3_fname, 'fc3.hdf5')

# call phono3py and compute thermal condutivity kappa
cmd = 'phono3py --dim=\"{0} {0} {0}\" --mesh=\"{1} {1} {1}\" --fc2 --fc3 --br --tmax {2} --tmin {3} --tstep {4}'.format(dim, mesh, T_max, T_min, T_step)
subprocess.call(cmd, shell=True)

kappa_fname = '../kappa_data/kappa_phono3py_mesh{}.hdf5'.format(mesh)
shutil.move('kappa-m{0}{0}{0}.hdf5'.format(mesh), kappa_fname)

# remove working dir
os.chdir('..')
shutil.rmtree(work_dir)
