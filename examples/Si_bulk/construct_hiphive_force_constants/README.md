Fitting
=======
The stucture containers must be first be prepared via `setup_structure_container.py`.

In order to always use the same training and validation set splits no matter fit method or cutoffs these are precomputed using the `generate_cv_splits.py` script and written to a pickle file.


Learning curves
----------------
The learning curves, i.e. training size convergences curves, are computed with the `compute_learning_curves.py`.


Cutoff convergence
-------------------
The convergence with respect to cutoff can be studied with the `compute_cutoff_convergence.py`.