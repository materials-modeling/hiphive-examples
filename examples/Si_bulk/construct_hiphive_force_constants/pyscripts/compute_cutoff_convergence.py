import os
import pandas as pd
import numpy as np

from hiphive import StructureContainer, ForceConstantPotential
from fit_tools import run_cv_analysis


# parameters
fit_method = 'least-squares'
n_train_structures = 5
third_order_cutoffs = [3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 8.5, 9.0, 9.65]
serial_fitting = False

serial_fitting_str = '_serial' if serial_fitting else ''


# setup pandas dataframe for cutoff convergence
df_fname = 'data/df_cutoff-convergence_{}_trainsize-{}{}.pickle'.format(fit_method, n_train_structures, serial_fitting_str)
if os.path.isfile(df_fname):
    df = pd.read_pickle(df_fname)
else:
    columns = ['third_order_cutoff', 'rmse_train_ave', 'rmse_train_std',
               'rmse_test_ave', 'rmse_test_std', 'n_nzp_ave', 'n_nzp_std']
    df = pd.DataFrame(columns=columns)


# run convergence
for cutoff in third_order_cutoffs:

    # if this cutoff already exists, skip
    if cutoff in df.third_order_cutoff.values:
        print('third_order_cutoff {} already calculated, skipping'.format(cutoff), flush=True)
        continue

    # read sc
    cutoffs = [9.65, cutoff, 2.5]
    cutoffs_str = str(cutoffs)[1:-1].replace(', ', '_')
    sc_fname = 'structure_containers/sc_cutoffs-{}.sc'.format(cutoffs_str)
    sc = StructureContainer.read(sc_fname)
    cs = sc.cluster_space

    # run cv
    tag = '{}_cutoffs-{}_trainsize-{}{}'.format(fit_method, cutoffs_str, n_train_structures, serial_fitting_str)
    print('Running CV for {}'.format(tag))
    df_cv_splits = run_cv_analysis(sc, n_train_structures, fit_method, serial_fitting)

    # write FCP for one CV-split
    split_ind = 0
    parameters = df_cv_splits.parameters[split_ind]
    fcp_fname = 'fcps/fcp_{}_split-{}.fcp'.format(tag, split_ind)
    fcp = ForceConstantPotential(cs, parameters)
    fcp.write(fcp_fname)

    # collect all data from the splits for this cutoff size
    data_row = dict(third_order_cutoff=cutoff)
    data_row['rmse_train_ave'] = np.mean(df_cv_splits.rmse_train.mean())
    data_row['rmse_train_std'] = np.mean(df_cv_splits.rmse_train.std())
    data_row['rmse_test_ave'] = np.mean(df_cv_splits.rmse_test.mean())
    data_row['rmse_test_std'] = np.mean(df_cv_splits.rmse_test.std())
    data_row['n_nzp_ave'] = np.mean(df_cv_splits.n_nzp.mean())
    data_row['n_nzp_std'] = np.mean(df_cv_splits.n_nzp.std())

    # update df and write to file
    df = df.append(data_row, ignore_index=True)
    df.to_pickle(df_fname)
