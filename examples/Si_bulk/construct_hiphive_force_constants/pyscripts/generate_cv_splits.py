import pickle
import numpy as np
from sklearn.model_selection import ShuffleSplit


def write_pickle(fname, data):
    with open(fname, 'wb') as handle:
        pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)


def read_pickle(fname):
    with open(fname, 'rb') as handle:
        return pickle.load(handle)


# parameters
n_structures = 20
n_splits = 10
train_sizes = np.arange(1, 16)

# splits for each training size
np.random.seed(42)
cv_splits = dict()
for train_size in train_sizes:
    test_size = n_structures - train_size
    ss = ShuffleSplit(n_splits, train_size=train_size, test_size=test_size)
    splits = []
    for train_set, test_set in ss.split(range(n_structures)):
        assert len(train_set) + len(test_set) == n_structures
        splits.append(dict(train_set=train_set, test_set=test_set))
    cv_splits[train_size] = splits

# write cv splits to file
write_pickle('data/cross_validation_splits.pickle', cv_splits)
