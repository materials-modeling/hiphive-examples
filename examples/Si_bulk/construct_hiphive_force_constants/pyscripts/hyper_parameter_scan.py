import pandas as pd
import numpy as np

from hiphive import StructureContainer
from fit_tools import run_cv_analysis


# parameters
fit_method = 'adaptive-lasso'
cutoffs = [9.65, 9.65, 2.5]
n_train_structures = 5
serial_fitting = False

cutoffs_str = str(cutoffs)[1:-1].replace(', ', '_')
serial_fitting_str = '_serial' if serial_fitting else ''

# hyper-parameters
if fit_method == 'rfe':
    nf_vals = [25, 33, 40, 55, 70, 90, 105, 120, 135, 150, 200, 300, 400, 500, 750, 1000, 1500, 2000]
    kwargs_list = [dict(n_features=nf) for nf in nf_vals]
elif fit_method == 'lasso':
    alpha_vals = np.logspace(-6, -2, 40)
    kwargs_list = [dict(alpha=alpha) for alpha in alpha_vals]
elif fit_method == 'ardr':
    lambda_vals = np.logspace(1, 6, 40)
    kwargs_list = [dict(threshold_lambda=lam) for lam in lambda_vals]
elif fit_method == 'adaptive-lasso':
    alpha_vals = np.logspace(-7, -2, 50)
    kwargs_list = [dict(alpha=alpha) for alpha in alpha_vals]
else:
    raise ValueError('Invalid fit method')


# read sc
sc_fname = 'structure_containers/sc_cutoffs-{}.sc'.format(cutoffs_str)
sc = StructureContainer.read(sc_fname)

# setup pandas dataframe
df_fname = 'data/df_hyper-param-scan_{}_cutoffs-{}_trainsize-{}{}.pickle'.format(
    fit_method, cutoffs_str, n_train_structures, serial_fitting_str)
df = pd.DataFrame()


# run hyper-parameter scan
for kwargs in kwargs_list:
    df_cv_splits = run_cv_analysis(sc, n_train_structures, fit_method, serial_fitting, **kwargs)

    data_row = kwargs.copy()
    data_row['rmse_train_ave'] = np.mean(df_cv_splits.rmse_train.mean())
    data_row['rmse_train_std'] = np.mean(df_cv_splits.rmse_train.std())
    data_row['rmse_test_ave'] = np.mean(df_cv_splits.rmse_test.mean())
    data_row['rmse_test_std'] = np.mean(df_cv_splits.rmse_test.std())
    data_row['n_nzp_ave'] = np.mean(df_cv_splits.n_nzp.mean())
    data_row['n_nzp_std'] = np.mean(df_cv_splits.n_nzp.std())

    print(data_row)

    # update df and write to file
    df = df.append(data_row, ignore_index=True)
    df.to_pickle(df_fname)

