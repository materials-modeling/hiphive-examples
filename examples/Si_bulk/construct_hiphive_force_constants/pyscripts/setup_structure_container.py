import numpy as np
from ase.io import read
from ase.db import connect
from hiphive import ClusterSpace, StructureContainer
from hiphive.utilities import get_displacements


# parameters
cutoffs = [9.65, 7.0, 2.5]
cutoffs_str = str(cutoffs)[1:-1].replace(', ', '_')
sc_fname = 'structure_containers/sc_cutoffs-{}.sc'.format(cutoffs_str)


# setup ClusterSpace and StructureContainer
atoms_ideal = read('../dft_calculations/structures/POSCAR_ideal_size5')
cs = ClusterSpace(atoms_ideal, cutoffs)
sc = StructureContainer(cs)

# add training structures
db = connect('../dft_calculations/structures/silicon-bulk-dft-n5-rattled-0.03.db')

for row in db.select():

    # get displacements and forces
    atoms = row.toatoms()
    forces = atoms.get_forces()
    displacements = get_displacements(atoms, atoms_ideal)

    # sanity check
    max_disp = np.max(np.linalg.norm(displacements, axis=1))
    assert max_disp < 0.5

    # add atoms to StructureContainer
    atoms.positions = atoms_ideal.get_positions()
    atoms.new_array('forces', forces)
    atoms.new_array('displacements', displacements)
    sc.add_structure(atoms)

# write to file
sc.write(sc_fname)
