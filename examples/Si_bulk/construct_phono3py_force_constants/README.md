Phono3py force constants
========================
In pyscripts the script compute_force_constants.py will collect dft calculations from databse silicon-bulk-dft-n5-phono3py and feed them to phono3py to produce force forcants (fc2.hdf5 and fc3.hdf5).

The structures in silicon-bulk-dft-n5-phono3py were produced by the generate_phono3py_poscars.py script.
