import subprocess
import glob
import os
import shutil
import numpy as np
from ase.io import read, write
from ase.db import connect
from tools import write_vasprun


# ideal atoms
atoms = read('structures/POSCAR_prim')


# working directory
work_dir = 'phono3py_calculation/'
shutil.rmtree(work_dir, ignore_errors=True)
os.makedirs(work_dir, exist_ok=True)
os.chdir(work_dir)


# Generate the displaced supercells
write('POSCAR', atoms, format='vasp')
subprocess.call('phono3py -d --dim=\"5 5 5\"', shell=True)
os.makedirs('poscars')
subprocess.call('mv POSCAR-* poscars', shell=True)


# Generate vasprun.xmls
os.makedirs('vaspruns')

db = connect('../../dft_calculations/structures/silicon-bulk-dft-phono3py-dim5.db')
poscars = glob.glob('poscars/POSCAR-*')

for poscar in poscars:
    tag = poscar.split('-')[-1]
    atoms_poscar = read(poscar)
    row = db.get(filename='run_{}.xml.gz'.format(tag))
    atoms_db = row.toatoms()

    assert np.max(np.abs(atoms_poscar.positions - atoms_db.positions)) < 1e-7
    write_vasprun('vaspruns/vasprun.xml-{}'.format(tag), atoms_db.get_forces())


# Produce force constants
subprocess.call('phono3py --cf3 vaspruns/vasprun.xml*', shell=True)

# Write fc2 and fc3
subprocess.call('phono3py --dim=\"5 5 5\" --sym-fc3r --sym-fc2', shell=True)
os.chdir('..')
