import os
import subprocess


# parameters
dim = 5

# generate poscars
cmd = 'phono3py -d --dim=\"{0} {0} {0}\" -c structures/POSCAR_prim'.format(dim)
subprocess.call(cmd, shell=True)

os.makedirs('poscars', exist_ok=True)
subprocess.call('mv POSCAR-* poscars', shell=True)
subprocess.call('rm SPOSCAR disp_fc3.yaml phono3py_disp.yaml', shell=True)
