import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from hiphive.input_output.phonopy import read_phonopy_fc2

from hiphive import ForceConstantPotential
from ase.io import read


try:
    import mplpub
    mplpub.setup(template='natcom', height=2.8, width=3.5)
except ModuleNotFoundError:
    pass


# parameters
train_size = 5
cutoffs = [9.65, 9.65, 2.5]
cutoffs_str = str(cutoffs)[1:-1].replace(', ', '_')


# read fcps
fcp_fname = '../construct_hiphive_force_constants/fcps/fcp_{}_cutoffs-{}_trainsize-{}{}_split-0.fcp'
fcp_l2 = ForceConstantPotential.read(fcp_fname.format('least-squares', cutoffs_str, train_size, ''))
fcp_rfe = ForceConstantPotential.read(fcp_fname.format('rfe', cutoffs_str, train_size, ''))
fcp_lasso = ForceConstantPotential.read(fcp_fname.format('lasso', cutoffs_str, train_size, ''))
fcp_adlasso = ForceConstantPotential.read(fcp_fname.format('adaptive-lasso', cutoffs_str, train_size, ''))
fcp_ardr = ForceConstantPotential.read(fcp_fname.format('ardr', cutoffs_str, train_size, '_serial'))

fcps = dict(l2=fcp_l2, lasso=fcp_lasso, adlasso=fcp_adlasso, rfe=fcp_rfe, ardr=fcp_ardr)

# read phono3py
atoms = read('phono3py_calculation/SPOSCAR')
fc2_phono3py = read_phonopy_fc2('phono3py_calculation/fc2.hdf5')

fcs_reg = dict()
for k, fcp in fcps.items():
    fcs_reg[k] = fcp.get_force_constants(atoms)

# find pairs
inds = list(range(len(atoms)))
distances = atoms.get_distances(0, inds, mic=True)
shells = np.unique(np.round(distances, 8))
nbrs = [np.argmin(np.abs(distances - s)) for s in shells]


# collect fc2 errors
i = 0
data = []
for j in nbrs:
    print(j)

    fc_ref = fc2_phono3py[(i, j)]
    dist = distances[j]
    row = dict(dist=dist, fc_ref_norm=np.linalg.norm(fc_ref))

    for key, fcs in fcs_reg.items():
        fc = fcs[(i, j)]
        delta_fc = np.linalg.norm(fc_ref - fc)
        row['delta_fc_{}'.format(key)] = delta_fc
    data.append(row)
df = pd.DataFrame(data)


# plotting
fit_methods = ['l2', 'lasso', 'adlasso', 'rfe', 'ardr']
labels = {'l2': 'OLS', 'lasso': 'LASSO', 'ardr': 'ARDR', 'rfe': 'RFE with OLS', 'adlasso': 'adaptive-LASSO'}


fig = plt.figure()
xlim = [1.1e-3, 1e2]
ylim = [1.1e-3, 2e-1]

gs = plt.GridSpec(5, 1)
gs.update(hspace=0.0)
axes = [plt.subplot(gs[ii, 0]) for ii in range(len(fit_methods))]

for ax, k in zip(axes, fit_methods):
    column = 'delta_fc_{}'.format(k)
    error = df[column].mean()
    print('Average error {:15} : {:12.6f}'.format(column, error))

    ax.loglog(df.fc_ref_norm, df[column], 'o', alpha=0.7)
    ax.plot(xlim, xlim, '--k')

    ax.text(0.45, 0.81, labels[k], transform=ax.transAxes)
    ax.text(0.45, 0.7, r'error: {:.5f}eV/\AA$^3$'.format(error), transform=ax.transAxes)
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    ax.set_ylabel(r'FC2 error (eV/\AA$^2$)')

axes[-1].set_xlabel(r'FC2 norm phono3py (eV/\AA$^2$)')

fig.tight_layout()
fig.savefig('pdf/Si_fc2_errors_trainsize-{}.pdf'.format(train_size))
plt.show()
