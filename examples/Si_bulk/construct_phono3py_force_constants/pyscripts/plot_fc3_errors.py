import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import itertools
from hiphive.input_output.phonopy import read_phonopy_fc3

from hiphive import ForceConstantPotential
from ase.io import read


try:
    import mplpub
    mplpub.setup(template='natcom', height=7.0, width=3.2)
except ModuleNotFoundError:
    pass


# parameters
train_size = 10
cutoffs = [9.65, 9.65, 2.5]
cutoffs_str = str(cutoffs)[1:-1].replace(', ', '_')


# read fcps
fcp_fname = '../construct_hiphive_force_constants/fcps/fcp_{}_cutoffs-{}_trainsize-{}{}_split-0.fcp'
fcp_l2 = ForceConstantPotential.read(fcp_fname.format('least-squares', cutoffs_str, train_size, ''))
fcp_rfe = ForceConstantPotential.read(fcp_fname.format('rfe', cutoffs_str, train_size, ''))
fcp_lasso = ForceConstantPotential.read(fcp_fname.format('lasso', cutoffs_str, train_size, ''))
fcp_adlasso = ForceConstantPotential.read(fcp_fname.format('adaptive-lasso', cutoffs_str, train_size, ''))
fcp_ardr = ForceConstantPotential.read(fcp_fname.format('ardr', cutoffs_str, train_size, '_serial'))

fcps = dict(l2=fcp_l2, lasso=fcp_lasso, adlasso=fcp_adlasso, rfe=fcp_rfe, ardr=fcp_ardr)

# read phono3py
atoms = read('phono3py_calculation/SPOSCAR')
fcs_phono3py = read_phonopy_fc3('phono3py_calculation/fc3.hdf5')

fcs_reg = dict()
for k, fcp in fcps.items():
    fcs_reg[k] = fcp.get_force_constants(atoms)


# find triplets
inds = list(range(len(atoms)))
distances = atoms.get_distances(0, inds, mic=True)
shells = np.unique(np.round(distances, 8))
nbrs = [np.argmin(np.abs(distances - s)) for s in shells]

# collect fcs
i = 0
data = []
for j in nbrs:
    print(j)
    for k in inds:
        fc_ref = fcs_phono3py[(i, j, k)]
        max_d = max(atoms.get_distance(m, n, mic=True) for m, n in itertools.combinations((i, j, k), r=2))
        row = dict(max_d=max_d, fc_ref_norm=np.linalg.norm(fc_ref))
        for key, fcs in fcs_reg.items():
            fc = fcs[(i, j, k)]
            delta_fc = np.linalg.norm(fc_ref - fc)
            row['delta_fc_{}'.format(key)] = delta_fc
        data.append(row)
df = pd.DataFrame(data)


# plotting
fit_methods = ['l2', 'lasso', 'adlasso', 'rfe', 'ardr']
labels = {'l2': 'OLS', 'lasso': 'LASSO', 'ardr': 'ARDR', 'rfe': 'RFE with OLS', 'adlasso': 'adaptive-LASSO'}


fig = plt.figure()
xlim = [3e-4, 3e2]
ylim = [3e-4, 0.95]

gs = plt.GridSpec(5, 1)
gs.update(hspace=0.0)
axes = [plt.subplot(gs[ii, 0]) for ii in range(len(fit_methods))]

for ax, k in zip(axes, fit_methods):
    column = 'delta_fc_{}'.format(k)
    error = df[column].mean()
    print('Average error {:15} : {:12.6f}'.format(column, error))

    ax.loglog(df.fc_ref_norm, df[column], 'o', alpha=0.7)
    ax.plot(xlim, xlim, '--k')

    ax.text(0.35, 0.21, labels[k], transform=ax.transAxes)
    ax.text(0.35, 0.1, r'error: {:.3f}eV/\AA$^3$'.format(error), transform=ax.transAxes)
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    ax.set_ylabel(r'FC3 error (eV/\AA$^3$)')

axes[-1].set_xlabel(r'FC3 norm phono3py (eV/\AA$^3$)')

fig.tight_layout()
fig.savefig('pdf/Si_fc3_errors_trainsize-{}.pdf'.format(train_size))
plt.show()
