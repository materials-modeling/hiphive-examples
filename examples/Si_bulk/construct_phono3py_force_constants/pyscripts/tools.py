def write_vasprun(file_name, forces):
    """ Writes dummy vasprun file with only forces """

    f = open(file_name, 'w')
    f.write('<?xml version="1.0" encoding="ISO-8859-1"?>\n')
    f.write('<modelling>\n')

    f.write(' <generator>\n')
    f.write('  <i name="version" type="string">4.6.35 </i>\n')
    f.write('</generator>\n')

    f.write(' <calculation>\n')

    f.write('  <varray name="forces" >\n')
    for force in forces:
        f.write('  <v>%16.12f   %16.12f   %16.12f </v>\n'
                % (force[0], force[1], force[2]))
    f.write('  </varray>\n')

    f.write(' </calculation>\n')
    f.write('</modelling>\n')

    f.close()
