Structure generation and DFT calculations
==========================================

Rattled structures
------------------
Script for generating rattled structures are found in pyscripts. The forces of these structures are computed with VASP and collected into an ase-database called silicon-bulk-dft-n5-rattled-0.03.db.


Phono3py structures
--------------------
The required structures for constructing force constants via phono3py are collected into the silicon-bulk-dft-n5-phono3py.db database.