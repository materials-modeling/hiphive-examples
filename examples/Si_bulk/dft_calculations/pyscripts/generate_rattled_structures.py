from ase.io import read, write
from hiphive.structure_generation import generate_rattled_structures


# parameters
size = 5
n_structures = 20
rattle = 0.02


# setup atoms and calc
atoms = read('structures/POSCAR_ideal_size{}'.format(size), format='vasp')

# generate rattled structures
rattled_structures = generate_rattled_structures(atoms, n_structures, rattle)

for i, atoms in enumerate(rattled_structures):
    write('POSCAR-{:05d}'.format(i), atoms)
