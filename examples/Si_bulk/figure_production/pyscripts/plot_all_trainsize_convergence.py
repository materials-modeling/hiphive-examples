from tools import read_hiphive_kappa, read_phono3py_kappa
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

try:
    import mplpub
    mplpub.setup(template='natcom', height=3.7, width=3.5)
except ModuleNotFoundError:
    pass


# parameters
cutoffs = [9.65, 9.65, 2.5]
fit_methods = ['least-squares', 'ardr', 'lasso', 'rfe']
serials = [False, True, False, False]
T = 300
mesh = 32

cutoffs_str = str(cutoffs)[1:-1].replace(', ', '_')

# read rmse learning curves
learning_curves = dict()
df_fname = '../construct_hiphive_force_constants/data/df_learning-curve_{}_cutoffs-{}{}.pickle'

learning_curves['least-squares'] = pd.read_pickle(df_fname.format('least-squares', cutoffs_str, ''))
learning_curves['ardr'] = pd.read_pickle(df_fname.format('ardr', cutoffs_str, '_serial'))
learning_curves['lasso'] = pd.read_pickle(df_fname.format('lasso', cutoffs_str, ''))
learning_curves['rfe'] = pd.read_pickle(df_fname.format('rfe', cutoffs_str, ''))

# read phono3py kappa
temperatures, kappa_phono3py = read_phono3py_kappa(mesh)
T_ind = np.where(temperatures == T)[0][0]

# read hiphive kappa data
kappa_data = dict()
for serial, fit_method in zip(serials, fit_methods):
    df = pd.DataFrame()
    for n_train_structures in range(2, 16):
        try:
            t, k = read_hiphive_kappa(fit_method, cutoffs, n_train_structures, serial, mesh)
        except:
            continue
        assert np.allclose(t, temperatures)

        row = dict(n_train_structures=n_train_structures, kappa=k[T_ind])
        df = df.append(row, ignore_index=True)

    kappa_data[fit_method] = df


# remove data for RFE and L2 in under-determined region as they fail completly
n_parameters = 2525
n_rows_per_structure = 750
# remove undetermined region for OLS based methods
for key in ['least-squares', 'rfe']:
    df = learning_curves[key]
    df = df[df.n_train_structures > n_parameters/n_rows_per_structure]
    learning_curves[key] = df

    df = kappa_data[key]
    df = df[df.n_train_structures > n_parameters/n_rows_per_structure]
    kappa_data[key] = df


# plotting
labels = {'least-squares': 'OLS', 'lasso': 'LASSO', 'ardr': 'ARDR', 'rfe': 'RFE with OLS'}
colors = {'least-squares': 'forestgreen', 'lasso': 'cornflowerblue', 'ardr': 'goldenrod', 'rfe': 'tab:red'}
markers = {'least-squares': '^', 'lasso': 'o', 'ardr': 's', 'rfe': 'd'}
alpha = 0.35

fig = plt.figure()
ax1 = fig.add_subplot(211)
ax2 = fig.add_subplot(212)

ax2.axhline(y=kappa_phono3py[T_ind], ls='--', c='k', lw=1.0, label='phono3py')
ax2.legend(loc=9)

for fit_method in fit_methods:
    df_rmse = learning_curves[fit_method]
    df_kappa = kappa_data[fit_method]

    col = colors[fit_method]
    ls = '-' + markers[fit_method]
    label = labels[fit_method]

    ax1.plot(df_rmse.n_train_structures, 1000 * df_rmse.rmse_test_ave, ls, color=col, label=label)
    ax2.plot(df_kappa.n_train_structures, df_kappa.kappa, ls, color=col, label='')


for ax in [ax1, ax2]:
    ax.set_xlim(0.5, 15.5)

ax1.legend()
ax1.set_xticklabels([])

ax2.set_xlabel('Number of training structures')
ax1.set_ylabel(r'RMSE validation (meV/\AA)')
ax2.set_ylabel('Thermal conductivity (W/mK)')

ax1.set_ylim([3.11, 4.0])
ax2.set_ylim([0, 195])

ax1.text(0.06, 0.88, r'a)', transform=ax1.transAxes)
ax2.text(0.06, 0.88, r'b)', transform=ax2.transAxes)

fig.tight_layout()
plt.subplots_adjust(wspace=0, hspace=0)

fig.savefig('figs/Si_trainsize-conv_cutoffs-{}.pdf'.format(cutoffs_str))
plt.show()

