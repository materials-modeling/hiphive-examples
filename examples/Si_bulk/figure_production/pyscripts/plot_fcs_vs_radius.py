import pandas as pd
import matplotlib.pyplot as plt
from hiphive import ForceConstantPotential
import matplotlib.gridspec as gridspec


try:
    import mplpub
    mplpub.setup(template='natcom', height=2.8, width=3.5)
except ModuleNotFoundError:
    pass


# parameters
train_size = 10
cutoffs = [9.65, 9.65, 2.5]
cutoffs_str = str(cutoffs)[1:-1].replace(', ', '_')


# read fcps
fcp_fname = '../construct_hiphive_force_constants/fcps/fcp_{}_cutoffs-{}_trainsize-{}{}_split-0.fcp'
fcp_ardr = ForceConstantPotential.read(fcp_fname.format('ardr', cutoffs_str, train_size, '_serial'))
fcp_rfe = ForceConstantPotential.read(fcp_fname.format('rfe', cutoffs_str, train_size, ''))
fcp_lasso = ForceConstantPotential.read(fcp_fname.format('lasso', cutoffs_str, train_size, ''))
fcp_adlasso = ForceConstantPotential.read(fcp_fname.format('adaptive-lasso', cutoffs_str, train_size, ''))

df_adlasso = pd.DataFrame(fcp_adlasso.orbit_data)
df_lasso = pd.DataFrame(fcp_lasso.orbit_data)
df_rfe = pd.DataFrame(fcp_rfe.orbit_data)
df_ardr = pd.DataFrame(fcp_ardr.orbit_data)


# set up figure
xlim = [-0.6, 5.5]
ylim = [2e-4, 5e3]
symbols = {1: '*', 2: 'o', 3: '^'}
colors = {1: 'forestgreen',
          2: 'cornflowerblue',
          3: 'goldenrod'}

gs = gridspec.GridSpec(2, 2)
gs.update(wspace=0.0, hspace=0.0)
ax1 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[1])
ax3 = plt.subplot(gs[2])
ax4 = plt.subplot(gs[3])

# plot data
for ax_3, df in zip([ax1, ax2, ax3, ax4], [df_rfe, df_ardr, df_lasso, df_adlasso]):

    # third order
    df_3 = df[df.order == 3]
    for gorder in [1, 2, 3]:
        df_3_body = df_3[df_3.geometrical_order == gorder]
        ax_3.semilogy(df_3_body.radius, df_3_body.force_constant_norm, symbols[gorder],
                      color=colors[gorder], alpha=0.6, label='{}-body'.format(gorder))

for ax in [ax1, ax2, ax3, ax4]:
    ax.set_xlim(xlim)
    ax.set_xticks([0, 1, 2, 3, 4, 5])
    ax.set_ylim(ylim)
for ax in [ax2, ax4]:
    ax.set_yticklabels([])

ax3.set_xlabel(r'Cluster radius (\AA)')
ax4.set_xlabel(r'Cluster radius (\AA)')
ax1.set_ylabel(r'Norm of third-order FCs (eV/\AA$^3$)')
ax1.yaxis.set_label_coords(-0.25, 0.0)
ax1.legend(loc=1, handlelength=2)


ax1.text(0.06, 0.85, r'RFE', transform=ax1.transAxes)
ax2.text(0.06, 0.85, r'ARDR', transform=ax2.transAxes)
ax3.text(0.06, 0.85, r'LASSO', transform=ax3.transAxes)
ax4.text(0.06, 0.85, r'adaptive-LASSO', transform=ax4.transAxes)

plt.tight_layout()
plt.savefig('figs/Si_fcs_vs_radius.pdf')
plt.show()
