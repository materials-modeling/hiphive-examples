import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


try:
    import mplpub
    mplpub.setup(template='natcom', height=2.5, width=3.5)  # noqa
except ModuleNotFoundError:
    pass


# parameters
train_size = 5

# read data
fname = '../construct_hiphive_force_constants/data/df_hyper-param-scan_{}_cutoffs-9.65_9.65_2.5_trainsize-{}{}.pickle'


dfs = dict()
dfs['rfe'] = pd.read_pickle(fname.format('rfe', train_size, ''))
dfs['lasso'] = pd.read_pickle(fname.format('lasso', train_size, ''))
dfs['ardr'] = pd.read_pickle(fname.format('ardr', train_size, '_serial'))
dfs['adaptive-lasso'] = pd.read_pickle(fname.format('adaptive-lasso', train_size, ''))


# plot
labels = {'least-squares': 'OLS', 'lasso': 'LASSO', 'ardr': 'ARDR', 'rfe': 'RFE with OLS', 'adaptive-lasso': 'adaptive-LASSO'}
colors = {'least-squares': 'forestgreen', 'lasso': 'cornflowerblue', 'ardr': 'goldenrod', 'rfe': 'tab:red', 'adaptive-lasso': '#8B008B'}
markers = {'least-squares': '^', 'lasso': 'o', 'ardr': 's', 'rfe': 'd', 'adaptive-lasso': 'H'}

fig, ax1 = plt.subplots()

for key, df in dfs.items():
    col = colors[key]
    ls = '-' + markers[key]
    label = labels[key]

    ax1.semilogx(df.n_nzp_ave, 1000 * df.rmse_test_ave, ls, color=col, label=label)

ax1.set_xlabel('Number of features')
ax1.set_ylabel(r'RMSE validation (meV/\AA)')
ax1.set_xlim([28, 2500])
ax1.set_yticks(np.arange(3.2, 6, 0.2))
ax1.set_ylim(3.2, 4.0)
ax1.legend()

fig.tight_layout()
fig.savefig('figs/Si_hyper-parameter_scan_trainsize-{}.pdf'.format(train_size), bbox_inches='tight')
plt.show()
