from tools import read_hiphive_kappa, read_phono3py_kappa
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

try:
    import mplpub
    mplpub.setup(template='natcom', height=2.4, width=3.5)
except ModuleNotFoundError:
    pass


# parameters
n_train_structures = 5
fit_methods = ['least-squares', 'ardr', 'lasso', 'rfe']
serials = [False, True, False, False]
T = 300
mesh = 32
n_rows_per_structure = 750


# read phono3py kappa
temperatures, kappa_phono3py = read_phono3py_kappa(mesh)
T_ind = np.where(temperatures == T)[0][0]

# read cutoff kappa data
dfs = dict()
for serial, fit_method in zip(serials, fit_methods):
    df = pd.DataFrame()
    for third_order_cutoff in [3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 8.5, 9.0, 9.65]:
        cutoffs = [9.65, third_order_cutoff, 2.5]
        t, k = read_hiphive_kappa(fit_method, cutoffs, n_train_structures, serial, mesh)
        assert np.allclose(t, temperatures)

        row = dict(cutoff=third_order_cutoff, kappa=k[T_ind])
        df = df.append(row, ignore_index=True)

    dfs[fit_method] = df

# read train-size kappa data
cutoffs = [9.65, 9.65, 2.5]
cutoffs_str = str(cutoffs)[1:-1].replace(', ', '_')


# plotting
labels = {'least-squares': 'OLS', 'lasso': 'LASSO', 'ardr': 'ARDR', 'rfe': 'RFE with OLS'}
colors = {'least-squares': 'forestgreen', 'lasso': 'cornflowerblue', 'ardr': 'goldenrod', 'rfe': 'tab:red'}
markers = {'least-squares': '^', 'lasso': 'o', 'ardr': 's', 'rfe': 'd'}

fig = plt.figure()
ax1 = fig.add_subplot(111)

for fit_method, df in dfs.items():
    col = colors[fit_method]
    ls = '-' + markers[fit_method]
    label = labels[fit_method]
    ax1.plot(df.cutoff, df.kappa, ls, color=col, label=label)

ax1.axhline(y=kappa_phono3py[T_ind], ls='--', c='k', lw=1.0, label='phono3py')

ax1.set_xlabel('Third order cutoff (\AA)')
ax1.set_ylabel('Thermal conductivity at {}K (W/mK)'.format(T))
ax1.legend()

fig.tight_layout()
fig.savefig('figs/Si_kappa_cutoff-conv_trainsize{}.pdf'.format(n_train_structures))
plt.show()
