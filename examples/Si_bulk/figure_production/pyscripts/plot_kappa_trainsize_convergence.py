from tools import read_hiphive_kappa, read_phono3py_kappa
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

try:
    import mplpub
    mplpub.setup(template='natcom', height=2.5, width=3.5)
except ModuleNotFoundError:
    pass


# parameters
cutoffs = [9.65, 9.65, 2.5]
fit_methods = ['least-squares', 'ardr', 'lasso', 'rfe', 'adaptive-lasso']
serials = [False, True, False, False, False, False]
T = 300
mesh = 32


# read phono3py kappa
temperatures, kappa_phono3py = read_phono3py_kappa(mesh)
T_ind = np.where(temperatures == T)[0][0]

# read cutoff kappa data
cutoffs_str = str(cutoffs)[1:-1].replace(', ', '_')
dfs = dict()
for serial, fit_method in zip(serials, fit_methods):
    df = pd.DataFrame()
    for n_train_structures in range(2, 16):
        try:
            t, k = read_hiphive_kappa(fit_method, cutoffs, n_train_structures, serial, mesh)
        except:
            continue
        assert np.allclose(t, temperatures)

        row = dict(train_size=n_train_structures, kappa=k[T_ind])
        df = df.append(row, ignore_index=True)

    dfs[fit_method] = df


# plotting
ylim = [10, 180]

labels = {'least-squares': 'OLS', 'lasso': 'LASSO', 'ardr': 'ARDR', 'rfe': 'RFE with OLS', 'adaptive-lasso': 'adaptive-LASSO'}
colors = {'least-squares': 'forestgreen', 'lasso': 'cornflowerblue', 'ardr': 'goldenrod', 'rfe': 'tab:red', 'adaptive-lasso': '#8B008B'}
markers = {'least-squares': '^', 'lasso': 'o', 'ardr': 's', 'rfe': 'd', 'adaptive-lasso': 'H'}

fig = plt.figure()
ax1 = fig.add_subplot(111)

for fit_method, df in dfs.items():
    col = colors[fit_method]
    ls = '-' + markers[fit_method]
    label = labels[fit_method]
    ax1.plot(df.train_size, df.kappa, ls, color=col, label=label)

ax1.axhline(y=kappa_phono3py[T_ind], ls='--', c='k', lw=1.0, label='phono3py')

ax1.set_xlabel('Number of training structures')
ax1.set_ylabel('Thermal conductivity at {}K (W/mK)'.format(T))
ax1.legend(loc=9, ncol=3)
ax1.set_ylim(ylim)

fig.tight_layout()
fig.savefig('figs/Si_kappa_trainsize-conv_cutoffs-{}.pdf'.format(cutoffs_str))
plt.show()
