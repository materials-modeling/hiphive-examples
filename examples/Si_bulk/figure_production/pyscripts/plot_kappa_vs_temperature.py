import numpy as np
from tools import read_hiphive_kappa, read_phono3py_kappa
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.ticker import MultipleLocator, FormatStrFormatter

try:
    import mplpub
    mplpub.setup(template='natcom', height=2.8, width=3.5)
except ModuleNotFoundError:
    pass


# parameters
train_sizes = [5, 10, 15]
mesh = 32
cutoffs = [9.65, 9.65, 2.5]
fit_methods = ['least-squares', 'lasso', 'rfe', 'ardr']
serials = [False, False, False, True]


# read phono3py kappa
temperatures, kappa_phono3py = read_phono3py_kappa(mesh)


# read hiphive kappa
kappa_hiphive = {f: {} for f in fit_methods}
for serial, fit_method in zip(serials, fit_methods):
    for train_size in train_sizes:
        t, kappa = read_hiphive_kappa(fit_method, cutoffs, train_size, serial, mesh)
        assert np.allclose(t, temperatures)
        kappa_hiphive[fit_method][train_size] = kappa


# figure parameters
lw = 1.0
xlim = [100, 1450]
ylim = [10, 700]
symbols = {1: '*', 2: 'o', 3: '^'}
colors = {5: 'goldenrod', 10: 'forestgreen', 15: 'cornflowerblue'}

# set up figure
gs = gridspec.GridSpec(2, 2)
gs.update(wspace=0.0, hspace=0.0)
ax1 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[1])
ax3 = plt.subplot(gs[2])
ax4 = plt.subplot(gs[3])


for ax, fit_method in zip([ax1, ax2, ax3, ax4], fit_methods):

    for train_size in train_sizes:
        col = colors[train_size]
        ax.semilogy(temperatures, kappa_hiphive[fit_method][train_size], '-', lw=lw, color=col, label='{} structures'.format(train_size))

    ax.semilogy(temperatures, kappa_phono3py, '--k', lw=lw, label='direct approach')
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)

ax2.legend(loc=1, bbox_to_anchor=(1.01, 0.99), handlelength=2, fontsize=6.5)
ax1.set_ylabel('Thermal conductivity (W/mK)')
ax1.yaxis.set_label_coords(-0.18, 0.0)

# set labels
for ax in [ax3, ax4]:
    ax.set_xlabel('Temperature (K)')

ax1.text(0.06, 0.85, r'OLS', transform=ax1.transAxes)
ax2.text(0.06, 0.85, r'LASSO', transform=ax2.transAxes)
ax3.text(0.06, 0.85, r'RFE with OLS', transform=ax3.transAxes)
ax4.text(0.06, 0.85, r'ARDR', transform=ax4.transAxes)

# set xticks
majorLocator = MultipleLocator(300)
majorFormatter = FormatStrFormatter('%d')
minorLocator = MultipleLocator(100)
for ax in [ax1, ax2, ax3, ax4]:
    ax.xaxis.set_major_locator(majorLocator)
    ax.xaxis.set_major_formatter(majorFormatter)
    ax.xaxis.set_minor_locator(minorLocator)

for ax in [ax2, ax4]:
    ax.set_yticklabels([])

plt.tight_layout()
plt.savefig('figs/Si_kappa_vs_temperature.pdf')
plt.show()
