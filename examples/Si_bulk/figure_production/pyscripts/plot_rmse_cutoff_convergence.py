import pandas as pd
import matplotlib.pyplot as plt

try:
    import mplpub
    mplpub.setup(template='natcom', height=3.7, width=3.5)
except ModuleNotFoundError:
    pass


# parameters
train_size = 10

# read data
cutoff_convergence = dict()
df_fname = '../construct_hiphive_force_constants/data/df_cutoff-convergence_{}_trainsize-{}{}.pickle'

cutoff_convergence['least-squares'] = pd.read_pickle(df_fname.format('least-squares', train_size, ''))
cutoff_convergence['ardr'] = pd.read_pickle(df_fname.format('ardr', train_size, '_serial'))
cutoff_convergence['lasso'] = pd.read_pickle(df_fname.format('lasso', train_size, ''))
cutoff_convergence['rfe'] = pd.read_pickle(df_fname.format('rfe', train_size, ''))
cutoff_convergence['adaptive-lasso'] = pd.read_pickle(df_fname.format('adaptive-lasso', train_size, ''))


# plotting
labels = {'least-squares': 'OLS', 'lasso': 'LASSO', 'ardr': 'ARDR', 'rfe': 'RFE with OLS', 'adaptive-lasso': 'adaptive-LASSO'}
colors = {'least-squares': 'forestgreen', 'lasso': 'cornflowerblue', 'ardr': 'goldenrod', 'rfe': 'tab:red', 'adaptive-lasso': '#8B008B'}
markers = {'least-squares': '^', 'lasso': 'o', 'ardr': 's', 'rfe': 'd', 'adaptive-lasso': 'H'}


fig = plt.figure()
ax1 = fig.add_subplot(211)
ax2 = fig.add_subplot(212)

for fit_method, df in cutoff_convergence.items():
    col = colors[fit_method]
    ls = '-' + markers[fit_method]
    label = labels[fit_method]

    ax1.plot(df.third_order_cutoff, 1000 * df.rmse_test_ave, ls, color=col, label=label)
    ax2.semilogy(df.third_order_cutoff, df.n_nzp_ave, ls, color=col, label=label)

for ax in [ax1, ax2]:
    ax.set_xlim([2.5, 10])
ax1.set_xticklabels([])
ax1.legend(loc=9)

ax1.set_ylabel(r'RMSE validation (meV/\AA)')
ax2.set_xlabel('Third order cutoff (\AA)')
ax2.set_ylabel('Number of features')

ax1.set_ylim([3.15, 4.0])
ax2.set_ylim(10, 5000)

ax1.text(0.06, 0.88, r'a)', transform=ax1.transAxes)
ax2.text(0.06, 0.88, r'b)', transform=ax2.transAxes)


fig.tight_layout()
plt.subplots_adjust(wspace=0, hspace=0)

fig.savefig('figs/Si_rmse_cutoff-conv_trainsize-{}.pdf'.format(train_size))
plt.show()
