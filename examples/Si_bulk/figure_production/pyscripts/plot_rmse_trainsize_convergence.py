import pandas as pd
import matplotlib.pyplot as plt

try:
    import mplpub
    mplpub.setup(template='natcom', height=4.4, width=3.5)
except ModuleNotFoundError:
    pass


# parameters
cutoffs = [9.65, 9.65, 2.5]
cutoffs_str = str(cutoffs)[1:-1].replace(', ', '_')
n_parameters = 2525
n_rows_per_structure = 750

# read data
learning_curves = dict()
df_fname = '../construct_hiphive_force_constants/data/df_learning-curve_{}_cutoffs-{}{}.pickle'

learning_curves['least-squares'] = pd.read_pickle(df_fname.format('least-squares', cutoffs_str, ''))
learning_curves['ardr'] = pd.read_pickle(df_fname.format('ardr', cutoffs_str, '_serial'))
learning_curves['lasso'] = pd.read_pickle(df_fname.format('lasso', cutoffs_str, ''))
learning_curves['rfe'] = pd.read_pickle(df_fname.format('rfe', cutoffs_str, ''))
learning_curves['adaptive-lasso'] = pd.read_pickle(df_fname.format('adaptive-lasso', cutoffs_str, ''))


# remove undetermined region for OLS based methods
for key in ['least-squares', 'rfe']:
    df = learning_curves[key]
    df = df[df.n_train_structures > n_parameters/n_rows_per_structure]
    learning_curves[key] = df

# plotting
labels = {'least-squares': 'OLS', 'lasso': 'LASSO', 'ardr': 'ARDR', 'rfe': 'RFE with OLS', 'adaptive-lasso': 'adaptive-LASSO'}
colors = {'least-squares': 'forestgreen', 'lasso': 'cornflowerblue', 'ardr': 'goldenrod', 'rfe': 'tab:red', 'adaptive-lasso': '#8B008B'}
markers = {'least-squares': '^', 'lasso': 'o', 'ardr': 's', 'rfe': 'd', 'adaptive-lasso': 'H'}

fig = plt.figure()
ax1 = fig.add_subplot(211)
ax2 = fig.add_subplot(212)

for fit_method, df in learning_curves.items():
    col = colors[fit_method]
    ls = '-' + markers[fit_method]
    label = labels[fit_method]

    ax1.plot(df.n_train_structures, 1000 * df.rmse_test_ave, ls, color=col, label=label)
    ax2.plot(df.n_train_structures, df.n_nzp_ave, ls, color=col, label=label)

for ax in [ax1, ax2]:
    ax.set_xlim(0.5, 15.5)
ax1.set_xticklabels([])
ax1.legend()

ax1.set_ylabel(r'RMSE validation (meV/\AA)')
ax2.set_xlabel('Number of training structures')
ax2.set_ylabel('Number of nonzero parameters')

ax1.set_ylim([3.2, 4.0])
ax2.set_ylim(0, 1000)

ax1.text(0.06, 0.88, r'a)', transform=ax1.transAxes)
ax2.text(0.06, 0.88, r'b)', transform=ax2.transAxes)

fig.tight_layout()
fig.savefig('figs/Si_rmse_trainsize-conv_cutoffs-{}.pdf'.format(cutoffs_str))
plt.show()
