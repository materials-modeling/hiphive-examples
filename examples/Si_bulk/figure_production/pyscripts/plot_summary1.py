import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tools import read_hiphive_kappa, read_phono3py_kappa


try:
    import mplpub
    mplpub.setup(template='natcom', height=5.2, width=3.5)
except ModuleNotFoundError:
    pass


# read cutoff rmse data
train_size = 5
cutoff_convergence = dict()
df_fname = '../construct_hiphive_force_constants/data/df_cutoff-convergence_{}_trainsize-{}{}.pickle'

cutoff_convergence['least-squares'] = pd.read_pickle(df_fname.format('least-squares', train_size, ''))
cutoff_convergence['ardr'] = pd.read_pickle(df_fname.format('ardr', train_size, '_serial'))
cutoff_convergence['lasso'] = pd.read_pickle(df_fname.format('lasso', train_size, ''))
cutoff_convergence['rfe'] = pd.read_pickle(df_fname.format('rfe', train_size, ''))


# read train-size kappa data
cutoffs = [9.65, 4.0, 2.5]
fit_methods = ['least-squares', 'ardr', 'lasso', 'rfe']
serials = [False, True, False, False]
T = 300
mesh = 32

# read phono3py kappa
temperatures, kappa_phono3py = read_phono3py_kappa(mesh)
T_ind = np.where(temperatures == T)[0][0]

# read hiphive kappa
cutoffs_str = str(cutoffs)[1:-1].replace(', ', '_')
dfs_kappa = dict()
for serial, fit_method in zip(serials, fit_methods):
    df = pd.DataFrame()
    for n_train_structures in range(2, 16):
        t, k = read_hiphive_kappa(fit_method, cutoffs, n_train_structures, serial, mesh)
        assert np.allclose(t, temperatures)

        row = dict(train_size=n_train_structures, kappa=k[T_ind])
        df = df.append(row, ignore_index=True)
    dfs_kappa[fit_method] = df


# plotting
labels = {'least-squares': 'OLS', 'lasso': 'LASSO', 'ardr': 'ARDR', 'rfe': 'RFE with OLS'}
colors = {'least-squares': 'forestgreen', 'lasso': 'cornflowerblue', 'ardr': 'goldenrod', 'rfe': 'tab:red'}
markers = {'least-squares': '^', 'lasso': 'o', 'ardr': 's', 'rfe': 'd'}

gs_top = plt.GridSpec(7, 1, hspace=0, top=0.98, right=0.96, bottom=0.068)
gs_bot = plt.GridSpec(7, 1, top=0.75, right=0.96, bottom=0.068)
fig = plt.figure()
ax1 = fig.add_subplot(gs_top[0:2, 0])
ax2 = fig.add_subplot(gs_top[2:4, 0])
ax3 = fig.add_subplot(gs_bot[4:, 0])

# plot rmse data
for fit_method, df in cutoff_convergence.items():
    col = colors[fit_method]
    ls = '-' + markers[fit_method]
    label = labels[fit_method]

    ax1.plot(df.third_order_cutoff, 1000 * df.rmse_test_ave, ls, color=col, label=label)
    ax2.semilogy(df.third_order_cutoff, df.n_nzp_ave, ls, color=col)

for ax in [ax1, ax2]:
    ax.set_xlim([2.5, 10])
ax1.set_xticklabels([])
ax1.legend(loc=9)

ax1.set_ylabel(r'RMSE validation (meV/\AA)')
ax2.set_xlabel('Third order cutoff (\AA)')
ax2.set_ylabel('Number of features')

ax1.set_ylim([3.15, 4.0])
ax2.set_ylim(10, 5000)


# plot kappa data
for fit_method, df in dfs_kappa.items():
    col = colors[fit_method]
    ls = '-' + markers[fit_method]
    label = labels[fit_method]
    ax3.plot(df.train_size, df.kappa, ls, color=col, label='')


k_ref = kappa_phono3py[T_ind]

ax3.axhline(y=k_ref, ls='--', c='k', lw=1.0, label='phono3py')

l = 0.1
alpha = 0.14
ax3.fill_between([0, 20], [(1+l)*k_ref, (1+l)*k_ref], [(1-l)*k_ref, (1-l)*k_ref], color='k', alpha=alpha,
                 linewidth=0.0, label='{}\%'.format(int(100*l)))

l = 0.02
alpha = 0.26
ax3.fill_between([0, 20], [(1+l)*k_ref, (1+l)*k_ref], [(1-l)*k_ref, (1-l)*k_ref], color='k', alpha=alpha,
                 linewidth=0.0, label='{}\%'.format(int(100*l)))

ax3.set_xlabel('Number of training structures')
ax3.set_ylabel('Thermal conductivity (W/mK)'.format(T))
ax3.legend(loc=9, ncol=3)
ax3.set_xlim([1, 16])
ax3.set_ylim([70, 190])


ax1.text(0.04, 0.84, r'a)', transform=ax1.transAxes)
ax2.text(0.04, 0.84, r'b)', transform=ax2.transAxes)
ax3.text(0.04, 0.84, r'c)', transform=ax3.transAxes)

fig.savefig('figs/Si_summary1.pdf'.format(train_size))
plt.show()
