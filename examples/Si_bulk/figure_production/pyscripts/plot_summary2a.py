import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tools import read_hiphive_kappa, read_phono3py_kappa


try:
    import mplpub
    mplpub.setup(template='natcom', height=3.8, width=3.5)
except ModuleNotFoundError:
    pass


# read cutoff rmse data
cutoffs = [9.65, 9.65, 2.5]
cutoffs_str = str(cutoffs)[1:-1].replace(', ', '_')

learning_curves = dict()
df_fname = '../construct_hiphive_force_constants/data/df_learning-curve_{}_cutoffs-{}{}.pickle'

learning_curves['least-squares'] = pd.read_pickle(df_fname.format('least-squares', cutoffs_str, ''))
learning_curves['ardr'] = pd.read_pickle(df_fname.format('ardr', cutoffs_str, '_serial'))
learning_curves['lasso'] = pd.read_pickle(df_fname.format('lasso', cutoffs_str, ''))
learning_curves['rfe'] = pd.read_pickle(df_fname.format('rfe', cutoffs_str, ''))
learning_curves['adaptive-lasso'] = pd.read_pickle(df_fname.format('adaptive-lasso', cutoffs_str, ''))


# read hyper-parameter data
train_size = 5

# read train-size kappa data
fit_methods = ['least-squares', 'ardr', 'lasso', 'rfe', 'adaptive-lasso']
serials = [False, True, False, False, False]
T = 300
mesh = 32

# read phono3py kappa
temperatures, kappa_phono3py = read_phono3py_kappa(mesh)
T_ind = np.where(temperatures == T)[0][0]

# read hiphive kappa
dfs_kappa = dict()
for serial, fit_method in zip(serials, fit_methods):
    df = pd.DataFrame()
    for n_train_structures in range(2, 16):
        try:
            t, k = read_hiphive_kappa(fit_method, cutoffs, n_train_structures, serial, mesh)
        except:
            continue
        assert np.allclose(t, temperatures)

        row = dict(n_train_structures=n_train_structures, kappa=k[T_ind])
        df = df.append(row, ignore_index=True)
    dfs_kappa[fit_method] = df


# remove undetermined region for OLS based methods
n_parameters = 2525
n_rows_per_structure = 750
for key in ['least-squares', 'rfe']:
    df = learning_curves[key]
    df = df[df.n_train_structures > n_parameters/n_rows_per_structure]
    learning_curves[key] = df

    df = dfs_kappa[key]
    df = df[df.n_train_structures > n_parameters/n_rows_per_structure]
    dfs_kappa[key] = df


# plotting
labels = {'least-squares': 'OLS', 'lasso': 'LASSO', 'ardr': 'ARDR', 'rfe': 'RFE with OLS', 'adaptive-lasso': 'adaptive-LASSO'}
colors = {'least-squares': 'forestgreen', 'lasso': 'cornflowerblue', 'ardr': 'goldenrod', 'rfe': 'tab:red', 'adaptive-lasso': '#8B008B'}
markers = {'least-squares': '^', 'lasso': 'o', 'ardr': 's', 'rfe': 'd', 'adaptive-lasso': 'H'}

gs = plt.GridSpec(2, 1, hspace=0)
fig = plt.figure()
ax1 = fig.add_subplot(gs[0])
ax2 = fig.add_subplot(gs[1])


# plot rmse and kappa
k_ref = kappa_phono3py[T_ind]
ax2.axhline(y=k_ref, ls='--', c='k', lw=1.0, label='phono3py')

l = 0.1
alpha = 0.14
ax2.fill_between([0, 20], [(1+l)*k_ref, (1+l)*k_ref], [(1-l)*k_ref, (1-l)*k_ref], color='k', alpha=alpha,
                 linewidth=0.0, label='{}\%'.format(int(100*l)))

for fit_method in fit_methods:

    df_rmse = learning_curves[fit_method]
    df_kappa = dfs_kappa[fit_method]

    col = colors[fit_method]
    ls = '-' + markers[fit_method]
    label = labels[fit_method]

    ax1.plot(df_rmse.n_train_structures, 1000 * df_rmse.rmse_test_ave, ls, color=col, label=label)
    ax2.plot(df_kappa.n_train_structures, df_kappa.kappa, ls, color=col, label='')

for ax in [ax1, ax2]:
    ax.set_xlim(0.5, 15.5)

ax1.set_xticklabels([])
ax1.legend(loc=9)
ax2.legend(loc=9, ncol=2)

ax2.set_xlabel('Number of training structures')
ax1.set_ylabel(r'RMSE validation (meV/\AA)')
ax2.set_ylabel('Thermal conductivity (W/mK)')

ax1.set_yticks(np.arange(3.0, 6, 0.1))
ax1.set_ylim([3.21, 4.0])
ax2.set_ylim([0, 195])

ax1.text(0.04, 0.84, r'a)', transform=ax1.transAxes)
ax2.text(0.04, 0.84, r'b)', transform=ax2.transAxes)

fig.tight_layout()
fig.savefig('figs/Si_summary2a.pdf'.format(train_size))
plt.show()
