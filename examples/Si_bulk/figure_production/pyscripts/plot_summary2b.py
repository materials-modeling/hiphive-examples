import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


try:
    import mplpub
    mplpub.setup(template='natcom', height=3.8, width=3.5)  # noqa
except ModuleNotFoundError:
    pass


# parameters
train_size = 5
cutoffs_str = '9.65_9.65_2.5'

# phono3py kappa at 300K
k_ref = 136.76805833225043

# read data
fname = '../construct_hiphive_force_constants/data/df_hyper-param-scan_{}_cutoffs-{}_trainsize-{}{}.pickle'

dfs = dict()
dfs['rfe'] = pd.read_pickle(fname.format('rfe', cutoffs_str, train_size, ''))
dfs['lasso'] = pd.read_pickle(fname.format('lasso', cutoffs_str, train_size, ''))
dfs['ardr'] = pd.read_pickle(fname.format('ardr', cutoffs_str, train_size, '_serial'))
dfs['adlasso'] = pd.read_pickle(fname.format('adaptive-lasso', cutoffs_str, train_size, ''))

dfs_kappa = dict()
dfs_kappa['ardr'] = pd.read_pickle('../kappa_hyper_curve/data/df_kappa_{}_cutoffs-{}_trainsize-{}{}.pickle'.format('ardr', cutoffs_str, train_size, '_serial'))
dfs_kappa['rfe'] = pd.read_pickle('../kappa_hyper_curve/data/df_kappa_{}_cutoffs-{}_trainsize-{}{}.pickle'.format('rfe', cutoffs_str, train_size, ''))
dfs_kappa['lasso'] = pd.read_pickle('../kappa_hyper_curve/data/df_kappa_{}_cutoffs-{}_trainsize-{}{}.pickle'.format('lasso', cutoffs_str, train_size, ''))
dfs_kappa['adlasso'] = pd.read_pickle('../kappa_hyper_curve/data/df_kappa_{}_cutoffs-{}_trainsize-{}{}.pickle'.format('adaptive-lasso', cutoffs_str, train_size, ''))


# plot
labels = {'least-squares': 'OLS', 'lasso': 'LASSO', 'ardr': 'ARDR', 'rfe': 'RFE with OLS', 'adlasso': 'adaptive-LASSO'}
colors = {'least-squares': 'forestgreen', 'lasso': 'cornflowerblue', 'ardr': 'goldenrod', 'rfe': 'tab:red', 'adlasso': '#8B008B'}
markers = {'least-squares': '^', 'lasso': 'o', 'ardr': 's', 'rfe': 'd', 'adlasso': 'H'}

xlim = [10, 2500]

fig = plt.figure()
gs = plt.GridSpec(2, 1, hspace=0.0)
ax1 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[1])

ax2.axhline(y=k_ref, ls='--', c='k', lw=1.0, label='phono3py')

for key, df in dfs.items():
    col = colors[key]
    ls = '-' + markers[key]
    label = labels[key]

    df2 = dfs_kappa[key]
    ax1.semilogx(df.n_nzp_ave, 1000 * df.rmse_test_ave, ls, color=col, label=label)
    ax2.semilogx(df2.n_nzp_ave, df2.kappa, ls, color=col, label='')

ax1.set_ylabel(r'RMSE validation (meV/\AA)')
ax1.set_xlim(xlim)
ax1.set_yticks(np.arange(3.2, 6, 0.2))
ax1.set_ylim(3.21, 4.0)
ax1.legend()
ax1.set_xticklabels([])

ax2.set_xlabel('Number of features')
ax2.set_ylabel('Thermal conductivity (W/mK)')
ax2.set_xlim(xlim)
ax2.set_ylim([5, 170])
ax2.legend(loc=1, bbox_to_anchor=(1.0, 0.8))

ax1.text(0.04, 0.88, r'a)', transform=ax1.transAxes)
ax2.text(0.04, 0.88, r'b)', transform=ax2.transAxes)

fig.tight_layout()
fig.savefig('figs/Si_summary2b.pdf'.format(train_size), bbox_inches='tight')
plt.show()
