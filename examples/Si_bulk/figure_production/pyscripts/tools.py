import numpy as np


def read_phono3py_kappa(mesh):
    kappa_fname = '../compute_thermal_conductivity/data/kappa_phono3py_mesh{}'.format(mesh)
    t, k = np.loadtxt(kappa_fname, unpack=True)
    return t, k


def read_hiphive_kappa(fit_method, cutoffs, train_size, serial, mesh):
    cutoffs_str = str(cutoffs)[1:-1].replace(', ', '_')
    serial_str = '_serial' if serial else ''
    fcp_tag = '{}_cutoffs-{}_trainsize-{}{}'.format(fit_method, cutoffs_str, train_size, serial_str)
    kappa_fname = '../compute_thermal_conductivity/data/kappa_hiphive_{}_mesh{}'.format(fcp_tag, mesh)

    t, k = np.loadtxt(kappa_fname, unpack=True)
    return t, k
