import h5py
import numpy as np
import glob
import pandas as pd


def read_hdf5_kappa(fname):
    with h5py.File(fname, 'r') as hf:
        temperatures = hf['temperature'][:]
        kappa = hf['kappa'][:][:, 0]  # xx
    return temperatures, kappa


# fcp parameters
split_ind = 0
n_train_structures = 5
cutoffs = [9.65, 9.65, 2.5]
cutoffs_str = str(cutoffs)[1:-1].replace(', ', '_')


T_kappa = 300

# get phono3py ref
t, k = read_hdf5_kappa('../compute_thermal_conductivity/kappa_data/kappa_phono3py_mesh32.hdf5')
ind = np.argmin(np.abs(t-T_kappa))

print('phono3py ref kappa  ', t[ind], k[ind])

# ----

fit_methods = ['lasso', 'rfe', 'adaptive-lasso', 'ardr']

for fit_method in fit_methods:

    serial_fitting = False if fit_method != 'ardr' else True
    serial_fitting_str = '_serial' if serial_fitting else ''
    df_rmse = pd.read_pickle('data/df_hyper-param-scan_{}_cutoffs-{}_trainsize-{}{}.pickle'.format(fit_method, cutoffs_str, n_train_structures, serial_fitting_str))
    kappa_fnames = glob.glob('kappa_data/kappa_hiphive_{}_cutoffs-{}_trainsize-{}_*{}_split-{}_mesh32.hdf5'.format(fit_method, cutoffs_str, n_train_structures, serial_fitting_str, split_ind))

    output_fname = 'data/df_kappa_{}_cutoffs-{}_trainsize-{}{}.pickle'.format(fit_method, cutoffs_str, n_train_structures, serial_fitting_str)

    records = []
    for kappa_fname in kappa_fnames:

        hyper_tag = kappa_fname.split('_trainsize-{}_'.format(n_train_structures))[-1]
        hyper_tag = hyper_tag.split('_split')[0]
        hyper_tag = hyper_tag.replace('_serial', '')
        name, val = hyper_tag.split('-', 1)
        val = float(val)

        t, k = read_hdf5_kappa(kappa_fname)
        ind = np.argmin(np.abs(t-T_kappa))
        kappa = k[ind]

        print(name, val, kappa)
        row = {name: val, 'kappa': kappa}
        records.append(row)

    df = pd.DataFrame(records)
    df = pd.merge(df_rmse, df)
    df = df.sort_values(by=name)
    df.to_pickle(output_fname)
