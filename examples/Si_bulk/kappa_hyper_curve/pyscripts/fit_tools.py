import pickle

import pandas as pd
import numpy as np

from collections import defaultdict
from hiphive.fitting import Optimizer


# hyper parameters
fit_kwargs = defaultdict(dict)
fit_kwargs['lasso'] = dict(max_iter=200000, tol=1e-5)
fit_kwargs['adaptive-lasso'] = dict(max_iter=5000, alphas=np.logspace(-7, -4, 12))
fit_kwargs['rfe'] = dict(step=0.004, n_jobs=1)
fit_kwargs['ardr'] = dict(threshold_lambda=1e4)
fit_kwargs['split-bregman'] = dict(lmbda=5e3, mu=0.3)
fit_kwargs['ridge'] = dict(tol=1e-6, max_iter=100000)

def compute_rmse(A, y, parameters):
    y_predicted = np.dot(A, parameters)
    delta_y = y_predicted - y
    rmse = np.sqrt(np.mean(delta_y**2))
    return rmse


def read_cv_splits():
    fname = '../construct_hiphive_force_constants/data/cross_validation_splits.pickle'
    with open(fname, 'rb') as handle:
        return pickle.load(handle)


def run_cv_analysis(sc, n_train_structures, fit_method, serial_fitting=False, **kwargs):
    """ Run a cv analysis on a StructureContainer for given train size
    and fit method.

    Parameters
    ----------
    sc : StructureContainer
        Structure container with available fit data
    n_train_structures : int
        number of training structures
    fit_method : str
        fit method to use
    serial_fitting : bool
        True means second order will first be fitted and then residual forces
        will be fitted in a second fit. False means everything will be trained
        in one go.
    kwargs : dict
        additional kwargs to be passed to optimizer
    """

    # setup
    kwargs = {**fit_kwargs[fit_method], **kwargs}
    cv_splits = read_cv_splits()

    # run cv
    data_splits = []
    for i, splits in enumerate(cv_splits[n_train_structures]):
        train_set = splits['train_set']
        test_set = splits['test_set']
        assert len(train_set) == n_train_structures

        if serial_fitting:
            summary = serial_fit(sc, fit_method, train_set, test_set, **kwargs)
        else:
            summary = single_fit(sc, fit_method, train_set, test_set, **kwargs)
        data_splits.append(summary)

    return pd.DataFrame(data_splits)


def collect_fit_summary(A_train, y_train, A_test, y_test, parameters):
    summary = dict()
    summary['rmse_train'] = compute_rmse(A_train, y_train, parameters)
    summary['rmse_test'] = compute_rmse(A_test, y_test, parameters)
    summary['n_nzp'] = np.count_nonzero(parameters)
    summary['parameters'] = parameters
    return summary


def single_fit(sc, fit_method, train_set, test_set, **kwargs):

    # setup train and test set
    A_train, y_train = sc.get_fit_data(train_set)
    A_test, y_test = sc.get_fit_data(test_set)
    y_scale = 1.0 / np.std(y_train)
    y_train *= y_scale

    # fit
    opt = Optimizer((A_train, y_train), train_size=1.0, fit_method=fit_method, **kwargs)
    opt.train()
    parameters = opt.parameters / y_scale

    # collect data
    y_train /= y_scale
    summary = collect_fit_summary(A_train, y_train, A_test, y_test, parameters)
    return summary


def serial_fit(sc, fit_method, train_set, test_set, **kwargs):
    """ Serial fit.

    First second order parameters are trained, then all parameters are trained
    on the residual forces.

    This seem to improve the numerical stability of fitting algorithms since
    second order often corresponds to a small fraction of total parameters but
    can explain a very large portion of the forces.
    """

    # setup train and test set
    A_train, y_train = sc.get_fit_data(train_set)
    A_test, y_test = sc.get_fit_data(test_set)
    y_scale = 1.0 / np.std(y_train)
    y_train *= y_scale

    # second order fit
    cs = sc._cs
    n_cols_2 = cs.get_n_dofs_by_order(order=2)
    n_cols_full = cs.n_dofs
    assert n_cols_full == A_train.shape[1]

    # fit using only second order parameters
    opt_2 = Optimizer((A_train[:, :n_cols_2], y_train), train_size=1.0, fit_method=fit_method, **kwargs)
    opt_2.train()
    y_train_res = y_train - opt_2.predict(A_train[:, :n_cols_2])
    parameters_2 = opt_2.parameters

    # fit all parameters to residuals
    y_scale_res = 1.0 / np.std(y_train_res)
    y_train_res *= y_scale_res
    opt = Optimizer((A_train, y_train_res), train_size=1.0, fit_method=fit_method, **kwargs)
    opt.train()
    parameters_res = opt.parameters / y_scale_res

    # collect full parameters
    parameters = np.zeros(n_cols_full)
    parameters[:n_cols_2] = parameters_2 + parameters_res[:n_cols_2]
    parameters[n_cols_2:] = parameters_res[n_cols_2:]
    parameters /= y_scale

    # collect data
    y_train /= y_scale
    summary = collect_fit_summary(A_train, y_train, A_test, y_test, parameters)
    return summary

