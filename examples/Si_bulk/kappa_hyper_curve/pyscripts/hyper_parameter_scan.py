import sys
import os
import pandas as pd
import numpy as np

from hiphive import StructureContainer, ForceConstantPotential
from fit_tools import run_cv_analysis


# parameters

fit_method = sys.argv[1]

cutoffs = [9.65, 9.65, 2.5]
n_train_structures = 5
serial_fitting = False


# hyper-parameters
if fit_method == 'rfe':
    nf_vals = [25, 40, 70, 100, 150, 400, 800, 1600]
    kwargs_list = [dict(n_features=nf) for nf in nf_vals]
elif fit_method == 'omp':
    nf_vals = [25, 40, 70, 100, 150, 400, 800, 1600]
    kwargs_list = [dict(n_nonzero_coefs=nf) for nf in nf_vals]
elif fit_method == 'ridge':
    alpha_vals = np.logspace(-3, 0, 25)
    kwargs_list = [dict(alpha=alpha) for alpha in alpha_vals]
elif fit_method == 'lasso':
    alpha_vals = np.logspace(-5, -2.5, 10)
    kwargs_list = [dict(alpha=alpha) for alpha in alpha_vals]
elif fit_method == 'ardr':
    serial_fitting = True
    lambda_vals = [3e2, 1e3, 3e3, 7e3, 1e4, 1.4e4, 2e4, 3e4, 5e4, 7e4, 1e5]
    kwargs_list = [dict(threshold_lambda=lam) for lam in lambda_vals]
elif fit_method == 'adaptive-lasso':
    alpha_vals = np.logspace(-6.2, -4, 9).tolist() + [1e-3]
    kwargs_list = [dict(alpha=alpha) for alpha in alpha_vals]
else:
    raise ValueError('Invalid fit method')


cutoffs_str = str(cutoffs)[1:-1].replace(', ', '_')
serial_fitting_str = '_serial' if serial_fitting else ''

# read sc
sc_fname = '../construct_hiphive_force_constants/structure_containers/sc_cutoffs-{}.sc'.format(cutoffs_str)
sc = StructureContainer.read(sc_fname)
cs = sc.cluster_space

# setup pandas dataframe
df_fname = 'data/df_hyper-param-scan_{}_cutoffs-{}_trainsize-{}{}.pickle'.format(
    fit_method, cutoffs_str, n_train_structures, serial_fitting_str)
df = pd.DataFrame()


# run hyper-parameter scan
for kwargs in kwargs_list:

    if len(kwargs) > 1:
        raise ValueError
    hyper_param = list(kwargs.keys())[0]
    hyper_value = kwargs[hyper_param]
    hyper_tag = '{}-{}'.format(hyper_param, hyper_value)


    tag = '{}_cutoffs-{}_trainsize-{}_{}{}'.format(fit_method, cutoffs_str, n_train_structures,hyper_tag, serial_fitting_str)
    print('Running CV for {}'.format(tag))
    df_cv_splits = run_cv_analysis(sc, n_train_structures, fit_method, serial_fitting, **kwargs)

    data_row = kwargs.copy()
    data_row['rmse_train_ave'] = np.mean(df_cv_splits.rmse_train.mean())
    data_row['rmse_train_std'] = np.mean(df_cv_splits.rmse_train.std())
    data_row['rmse_test_ave'] = np.mean(df_cv_splits.rmse_test.mean())
    data_row['rmse_test_std'] = np.mean(df_cv_splits.rmse_test.std())
    data_row['n_nzp_ave'] = np.mean(df_cv_splits.n_nzp.mean())
    data_row['n_nzp_std'] = np.mean(df_cv_splits.n_nzp.std())


    for k, v in data_row.items():
        print('   {:20}  :  {}'.format(k, v))

    # update df and write to file
    df = df.append(data_row, ignore_index=True)
    df.to_pickle(df_fname)

    # write FCP for one CV-split
    for split_ind in range(10):
        parameters = df_cv_splits.parameters[split_ind]
        fcp_fname = 'fcps/fcp_{}_split-{}.fcp'.format(tag, split_ind)
        fcp = ForceConstantPotential(cs, parameters)
        fcp.write(fcp_fname)


