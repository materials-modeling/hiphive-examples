import pandas as pd
import matplotlib.pyplot as plt
import mplpub
mplpub.setup('natcom', width=7.2, height=5.0)

# parameters
cutoffs = [9.65, 9.65, 2.5]
n_train_structures = 5
cutoffs_str = str(cutoffs)[1:-1].replace(', ', '_')

hyper_parameters = dict(ardr='threshold_lambda',
                        ridge='alpha',
                        lasso='alpha',
                        adlasso='alpha',
                        rfe='n_features',
                        omp='n_nonzero_coefs')


dfs = dict()
dfs['ardr'] = pd.read_pickle('data/df_kappa_{}_cutoffs-{}_trainsize-{}{}.pickle'.format('ardr', cutoffs_str, n_train_structures, '_serial'))
dfs['rfe'] = pd.read_pickle('data/df_kappa_{}_cutoffs-{}_trainsize-{}{}.pickle'.format('rfe', cutoffs_str, n_train_structures, ''))
dfs['lasso'] = pd.read_pickle('data/df_kappa_{}_cutoffs-{}_trainsize-{}{}.pickle'.format('lasso', cutoffs_str, n_train_structures, ''))
dfs['adlasso'] = pd.read_pickle('data/df_kappa_{}_cutoffs-{}_trainsize-{}{}.pickle'.format('adaptive-lasso', cutoffs_str, n_train_structures, ''))


# phono3py kappa
k_ref = 136.76805833225043

# plot params
fit_methods = sorted(dfs.keys())
fit_methods = ['lasso', 'adlasso', 'rfe', 'ardr']

xlabels = {'lasso': r'$\alpha$', 'adlasso': r'$\alpha$', 'rfe': 'Number of features', 'ardr': r'$\lambda$-threshold'}
titles = dict(lasso='LASSO', adlasso='adaptive-LASSO', rfe='RFE', ardr='ARDR')

n_rows = len(fit_methods)
ylim1 = [2, 2600]
ylim2 = [2.5, 7.9]
ylim3 = [0, 170]


# setup
fig = plt.figure()
gs = plt.GridSpec(3, n_rows, wspace=0.0, hspace=0.0)
axes = [[fig.add_subplot(gs[0, i]), fig.add_subplot(gs[1, i]), fig.add_subplot(gs[2, i])] for i in range(n_rows)]

for i, fit_method in enumerate(fit_methods):
    h = hyper_parameters[fit_method]
    df = dfs[fit_method]
    xlim = [0.7*df[h].min(), 1.3*df[h].max()]

    axes[i][0].loglog(df[h], df.n_nzp_ave, '-o', label='')
    axes[i][1].semilogx(df[h], 1000 * df.rmse_test_ave, '-o', label='')
    axes[i][2].semilogx(df[h], df.kappa, '-o', label='')
    axes[i][2].semilogx(xlim, [k_ref, k_ref], '--k', label='phono3py')

    axes[i][0].set_xlim(xlim)
    axes[i][1].set_xlim(xlim)
    axes[i][2].set_xlim(xlim)
    axes[i][2].set_xlabel(xlabels[fit_method])
    axes[i][0].set_title(titles[fit_method])

    axes[i][0].set_xticklabels([])
    axes[i][1].set_xticklabels([])
    axes[i][2].legend(loc=8)


# labels and ticks
for i in range(n_rows):
    axes[i][0].set_ylim(ylim1)
    axes[i][1].set_ylim(ylim2)
    axes[i][2].set_ylim(ylim3)

for i in range(1, n_rows):
    axes[i][0].set_yticklabels([])
    axes[i][1].set_yticklabels([])
    axes[i][2].set_yticklabels([])

axes[0][0].set_ylabel('Number of features')
axes[0][1].set_ylabel(r'RMSE (meV/\AA)')
axes[0][2].set_ylabel('Kappa (W/mK)')

fig.tight_layout()
fig.savefig('pdf/hyper_curves.pdf')
plt.show()
