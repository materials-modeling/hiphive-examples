import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_context('talk')


# parameters
cutoffs = [9.65, 9.65, 2.5]
n_train_structures = 5
cutoffs_str = str(cutoffs)[1:-1].replace(', ', '_')

hyper_parameters = dict(ardr='threshold_lambda',
                        ridge='alpha',
                        lasso='alpha',
                        adlasso='alpha',
                        rfe='n_features',
                        omp='n_nonzero_coefs')


# read data
dfs_rmse = dict()

dfs_rmse['ardr'] = pd.read_pickle('data/df_hyper-param-scan_{}_cutoffs-{}_trainsize-{}{}.pickle'.format(
    'ardr', cutoffs_str, n_train_structures, ''))
dfs_rmse['rfe'] = pd.read_pickle('data/df_hyper-param-scan_{}_cutoffs-{}_trainsize-{}{}.pickle'.format(
    'rfe', cutoffs_str, n_train_structures, ''))
dfs_rmse['lasso'] = pd.read_pickle('data/df_hyper-param-scan_{}_cutoffs-{}_trainsize-{}{}.pickle'.format(
    'lasso', cutoffs_str, n_train_structures, ''))
dfs_rmse['adlasso'] = pd.read_pickle('data/df_hyper-param-scan_{}_cutoffs-{}_trainsize-{}{}.pickle'.format(
    'adaptive-lasso', cutoffs_str, n_train_structures, ''))
dfs_rmse['omp']  = pd.read_pickle('data/df_hyper-param-scan_{}_cutoffs-{}_trainsize-{}{}.pickle'.format(
    'omp', cutoffs_str, n_train_structures, ''))
dfs_rmse['ridge']  = pd.read_pickle('data/df_hyper-param-scan_{}_cutoffs-{}_trainsize-{}{}.pickle'.format(
    'ridge', cutoffs_str, n_train_structures, ''))


dfs_kappa = dict()
dfs_kappa['ardr'] = pd.read_pickle('data/kappa_data_ardr.pickle')
dfs_kappa['rfe'] = pd.read_pickle('data/kappa_data_rfe.pickle')
dfs_kappa['lasso'] = pd.read_pickle('data/kappa_data_lasso.pickle')
dfs_kappa['adlasso'] = pd.read_pickle('data/kappa_data_adaptive-lasso.pickle')
dfs_kappa['omp'] = pd.read_pickle('data/kappa_data_omp.pickle')
#dfs_kappa['ridge'] = pd.read_pickle('data/kappa_data_ridge.pickle')


# phono3py kappa
k_ref = 136.76805833225043

# plot params
fit_methods = sorted(dfs_kappa.keys())
n_rows = len(fit_methods)
xlim = [8, 2500]
ylim1 = [3, 7]
ylim2 = [0, 160]




# setup

fig = plt.figure(figsize=(18, 7))
gs = plt.GridSpec(2, n_rows, wspace=0)
axes = [[fig.add_subplot(gs[0, i]), fig.add_subplot(gs[1, i])] for i in range(n_rows)]

for i, fit_method in enumerate(fit_methods):

    df1 = dfs_rmse[fit_method]
    df2 = dfs_kappa[fit_method]
    df = pd.merge(df1, df2)

    axes[i][0].semilogx(df1.n_nzp_ave, 1000 * df1.rmse_test_ave, '-o')
    axes[i][1].semilogx(df.n_nzp_ave, df.kappa, '-o')
    axes[i][1].semilogx(xlim, [k_ref, k_ref], '--k')

    axes[i][0].set_xlim(xlim)
    axes[i][1].set_xlim(xlim)
    axes[i][0].set_title(fit_method)
    axes[i][1].set_xlabel('num features')


# labels and ticks
for i in range(n_rows):
    axes[i][0].set_ylim(ylim1)
    axes[i][1].set_ylim(ylim2)

for i in range(1, n_rows):
    axes[i][0].set_yticklabels([])
    axes[i][1].set_yticklabels([])

axes[0][0].set_ylabel('RMSE (meV/Å)')
axes[0][1].set_ylabel('Kappa (W/mK)')

fig.tight_layout()
fig.savefig('pdf/nzp_curves.pdf')
plt.show()
