import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_context('talk')

# parameters
cutoffs = [9.65, 9.65, 2.5]
n_train_structures = 5
cutoffs_str = str(cutoffs)[1:-1].replace(', ', '_')

hyper_parameters = dict(ardr='threshold_lambda',
                        ridge='alpha',
                        lasso='alpha',
                        adlasso='alpha',
                        rfe='n_features',
                        omp='n_nonzero_coefs')


# read data
dfs_rmse = dict()

dfs_rmse['ardr'] = pd.read_pickle('data/df_hyper-param-scan_{}_cutoffs-{}_trainsize-{}{}.pickle'.format(
    'ardr', cutoffs_str, n_train_structures, '_serial'))
dfs_rmse['rfe'] = pd.read_pickle('data/df_hyper-param-scan_{}_cutoffs-{}_trainsize-{}{}.pickle'.format(
    'rfe', cutoffs_str, n_train_structures, ''))
dfs_rmse['lasso'] = pd.read_pickle('data/df_hyper-param-scan_{}_cutoffs-{}_trainsize-{}{}.pickle'.format(
    'lasso', cutoffs_str, n_train_structures, ''))
dfs_rmse['adlasso'] = pd.read_pickle('data/df_hyper-param-scan_{}_cutoffs-{}_trainsize-{}{}.pickle'.format(
    'adaptive-lasso', cutoffs_str, n_train_structures, ''))


dfs_kappa = dict()
dfs_kappa['ardr'] = pd.read_pickle('data/kappa_data_ardr.pickle')
dfs_kappa['rfe'] = pd.read_pickle('data/kappa_data_rfe.pickle')
dfs_kappa['lasso'] = pd.read_pickle('data/kappa_data_lasso.pickle')
dfs_kappa['adlasso'] = pd.read_pickle('data/kappa_data_adaptive-lasso.pickle')

if False:
    for k, df in dfs_kappa.items():
        df = df[df.kappa < 1e5]  # remove kappa values for which all third-order was pruned
        dfs_kappa[k] = df


# phono3py kappa
k_ref = 136.76805833225043

# plot params
fit_methods = sorted(dfs_kappa.keys())
n_rows = len(fit_methods)
xlim = [8, 2500]
ylim1 = [3, 7]
ylim2 = [0, 160]




# setup
fig = plt.figure(figsize=(7, 7))
ax1 = fig.add_subplot(2, 1, 1)
ax2 = fig.add_subplot(2, 1, 2)

ax2.semilogx(xlim, [k_ref, k_ref], '--k', label='phono3py')

for i, fit_method in enumerate(fit_methods):

    df1 = dfs_rmse[fit_method]
    df2 = dfs_kappa[fit_method]
    df = pd.merge(df1, df2)

    ax1.semilogx(df1.n_nzp_ave, 1000 * df1.rmse_test_ave, '-o', label=fit_method)
    ax2.semilogx(df.n_nzp_ave, df.kappa, '-o', label=fit_method)

ax1.legend(fontsize=11)
ax2.legend(fontsize=11)

ax1.set_xlim(xlim)
ax2.set_xlim(xlim)
ax2.set_xlabel('Number nonzero parameters')


# labels and ticks
ax1.set_ylim(ylim1)
ax2.set_ylim(ylim2)

ax1.set_ylabel('RMSE (meV/Å)')
ax2.set_ylabel('Kappa (W/mK)')

fig.tight_layout()
fig.savefig('pdf/nzp_curves_v2.pdf')
plt.show()
