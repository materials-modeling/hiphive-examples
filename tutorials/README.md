# Tutorials


## Content

* Basic tutorial
    * Basic
    * Third order
    * Fourth order
* Extra
    * Convergence is important (Si study)
    * Fourth order model for Cu surface and its anharmonic potential energy landscape



## Setup
If you do not already have hiphive installed, this can be achived by
    
    pip3 install --user hiphive

These tutorials are provided as jupyter notebooks, to install jupyter run

    pip3 install jupyter

Additional packages that are good to have for the tutorials are 

* `phonopy <https://atztogo.github.io/phonopy/>`_
* `phono3py <https://atztogo.github.io/phono3py/>`_

The installation procedure for these two packages can be found on their respective homepages.
They can for example be installed via `pip`

    pip3 install --user phonopy
    pip3 install --user phono3py


## Getting started with tutorials
To get started simply start a jupyter notebook via

    jupyter notebook