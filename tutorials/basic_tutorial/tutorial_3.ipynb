{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Molecular dynamics simulations\n",
    "\n",
    "The objective of this tutorial section is to demonstrate the usage of an FCP\n",
    "object in a molecular dynamics (MD) simulation. Such simulations can provide\n",
    "very rich information pertaining to the system including but not limited to\n",
    "free energies, the lattice thermal conductivity, or phonon lifetimes, fully\n",
    "accounting for anharmonic effects in the classical limit.\n",
    "\n",
    "The integration of the equations of motion will be carried out using\n",
    "functionality provided by [ASE](https://wiki.fysik.dtu.dk/ase/) while\n",
    "`hiPhive` is used to provide an interaction model in the form of an\n",
    "ASE calculator object.\n",
    "\n",
    "## Preparations\n",
    "\n",
    "First a number of parameters are set that define\n",
    "\n",
    "* the size of the simulation cell (``cell_size``),\n",
    "* the number of MD steps (``number_of_MD_steps``),\n",
    "* the time step in fs (``time_step``),\n",
    "* the temperatures at which MD simulations will be carried out (``temperatures``),\n",
    "* the frequency at which configurations will be written to disk (``dump_interval``), and\n",
    "* the names of the output files (``log_file`` and ``traj_file``)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "# parameters\n",
    "cell_size = 6  # system size\n",
    "number_of_MD_steps = 1000\n",
    "time_step = 5  # in fs\n",
    "dump_interval = 20\n",
    "temperatures = [600, 1200]\n",
    "log_file = 'md_runs/logs_T{}'\n",
    "traj_file = 'md_runs/trajs_T{}.traj'\n",
    "if not os.path.isdir(os.path.dirname(log_file)):\n",
    "    os.mkdir(os.path.dirname(log_file))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we prepare a supercell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ase.build import bulk\n",
    "\n",
    "atoms = bulk('Ni').repeat(cell_size)\n",
    "reference_positions = atoms.get_positions()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then we initialize an ASE calculator object using the FCP prepared previously. To this end, we construct the\n",
    "force constant matrices for the supercell to be used in the MD simulations using the  `ForceConstantPotential.get_force_constants` function, which are\n",
    "subsequently used to initialize an ASE calculator via the\n",
    "`ForceConstantCalculator`\n",
    "class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Populating orbits\n",
      "Done in 0d 0h 0m 20.0s\n"
     ]
    }
   ],
   "source": [
    "from hiphive import ForceConstantPotential\n",
    "from hiphive.calculators import ForceConstantCalculator\n",
    "\n",
    "fcp = ForceConstantPotential.read('fcc-nickel.fcp')\n",
    "fcs = fcp.get_force_constants(atoms)\n",
    "calc = ForceConstantCalculator(fcs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## MD simulations\n",
    "\n",
    "We are ready to carry out MD simulations using the functionality provided by ASE. The `ForceConstantCalculator` is attached to the supercell\n",
    "structure, an integrator that samples Langevin dynamics is initialized, and the\n",
    "output is prepared. Finally, the atomic velocities are initialized and the MD\n",
    "simulation is run"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ase import units\n",
    "from ase.io.trajectory import Trajectory\n",
    "from ase.md.velocitydistribution import MaxwellBoltzmannDistribution\n",
    "from ase.md.langevin import Langevin\n",
    "from ase.md import MDLogger\n",
    "\n",
    "atoms.set_calculator(calc)\n",
    "for temperature in temperatures:\n",
    "    dyn = Langevin(atoms, time_step * units.fs, temperature * units.kB, 0.02)\n",
    "    logger = MDLogger(dyn, atoms, log_file.format(temperature),\n",
    "                      header=True, stress=False, peratom=True, mode='w')\n",
    "    traj_writer = Trajectory(traj_file.format(temperature), 'w', atoms)\n",
    "    dyn.attach(logger, interval=dump_interval)\n",
    "    dyn.attach(traj_writer.write, interval=dump_interval)\n",
    "\n",
    "    # run MD\n",
    "    MaxwellBoltzmannDistribution(atoms, temperature * units.kB)\n",
    "    dyn.run(number_of_MD_steps)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once the MD simulations have concluded the mean-square displacements (MSDs) are computed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "T =  600    MSD = 0.02153 A**2\n",
      "T = 1200    MSD = 0.04694 A**2\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "\n",
    "for temperature in temperatures:\n",
    "    traj_reader = Trajectory(traj_file.format(temperature), 'r')\n",
    "    msd = []\n",
    "    for atoms in [a for a in traj_reader][10:]:\n",
    "        displacements = atoms.positions - reference_positions\n",
    "        msd.append(np.mean(np.sum(displacements**2, axis=1)))\n",
    "    print('T = {:4d}    MSD = {:.5f} A**2'.format(temperature, np.mean(msd)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can compare this with the values from the harmonic approximation earlier.\n",
    "\n",
    "This comparison indicates that the harmonic approximation systematically overestimates the MSDs. It must be noted though that the MSD analysis from phonopy includes quantum-mechanical effects, most notably zero-point motion, whereas the MD simulation is purely classical. More importantly with respect to the verification of the force constant model, a comprehensive study with tighter convergence parameters demonstrates that the fourth-order model constructed here closely reproduces the results from MD simulations that employ the original (EMT) potential directly, as shown in the figure below.\n",
    "\n",
    "![MSD](https://hiphive.materialsmodeling.org/_images/msds_nickel.svg \"MSD\")\n",
    "\n",
    "Mean-square displacement of FCC Ni as a function of temperature as obtained within the harmonic approximation as well as from molecular dynamics (MD) simulations based on the original potential (EMT) and a fourth-order Hamiltonian constructed from MC rattled configurations."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
